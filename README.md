# Experience Visualizer

This python application provides you an interface and its tools to visualize your experiences data. 

## Install requirements

Make sure you have all the following requirements :
- Python 3 (created with the 3.8.10 version) and its default library (especially tkinter)
- numpy
- matplotlib
- panda
- seaborn
- svgpath2mpl
- pingouin
- scipy

## Run the app

As this is a python app, you only need to run `python3 ExperienceVisualizer.py` at the project root.

## Limits

This project has been made to visualize the data for the *Online2DHand* experience and thus is obviously not adapted to every need. Nevertheless, it is higly customizeable and allows you to explore your data with your own specific treatment.

## Add your data

Create the folder *YourExperience* under *Experiences/* with the following structure :
- CSV
  - `Data.csv` (your experience data file)
- `YourExperienceProcessor.py` (extends *App.DataProcessor*)
- `YourExperienceVisualizer.py` (extends *App.Visualizer*)

Don't forget to add the necessary lines in the `ExperienceVisualizer.py` file

## License

This project is licensed under the AGPL-V3 which allows anyone to make modifications to the code and distribute it even for commercial purposes.