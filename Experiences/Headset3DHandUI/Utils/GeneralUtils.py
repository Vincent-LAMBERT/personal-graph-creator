#!/usr/bin/env python3

import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from svgpath2mpl import parse_path
from Experiences.Headset3DHandUI.Utils.SeedRetriever import SeedRetriever

from Experiences.Online2DHand.Utils.FamiliesUtils import *

ID="Id"
GENDER="Gender"
PREVIOUS_STUDY="Previous study done"
GENDERS=["Male", "Female", "Other"]
AGE="Age"
SD_ORDER_KEY="Pref Static/Dynamic"

ALL_MG="All"
ALL="All"
FAMILIES = ["A", "F", "H", "R"]
TAP="tap"
SWIPE="swipe"
FLEX="flex"
HOLD="hold"
STATIC="static"
DYNAMIC="dynamic"
QUANTITY="quantity"
PERCENTAGE="percentage"
MATHS="Maths"
SPACE=" "
PHASE="Phase" 
PHASES = [x for x in range(1,4)]
PERFORMANCES = "Performances"
SATISFACTION = "Satisfaction"
FIRST_PHASE = "Priori"
THIRD_PHASE = "Posteriori"
ORDER_STRING_PHASE = [FIRST_PHASE, THIRD_PHASE]
STRING_PHASES = [PERFORMANCES, SATISFACTION]
MICROGESTURES = [TAP, SWIPE, FLEX, HOLD]
TAP_RECOGNIZED="Tap"
SWIPE_RECOGNIZED="Swipe"
FLEX_RECOGNIZED="Flex"
HOLD_RECOGNIZED="Hold"
OTHER_RECOGNIZED="Other"
RECOGNIZED = [TAP_RECOGNIZED, SWIPE_RECOGNIZED, FLEX_RECOGNIZED, HOLD_RECOGNIZED, OTHER_RECOGNIZED]
MICROGESTURES_GUI = [ALL, TAP, SWIPE, FLEX, HOLD]
STATES = [STATIC, DYNAMIC]
STATES_GUI = [ALL]+STATES

ARROW=" => "
FIRST = "1st"
SD=STATIC+SPACE+FIRST
DS=DYNAMIC+SPACE+FIRST

PREVIOUS_DONE = "Done"
PREVIOUS_NOT_DONE = "Not done"

STATES_COLORS = {STATIC : "#BD2222",
                 DYNAMIC : "#2F22BD"}
MG_COLORS = {TAP : "#267123",
            SWIPE : "#4E9E4B",
            FLEX : "#7CC17A",
            HOLD : "#B7EAB5"}
PHASES_COLORS = {FIRST_PHASE: "#B445A0",
                THIRD_PHASE: "#4BB445"}
ORDER_COLORS = {SD: "#B445A0",
                DS: "#4BB445"}
PREVIOUS_COLORS = {PREVIOUS_DONE: "#B445A0",
                      PREVIOUS_NOT_DONE: "#4BB445"}

MEAN="mean"
MEDIAN="median"
STDEV="stdev"
RANK="rank"
SIZE="size"

def effectSize(means, stdevs) :
    effectSizes=[]
    for i in range(len(means)):
        effectSizes.append(computeEffectSize(means[i], stdevs[i]))
    return pd.Series(effectSizes)

def computeEffectSize(mean, stdev) :
    return mean/stdev

SCORES = {"Very bad" : 1, "Bad" : 2,  "Quite bad" : 3, "Quite good" : 4, "Good" : 5, "Very good" : 6}
REPLETTERTONBR = {chr(a):a-64 for a in range(65, 65+21)}
REPNBRTOLETTER = {a-64:chr(a) for a in range(65, 65+21)}
IDENTITY=lambda x:x
FAMILY_NBR=4
PARTICIPANT_ID='Participant ID'
ORDER='Order'
STATE='State'
MICROGESTURE='Microgesture'
FAMILY='Family'
INFO="Info"
SCORE="Note"
COUNT="Count"
DATA_TO_FOCUS=[PARTICIPANT_ID, PREVIOUS_STUDY, ORDER, STATE, MICROGESTURE, FAMILY]
DATA_TO_FOCUS_SPLIT_FAM=[PARTICIPANT_ID, PREVIOUS_STUDY, ORDER, STATE, MICROGESTURE, FAMILY]
CATEGORICAL_FOCUSES = [PARTICIPANT_ID, STATE, ORDER, MICROGESTURE, FAMILY]
PERFORMANCE_COLUMNS=[FAMILY, PHASE, STATE, MICROGESTURE]

PIE="Pie"
BOX="Box"
STRIP="Strip"
VIOLIN="Violin"
BAR="Bar"
CLUSTERED_STACKED_BAR="Clustered stacked bar"
SCATTER="Scatter"
ELLIPSES="Ellipses"
MULTIPLE_LINES="Multiple lines"
MULTIPLE_BARS="Multiple bars"
MULTIPLE_BOXES="Multiple boxes"
MULTIPLE_VIOLIN="Multiple violin"
EFFECT="Effect sizes"
ORDERING="Ordering"
REGPLOT="Regplot"
GRAPH_TYPES_P2 = [PIE, STRIP, BOX, VIOLIN, BAR, SCATTER, ELLIPSES, MULTIPLE_BARS, MULTIPLE_LINES, MULTIPLE_BOXES, MULTIPLE_VIOLIN, EFFECT, ORDERING, REGPLOT]
GRAPH_TYPES_P1_P3 = [CLUSTERED_STACKED_BAR, STRIP, BOX, VIOLIN, BAR, MULTIPLE_BARS, MULTIPLE_BOXES, MULTIPLE_VIOLIN, REGPLOT, EFFECT]


AANDB_NAME="A&B"
CHV_NAME="Chv"
CHP_NAME="Chp"
MAS_NAME="MaS"

ORIENT_SIDE="Side"
ORIENT_ABOVE="Above"
ORIENT_NONE="None"
ORIENTS=[ORIENT_SIDE, ORIENT_ABOVE, ORIENT_NONE]

SYMBOLS={AANDB_NAME : AANDB, CHV_NAME : CHV, CHP_NAME : CHP, MAS_NAME : MAS}

def computeMark(family) :
    sym = parse_path(SYMBOLS[family])
    sym.vertices -= sym.vertices.mean(axis=0)
    return sym

FAM_MARKERS={f:computeMark(f) for f in SYMBOLS}

FAM_COLORS={AANDB_NAME : "#C68D8D", CHV_NAME : "#B8F2FF", CHP_NAME : "#DEF3AB", MAS_NAME : "#6C9A95"}

def get_flag(name, orient):
    path = os.path.dirname(__file__)
    path = path+"/../Images/Families/Icons/"+orient+"/PNG/{}.png".format(name)
    im = plt.imread(path)
    return im

def offsetXImage(coord, name, ax, orient):
    img = get_flag(name.get_text(), orient)
    
    if orient == ORIENT_SIDE :
        margin = 33.
        zoom=0.70
    elif orient == ORIENT_ABOVE :
        margin = 40.
        zoom=0.70
    elif orient == ORIENT_NONE :
        margin = 35.
        zoom=0.47


    im = OffsetImage(img, zoom=zoom)
    im.image.axes = ax
    ab = AnnotationBbox(im, coord,  xybox=(0., -margin), frameon=False,
                        xycoords='data',  boxcoords="offset points", pad=0)

    ax.add_artist(ab)

def offsetYImage(coord, name, ax, orient):
    img = get_flag(name.get_text(), orient)
    im = OffsetImage(img, zoom=0.70)
    im.image.axes = ax

    ab = AnnotationBbox(im, coord,  xybox=(-33., 0.), frameon=False,
                        xycoords='data',  boxcoords="offset points", pad=0)

    ax.add_artist(ab)

## UTILS ##

def getFamilyNameFromId(id) :
    return {
        "A" : AANDB_NAME,
        "F" : CHV_NAME,
        "H" : CHP_NAME,
        "R" : MAS_NAME
    }[id]

def getValuesForKey(dictList, key, function=IDENTITY) :
    return [function(dictList[i][key]) for i in range(len(dictList))]

def getGender(genderString) :
    genderString = re.sub(r'[^\w\s]','', genderString).lower().replace("é", "e")

    if genderString in ["masculin", "homme", "male", "monsieur", "m", "Français", "français", "Francais", "francais"] :
        return "Male"
    elif genderString in ["feminin", "femme", "femelle", "madame", "mademoiselle", "f", "Française", "française", "Francaise", "francaise"] :
        return "Female"
    else :
        return "Other"

def getDaltonism(s) :
    if s.lower() == "oui" : return "Daltonian"
    else : return ""


FAMILIAR="Familiar"
NOT_FAMILIAR="Not familiar"

UNKNOWN="Unknown"
HEARD_ABOUT="Heard\nabout"
NEVER_USED="Never\nused"
WITHOUT_HEADSET="Without\nheadset"
WITH_HEADSET="With\nheadset"
WELL_KNOWN="Well\nknown"
    
def getMiniFamArFromInt(s) :
    return {
        1: NOT_FAMILIAR, # UNKNOWN,
        2: NOT_FAMILIAR, # HEARD_ABOUT,
        3: NOT_FAMILIAR, # NEVER_USED,
        4: FAMILIAR, # WITHOUT_HEADSET,
        5: FAMILIAR, # WITH_HEADSET,
        6: FAMILIAR, # WELL_KNOWN,
    }[s]
    
def getFamAr(s) :
    return {
        "Je ne connais pas cette technologie": 1,
        "J'en ai entendu parler mais sans plus": 2,
        "Je comprends ce que c'est mais je ne l'ai jamais utilisée": 3,
        "J'ai déjà utilisé la réalité augmentée mais sans casque": 4,
        "J'ai déjà utilisé un casque de réalité augmentée": 5,
        "Je connais très bien cette technologie": 6
    }[s]

VAGUE_IDEAS="Vague\nideas"
SOME_EXAMPLES="Some\nexamples"
DIVERSE_KNOWLEDGE="Diverse\nknowledge"

def getMiniFamMgFromInt(s) :
    return {
        1: NOT_FAMILIAR, # UNKOWN,
        2: NOT_FAMILIAR, # VAGUE_IDEAS,
        3: NOT_FAMILIAR, # SOME_EXAMPLES,
        4: FAMILIAR, # DIVERSE_KNOWLEDGE,
        5: FAMILIAR # WELL_KNOWN,
    }[s]

def getFamMg(s) :
    return {
        "Je n'ai jamais porté attention à comment étaient représentés les gestes": 1,
        "J'ai quelques exemples de représentations en tête": 2,
        "Je pourrais donner une certaine diversité d'exemples": 3,
        "Je vois bien comment représenter la gestuelle et ce dans différents domaines": 4,
        "Je suis très familier avec ces notions": 5
    }[s]

SD_ORDER=[SD, DS]
SD_ORDER_GUI=[ALL]+SD_ORDER
D_S = "d"
S_D = "s"
SD_EQUIVALENCES={D_S : DS, S_D : SD}
SD_CONDITIONS={ ALL: lambda x:True, 
                SD : lambda x:matchingSdOrder(x, SD), 
                DS : lambda x:matchingSdOrder(x, DS)}

EFFECT_TO_FOCUS = { ORDER : {x for x in SD_ORDER},
                    STATE : {x for x in STATES},
                    MICROGESTURE : {x for x in MICROGESTURES},
                    FAMILY : {x for x in SYMBOLS}}

def removeAssembledMgs(stateData) :
    stateData.pop(TAP, None)
    stateData.pop(SWIPE, None)
    stateData.pop(FLEX, None)
    stateData.pop(HOLD, None)
    return stateData

def matchingSdOrder(participant, order) :
    return SD_EQUIVALENCES[participant[SD_ORDER_KEY]] == order

def mg_treatment_utils_side(ar) :
    st = '_'.join(ar)
    return st

class RGB(np.ndarray):
  @classmethod
  def from_str(cls, rgbstr):
    return np.array([
      int(rgbstr[i:i+2], 16)
      for i in range(1, len(rgbstr), 2)
    ]).view(cls)
 
  def __str__(self):
    self = self.astype(np.uint8)
    return '#' + ''.join(format(n, 'x') for n in self)

def getKeyByValue(dictOfElements, valueToFind):
    for item in dictOfElements :
        if valueToFind in dictOfElements[item] :
            return item
    return None


palette = sns.color_palette("husl", 45).as_hex()
MORE_COLORS= {x : palette[x] for x in range(45)}
    
def mixColorsCombination(combination, cols) :
    d = {identifyColor(combination[k]):1-k/len(combination) for k in range(len(combination))}
    colors = [x for x in MORE_COLORS.values() if str(x).lower() not in cols]
    d_items = list(d.items())
    for i in range(len(d_items)) :
        if d_items[i][0]==None:
            c = colors.pop()
            d_items[i]=(c[1:], d_items[i][1])
    d_items = sorted(d_items)
    tot_weight = sum(d.values())
    red = int(sum([int(k[:2], 16)*v for k, v in d_items])/tot_weight)
    green = int(sum([int(k[2:4], 16)*v for k, v in d_items])/tot_weight)
    blue = int(sum([int(k[4:6], 16)*v for k, v in d_items])/tot_weight)
    zpad = lambda x: x if len(x)==2 else '0' + x
    return "#" + zpad(hex(red)[2:]) + zpad(hex(green)[2:]) + zpad(hex(blue)[2:])

DARK="#262626"

def darkenColor(col) :
    d = {col[1:]: 0.5, DARK[1:]: 0.5}
    d_items = sorted(d.items())
    tot_weight = sum(d.values())
    red = int(sum([int(k[:2], 16)*v for k, v in d_items])/tot_weight)
    green = int(sum([int(k[2:4], 16)*v for k, v in d_items])/tot_weight)
    blue = int(sum([int(k[4:6], 16)*v for k, v in d_items])/tot_weight)
    zpad = lambda x: x if len(x)==2 else '0' + x
    color = "#" + zpad(hex(red)[2:]) + zpad(hex(green)[2:]) + zpad(hex(blue)[2:])
    return color

def identifyColor(stri) :
    if isinstance(stri, int) :
        return MORE_COLORS[stri][1:]
    if stri in SD_ORDER :
        return ORDER_COLORS[stri][1:]
    if stri in STATES :
        return STATES_COLORS[stri][1:]
    if stri in MICROGESTURES :
        return MG_COLORS[stri][1:]
    if stri in FAM_COLORS :
        return FAM_COLORS[stri][1:]
    if stri in [FIRST_PHASE, THIRD_PHASE] :
        return PHASES_COLORS[stri][1:]
    else :
        return PREVIOUS_COLORS[stri][1:]

DEFAULT_MARKERS = [',', '.', 'o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X']*200

def identifyMark(str, i) :
    if str in FAM_MARKERS :
        return FAM_MARKERS[str]
    else :
        return DEFAULT_MARKERS[i]
    
def getCombination(colsToUnite, df, chariot=False) :
    combination = ' '.join(colsToUnite)
    df[combination] = df[colsToUnite].agg(lambda x : aggregate(x, chariot), axis=1)
    return combination, df

def aggregate(value, chariot) :
    if chariot : joint='\n'
    else : joint=' '
    strList = [str(x) for x in value.to_list()]
    return joint.join(strList)

def getDropped(dic) :
    """
    Fetch the dictionnary keys
    associated with a value of zero
    
    :param dic: a dictionnary with
    binary values
    :return: a list of keys
    """
    droppedKeys = []
    for key in dic :
        if not dic[key] :
            droppedKeys.append(key)
    return droppedKeys


def setIfElseUnset(el, values, value) :
    """
    Sets to 1 the value of an IntVar
    if the given value is in the given
    values. Sets to 0 otherwise.
    
    :param values: list of values
    to consider
    :param value: value to consider
    """
    if value in values: 
        el.set(1)
    else : 
        el.set(0)
    
def getSubstracted(valueList, *values) :
    """
    Returns valueList without the specified values
    
    :return: a list of values 
    """
    return [x for x in valueList if x not in values][0]