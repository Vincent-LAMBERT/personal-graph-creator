#!/usr/bin/env python

import sys, random

REP_STATE = ["S", "D"]
REP_FAMILY = ["A", "F", "H", "R"]
REP_MICROGESTURE = ["Tap", "Swipe", "Flex", "Hold"]
REP_STATIC_TAP = [REP_MICROGESTURE[0] + "_" + family + REP_STATE[0] for family in REP_FAMILY]
REP_DYNAMIC_TAP = [REP_MICROGESTURE[0] + "_" + family + REP_STATE[1] for family in REP_FAMILY]
REP_STATIC_SWIPE = [REP_MICROGESTURE[1] + "_" + family + REP_STATE[0] for family in REP_FAMILY]
REP_DYNAMIC_SWIPE = [REP_MICROGESTURE[1] + "_" + family + REP_STATE[1] for family in REP_FAMILY]
REP_STATIC_FLEX = [REP_MICROGESTURE[2] + "_" + family + REP_STATE[0] for family in REP_FAMILY]
REP_DYNAMIC_FLEX = [REP_MICROGESTURE[2] + "_" + family + REP_STATE[1] for family in REP_FAMILY]
REP_STATIC_HOLD = [REP_MICROGESTURE[3] + "_" + family + REP_STATE[0] for family in REP_FAMILY]
REP_DYNAMIC_HOLD = [REP_MICROGESTURE[3] + "_" + family + REP_STATE[1] for family in REP_FAMILY]
REP_TAP = REP_STATIC_TAP + REP_DYNAMIC_TAP
REP_SWIPE = REP_STATIC_SWIPE + REP_DYNAMIC_SWIPE
REP_FLEX = REP_STATIC_FLEX + REP_DYNAMIC_FLEX
REP_HOLD = REP_STATIC_HOLD + REP_DYNAMIC_HOLD
REPRESENTATIONS = REP_TAP + REP_SWIPE + REP_FLEX + REP_HOLD

class SeedRetriever :
    def __init__ (self) :
        self.orderPhase1 = REPRESENTATIONS.copy()
        self.orderPhase2_Microgesture = REP_MICROGESTURE.copy()
        self.orderPhase2_Family_Static_Tap = REP_STATIC_TAP.copy()
        self.orderPhase2_Family_Dynamic_Tap = REP_DYNAMIC_TAP.copy()
        self.orderPhase2_Family_Static_Swipe = REP_STATIC_SWIPE.copy()
        self.orderPhase2_Family_Dynamic_Swipe = REP_DYNAMIC_SWIPE.copy()
        self.orderPhase2_Family_Static_Flex = REP_STATIC_FLEX.copy()
        self.orderPhase2_Family_Dynamic_Flex = REP_DYNAMIC_FLEX.copy()
        self.orderPhase2_Family_Static_Hold = REP_STATIC_HOLD.copy()
        self.orderPhase2_Family_Dynamic_Hold = REP_DYNAMIC_HOLD.copy()
        self.orderPhase2 = []
        self.orderPhase3 = REPRESENTATIONS.copy()
        self.orderPhase5 = REPRESENTATIONS.copy()

    def retrieveListOrder(self, custom_seed, listToShuffle) :
        random.Random(custom_seed).shuffle(listToShuffle)
        return listToShuffle
    
    def getStateFromInput(self, input) :
        return {
            'S': "Static",
            'Static' : "Static",
            's' : "Static",
            'static' : "Static",
            'D': 'Dynamic',
            'Dynamic': 'Dynamic',
            'd': 'Dynamic',
            'dynamic': 'Dynamic',
        }[input]
    
    def getMicrogestureFromInput(self, input) :
        return {
            't': "Tap",
            'T': "Tap",
            'tap': "Tap",
            'Tap' : "Tap",
            'sw' : "Swipe",
            'Sw' : "Swipe",
            'swipe' : "Swipe",
            'Swipe' : "Swipe",
            'st': 'Flex',
            'St': 'Flex',
            'flex': 'Flex',
            'Flex': 'Flex',
            'h': 'Hold',
            'H': 'Hold',
            'hold': 'Hold',
            'Hold': 'Hold',
        }[input]
    
    def getListFromPhase2(self, state, microgesture) :
        return {
            '(Static, Tap)': self.orderPhase2_Family_Static_Tap,
            '(Static, Swipe)': self.orderPhase2_Family_Static_Swipe,
            '(Static, Flex)': self.orderPhase2_Family_Static_Flex,
            '(Static, Hold)': self.orderPhase2_Family_Static_Hold,
            '(Dynamic, Tap)': self.orderPhase2_Family_Dynamic_Tap,
            '(Dynamic, Swipe)': self.orderPhase2_Family_Dynamic_Swipe,
            '(Dynamic, Flex)': self.orderPhase2_Family_Dynamic_Flex,
            '(Dynamic, Hold)': self.orderPhase2_Family_Dynamic_Hold,
        }[str((state, microgesture))]
        

    def run(self) :
        print('Please, enter the seed : ')
        custom_seed = input()
        print('Please, enter the phase : ')
        phase = input()
        if phase==2 :
            listToShuffle = self.orderPhase1
        elif phase==3 :
            print('Please, enter the state : ')
            state = self.getStateFromInput(input())
            print('Please, enter the microgesture : ')
            microgesture = self.getMicrogestureFromInput(input())
            listToShuffle = self.getListForPhase2(state, microgesture)
        elif phase==3 :
            listToShuffle = self.orderPhase3
        else : 
            print('Phase not recognized, exiting...')
            exit
        print(self.retrieveListOrder(custom_seed, listToShuffle))
        
    def p1Seed(self, seed) :
        listToShuffle = self.orderPhase1
        return self.retrieveListOrder(seed, listToShuffle)
        
    def p2Seed(self, seed, state, microgesture) :
        listToShuffle = self.getListForPhase2(state, microgesture)
        return self.retrieveListOrder(seed, listToShuffle)
		
def launch(args) :
	seedRetriever = SeedRetriever()
	seedRetriever.run(sys.argv[1:])

if __name__ == "__main__" :
	launch(sys.argv[1:])