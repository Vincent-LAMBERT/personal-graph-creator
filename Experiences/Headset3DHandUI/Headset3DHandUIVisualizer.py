#!/usr/bin/env python3

import itertools
import tkinter as tk

import pandas as pd
from matplotlib.figure import Figure

from App.DataVisualizer import COVERAGE_PERCENTS, DataVisualizer
from App.DrawUI import *
from App.Utils import *
from Experiences.Headset3DHandUI.Utils.GeneralUtils import *


class Headset3DHandUIVisualizer (DataVisualizer) :
    def __init__ (self, GUI, processor) :
        super().__init__(GUI, processor)
        self.imageOrient = tk.StringVar()
        self.graphTypeCombobox = tk.StringVar()
        self.orderCombobox = tk.StringVar()
        self.dataCombobox = tk.StringVar()
        self.dataValueCombobox = tk.StringVar()
        self.phaseCombobox = tk.StringVar()
        self.seeParameters = tk.IntVar()
        self.seeFamilies = tk.IntVar()
        self.seeMovements = tk.IntVar()
        self.checkAllMovements = tk.IntVar()
        self.dataFocus = tk.StringVar()
        self.effectFocus = tk.StringVar()
        self.infoFocus = tk.StringVar()
        self.previous = tk.IntVar()
        self.notPrevious = tk.IntVar()
        self.sd = tk.IntVar()
        self.ds = tk.IntVar()
        self.static = tk.IntVar()
        self.dynamic = tk.IntVar()
        self.firstPhase = tk.IntVar()
        self.thirdPhase = tk.IntVar()
        self.tap = tk.IntVar()
        self.swipe = tk.IntVar()
        self.flex = tk.IntVar()
        self.hold = tk.IntVar()
        self.checkAllMicrogestures = tk.IntVar()
        self.checkAllFamilies = tk.IntVar()
        self.allOrders = tk.IntVar()
        self.allPrevious = tk.IntVar()
        self.allStates = tk.IntVar()
        self.allMicrogestures = tk.IntVar()
        self.allPhases = tk.IntVar()
        self.allFamilies = tk.IntVar()
        self.axisInvert = tk.IntVar()
        self.xInvert = tk.IntVar()
        self.yInvert = tk.IntVar()
        self.styleInvert = tk.IntVar()
        self.scaleBefore = tk.IntVar()
        self.scaleAfter = tk.IntVar()
        self.scaleSize = tk.IntVar()
        self.coverageCombobox = tk.IntVar()
        self.absissesCombobox = tk.StringVar()

        self.aandb = tk.IntVar()
        self.dpa = tk.IntVar()
        self.dea = tk.IntVar()
        self.ba = tk.IntVar()
        self.at = tk.IntVar()
        self.chv = tk.IntVar()
        self.atc = tk.IntVar()
        self.chp = tk.IntVar()
        self.hiz = tk.IntVar()
        self.hib = tk.IntVar()
        self.dsl = tk.IntVar()
        self.dsb = tk.IntVar()
        self.cws = tk.IntVar()
        self.cns = tk.IntVar()
        self.mst = tk.IntVar()
        self.eml = tk.IntVar()
        self.ri = tk.IntVar()
        self.mas = tk.IntVar()
        self.elc = tk.IntVar()
        self.gra = tk.IntVar()
        self.tar = tk.IntVar()

        self.explicitnessFocus = tk.StringVar()
        self.explicitnessStringFocus = tk.IntVar()
        self.explicitnessValueFocus = tk.IntVar()
        self.trajectoryFocus = tk.IntVar()
        self.directionFocus = tk.IntVar()
        self.fingerFocus = tk.IntVar()
        self.returnFocus = tk.IntVar()
        self.touchPointFocus = tk.IntVar()
        self.aisFocus = tk.IntVar()
        self.ddFocus = tk.IntVar()
        self.movingPartFocus = tk.IntVar()
        self.pauseFocus = tk.IntVar()
        self.natureFocus = tk.IntVar()

        self.familyNbr = tk.IntVar()

        self.wrapCanvas = tk.IntVar()
        self.userAggregate = tk.IntVar()
        self.showPoints = tk.IntVar()
        self.chariot = tk.IntVar()
        self.scaleTop = tk.IntVar()
        
        self.xLimInf = tk.IntVar()
        self.xLimSup = tk.IntVar()
        
        self.arUnknown = tk.IntVar()
        self.arHeardAbout = tk.IntVar()
        self.arNeverUsed = tk.IntVar()
        self.arWithoutHeadset = tk.IntVar()
        self.arWithHeadset = tk.IntVar()
        self.arWellKnown = tk.IntVar()
        self.mgUnknown = tk.IntVar()
        self.mgVagueIdeas = tk.IntVar()
        self.mgSomeExamples = tk.IntVar()
        self.mgDiverseKnowledge = tk.IntVar()
        self.mgWellKnown = tk.IntVar()
        
        self.stackFocus = tk.StringVar()
        self.hashFocus = tk.StringVar()
        self.clusterFocus = tk.StringVar()
        self.perfStates = tk.StringVar()
        self.perfPhases = tk.StringVar()
        self.perfMicrogestures = tk.StringVar()
        self.perfFamilies = tk.StringVar()
        self.hashInvert = tk.IntVar()
        self.clusterInvert = tk.IntVar()

        self.imageOrient.set(ORIENTS[0])
        self.graphTypeCombobox.set(GRAPH_TYPES_P2[0])
        self.orderCombobox.set(SD_ORDER[0])
        self.dataCombobox.set(GENDER)
        self.dataValueCombobox.set(PERCENTAGE)
        self.phaseCombobox.set(STRING_PHASES[0])
        self.seeParameters.set(0)
        self.seeFamilies.set(0)
        self.seeMovements.set(0)
        self.checkAllMovements.set(1)
        self.dataFocus.set(DATA_TO_FOCUS[-1])
        self.effectFocus.set(next(iter(EFFECT_TO_FOCUS[FAMILY])))
        self.previous.set(1)
        self.notPrevious.set(1)
        self.sd.set(1)
        self.ds.set(1)
        self.static.set(1)
        self.firstPhase.set(1)
        self.thirdPhase.set(1)
        self.dynamic.set(1)
        self.tap.set(1)
        self.swipe.set(1)
        self.flex.set(1)
        self.hold.set(1)
        self.checkAllMicrogestures.set(1)
        self.checkAllFamilies.set(1)
        self.allOrders.set(1)
        self.allPrevious.set(1)
        self.allStates.set(1)
        self.allMicrogestures.set(1)
        self.allPhases.set(1)
        self.allFamilies.set(0)
        self.axisInvert.set(0)
        self.xInvert.set(0)
        self.yInvert.set(0)
        self.styleInvert.set(0)
        self.scaleBefore.set(0)
        self.scaleAfter.set(0)
        self.scaleSize.set(150)
        self.coverageCombobox.set(50)
        self.absissesCombobox.set(STDEV)

        self.aandb.set(1)
        self.dpa.set(1)
        self.dea.set(1)
        self.ba.set(1)
        self.at.set(1)
        self.chv.set(1)
        self.atc.set(1)
        self.chp.set(1)
        self.hiz.set(1)
        self.hib.set(1)
        self.dsl.set(1)
        self.dsb.set(1)
        self.cws.set(1)
        self.cns.set(1)
        self.mst.set(1)
        self.eml.set(1)
        self.ri.set(1)
        self.mas.set(1)
        self.elc.set(1)
        self.gra.set(1)
        self.tar.set(1)

        self.explicitnessFocus.set("String")
        self.explicitnessStringFocus.set(1)
        self.explicitnessValueFocus.set(1)
        self.trajectoryFocus.set(1)
        self.directionFocus.set(0)
        self.fingerFocus.set(0)
        self.returnFocus.set(0)
        self.touchPointFocus.set(0)
        self.aisFocus.set(0)
        self.ddFocus.set(0)
        self.movingPartFocus.set(0)
        self.pauseFocus.set(0)
        self.natureFocus.set(0)

        self.familyNbr.set(0)
        self.wrapCanvas.set(0)
        self.userAggregate.set(0)
        self.showPoints.set(1)
        self.chariot.set(0)
        self.scaleTop.set(3)
        self.graphType = None
        self.xLimInf.set(0)
        self.xLimSup.set(0)
        
        self.arUnknown.set(1)
        self.arHeardAbout.set(1)
        self.arNeverUsed.set(1)
        self.arWithoutHeadset.set(1)
        self.arWithHeadset.set(1)
        self.arWellKnown.set(1)
        self.mgUnknown.set(1)
        self.mgVagueIdeas.set(1)
        self.mgSomeExamples.set(1)
        self.mgDiverseKnowledge.set(1)
        self.mgWellKnown.set(1)
        self.stackFocus.set(FAMILY)
        self.hashFocus.set(PHASE)
        self.clusterFocus.set(STATE)
        self.perfStates.set(STATIC)
        self.perfPhases.set(FIRST_PHASE)
        self.perfMicrogestures.set(TAP)
        self.perfFamilies.set(AANDB_NAME)
        self.hashInvert.set(0)
        self.clusterInvert.set(0)

        # pd.set_option('display.max_rows', None)
        
        self.actualizeVisualizerGUI()

        self.actualizePlot()
    
    
    def getSelectedStates(self) :
        """
        Returns the checked states
        
        :return: list of states
        """
        states = []
        if self.static.get()!=0 : states.append(STATIC)
        if self.dynamic.get()!=0 : states.append(DYNAMIC)
        return states
    
    def getSelectedMgs(self) :
        """
        Returns the checked microgestures
        
        :return: list of microgestures
        """
        mgs = []
        if self.tap.get()!=0 : mgs.append(TAP)
        if self.swipe.get()!=0 : mgs.append(SWIPE)
        if self.flex.get()!=0 : mgs.append(FLEX)
        if self.hold.get()!=0 : mgs.append(HOLD)
        return mgs
    
    def getCombinedStates(self) :
        """
        Returns the combined states
        
        :return: list of lists of states
        """
        selectedStates = self.getSelectedStates()
        
        combinedStates = []
        for i in range(1, len(selectedStates)+1) :
            for x in itertools.combinations(selectedStates, i) :
                combinedStates.append(x)
        return [[y for y in x] for x in combinedStates]
    
    def getCombinedMgs(self) :
        """
        Returns the combined
        microgestures
        
        :return: list of lists of
        microgestures
        """
        selectedMgs = self.getSelectedMgs()
        combinedMgs = []
        for i in range(1, len(selectedMgs)+1) :
            for x in itertools.combinations(selectedMgs, i) :
                combinedMgs.append(x)
        return [[y for y in x] for x in combinedMgs]
    
    def updateStates(self, states) :
        """
        Activate the checkboxes
        of the states in the parameter 
        "states". Desactivate the others
        
        :param states: list of states
        to consider
        """
        setIfElseUnset(self.static, states, STATIC)
        setIfElseUnset(self.dynamic, states, DYNAMIC)
        
    def getDroppedPrevious(self) :
        """
        Returns the previous to drop
        """
        droppedPrevious={PREVIOUS_DONE : self.previous.get(), 
                         PREVIOUS_NOT_DONE : self.notPrevious.get()}
        return getDropped(droppedPrevious)
        
    def getDroppedOrders(self) :
        """
        Returns the orders to drop
        
        :return: a list of orders
        """
        orders={SD : self.sd.get(), 
                DS : self.ds.get()}
        return getDropped(orders)
    
    def getDroppedStates(self) :
        """
        Returns the states to drop
        
        :return: a list of states
        """
        states={STATIC : self.static.get(), 
                DYNAMIC : self.dynamic.get()}
        return getDropped(states)
    
    def getDroppedPhases(self) :
        """
        Returns the states to drop
        
        :return: a list of states
        """
        phases={FIRST_PHASE : self.firstPhase.get(), 
                THIRD_PHASE : self.thirdPhase.get()}
        return getDropped(phases)

    def getDroppedMgs(self) :
        """
        Returns the microgestures to drop
        
        :return: a list of microgestures
        """
        mgs={TAP : self.tap.get(), 
             SWIPE : self.swipe.get(), 
             FLEX : self.flex.get(), 
             HOLD : self.hold.get()}
        return getDropped(mgs)

    def getDroppedFamilies(self) :
        """
        Returns the families to drop
        
        :return: a list of families
        """
        fams={AANDB_NAME : self.aandb.get(), 
              CHV_NAME : self.chv.get(), 
              CHP_NAME : self.chp.get(),
              MAS_NAME : self.mas.get()}
        return getDropped(fams)

###########################################################################
#######                      ACTUALIZATIONS                       #########
###########################################################################

    def actualizeVisualizerGUI(self) :
        """
        Actualize the MenuFrame
        """
        for widget in self.GUI.MenuFrame.winfo_children():
            widget.destroy()

        self.familyNbr.set(FAMILY_NBR-1)
        drawLabel(self.GUI.MenuFrame, "Preset", side=LEFT)
        self.presetsNames = self.getPresetsNames()
        combobox = drawCombobox(self.GUI.MenuFrame, self.presetsCombobox, self.presetsNames, function=self.actualizePreset, side=RIGHT)
        drawButton(self.GUI.MenuFrame, "Save", function=self.savePreset, size=11, side=LEFT)
        drawButton(self.GUI.MenuFrame, "Delete", function=self.removePreset, size=11, side=RIGHT)
        drawLabel(self.GUI.MenuFrame, "Phase", side=LEFT)
        drawCombobox(self.GUI.MenuFrame, self.phaseCombobox, STRING_PHASES, function=self.actualizeVisualizerGUI, current=self.phaseCombobox.get(), side=RIGHT)
        drawLabel(self.GUI.MenuFrame, "Data", side=LEFT)
        drawCombobox(self.GUI.MenuFrame, self.graphTypeCombobox, self.getGraphTypeFocus(), function=self.actualizeDataFrameUI, current=self.getCurrentPhaseFocus(self.graphTypeCombobox.get(), self.getGraphTypeFocus()), side=RIGHT)
        
        self.DataFrameUI = drawFrame(self.GUI.MenuFrame)
        self.DataFrameUI.grid_columnconfigure(0, weight=1, uniform="fred")
        self.DataFrameUI.grid_columnconfigure(1, weight=1, uniform="fred")
        
        self.actualizeDataFrameUI()
    
    def getGraphTypeFocus(self) :
        """
        Returns the column to focus on in the dataframe
        
        :return: The column to focus on
        """
        if self.phaseCombobox.get()==PERFORMANCES : return GRAPH_TYPES_P1_P3
        return GRAPH_TYPES_P2

    def getCurrentPhaseFocus(self, data, datalist):
        """
        Returns the data corresponding to the
        focused column in the dataframe
        
        :return: the focused data
        """
        if data not in datalist :
            data = datalist[0]
        return data

    def actualizeDataFrameUI(self) :
        """
        Actualize the DataFrameUI
        """
        graphType = self.graphTypeCombobox.get()

        self.cleanDataFrameUI()
        self.drawImageWrap()

        if graphType==PIE :
            self.drawPieUI()
        elif graphType==STRIP :
            self.drawStripUI()
        elif graphType==BOX :
            self.drawBoxUI()
        elif graphType==VIOLIN :
            self.drawBarUI()
        elif graphType==BAR :
            self.drawBarUI()
        elif graphType==CLUSTERED_STACKED_BAR :
            self.drawClusteredStackedBarUI()
        elif graphType==SCATTER :
            self.drawScatterUI()
        elif graphType==ELLIPSES:
            self.drawEllipsesUI()
        elif graphType==MULTIPLE_BARS :
            self.drawMultipleUI()
        elif graphType==MULTIPLE_LINES :
            self.drawMultipleUI()
        elif graphType==MULTIPLE_BOXES :
            self.drawMultipleUI()
        elif graphType==MULTIPLE_VIOLIN :
            self.drawMultipleUI()
        elif graphType==EFFECT :
            self.drawEffectSizeUI()
        elif graphType==ORDERING :
            self.drawOrderingUI()
        elif graphType==REGPLOT :
            self.drawRegplotUI()
        else :
            pass
        self.graphType = graphType
        
    def actualizePlot(self):
        """
        Actualize the GraphFrame
        """
        self.prepareVisualize()
        graphType = self.graphTypeCombobox.get()

        if graphType==PIE :
            self.actualizePie()
        elif graphType==STRIP:
            self.actualizeStrip()
        elif graphType==BOX :
            self.actualizeBox()
        elif graphType==VIOLIN :
            self.actualizeViolin()
        elif graphType==BAR :
            self.actualizeBar()
        elif graphType==CLUSTERED_STACKED_BAR :
            self.actualizeClusteredStackedBar()
        elif graphType==SCATTER :
            self.actualizeScatter()
        elif graphType==ELLIPSES:
            self.actualizeEllipses()
        elif graphType==MULTIPLE_BARS :
            self.actualizeMultipleBars()
        elif graphType==MULTIPLE_LINES :
            self.actualizeMultipleLines()
        elif graphType==MULTIPLE_BOXES :
            self.actualizeMultipleBoxes()
        elif graphType==MULTIPLE_VIOLIN :
            self.actualizeMultipleViolin()
        elif graphType==EFFECT :
            self.actualizeEffectSize()
        elif graphType==ORDERING :
            self.actualizeOrdering()
        elif graphType==REGPLOT :
            self.actualizeRegplot()
        else :
            pass
        
        self.applyLegendModifier()
        self.actualizeCanvas()
    
    def applyLegendModifier(self) :
        """
        Remove (or not) the graph legend
        """
        if self.showLegend.get() == False :
            self.axis.get_legend().remove()

    def actualizePie(self) :
        """
        Replace the plot canvas with a pie chart
        """
        data = self.dataCombobox.get()
        dataValue = self.dataValueCombobox.get()

        labels, values = self.processor.getInfoState(data)
        print(data)
        self.drawPie(data, values, labels, dataValue)
    
    def actualizeBar(self) :
        """
        Replace the plot canvas with a bar plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawBars(title, colToFocus, MEAN, df, order=hueOrder, palette=hueColors, yLabels = self.getYLabels(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())
    
    def actualizeClusteredStackedBar(self) :
        """
        Replace the plot canvas with a stacked bar plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        stack = self.stackFocus.get()
        hash = self.hashFocus.get()
        cluster = self.clusterFocus.get()
        droppedColumn = getSubstracted(PERFORMANCE_COLUMNS, stack, hash, cluster)
        if droppedColumn==FAMILY: keptValue = self.perfFamilies.get()
        elif droppedColumn==PHASE: keptValue = self.perfPhases.get()
        elif droppedColumn==STATE: keptValue = self.perfStates.get()
        else : keptValue = self.perfMicrogestures.get()
        
        self.drawClusteredStackedBars("Clustered Stacked Bars", df, droppedColumn, keptValue, stack, hash, cluster, hashInvert=self.hashInvert.get(), clusterInvert=self.clusterInvert.get())
        
    def getYLabels(self) :
        """
        Returns the y labels depending on the phase
        """
        if self.phaseCombobox.get()!=PERFORMANCES :
            return SCORES
        else :
            return None
        
        
    def actualizeStrip(self) :
        """
        Replace the plot canvas with a box plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        
        self.drawStrip(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels = self.getYLabels(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeBox(self) :
        """
        Replace the plot canvas with a box plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        
        self.drawBox(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels = self.getYLabels(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get(), showPoints=self.showPoints.get())

    def actualizeViolin(self):
        """
        Replace the plot canvas with a violin plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawViolin(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels = self.getYLabels(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeScatter(self):
        """
        Replace the plot canvas with a scatter plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        styleOrder, styleColors, df = self.processor.orderData(combination, colsToUnite, df)
        hueOrder, hueColors = self.processor.getColorsFor(colToFocus, df)
        markers = self.processor.getMarkersFor(combination, df)

        title="{0} for {1}".format(colToFocus, combination)
        self.drawScatter(title, self.absissesCombobox.get(), MEAN, colToFocus, combination, markers,
                df, hueOrder, hueColors, styleOrder, size=self.scaleSize.get(), yLabels = self.getYLabels(), axisInvert=self.axisInvert.get(), invertY=self.yInvert.get())

    def actualizeEllipses(self):
        """
        Replace the plot canvas with a scatter plot and its bivariate ellipses
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        styleOrder, styleColors, df = self.processor.orderData(combination, colsToUnite, df)
        hueOrder, hueColors = self.processor.getColorsFor(colToFocus, df)

        title="{0} for {1}".format(colToFocus, combination)
        self.drawBivariateEllipses(title, self.absissesCombobox.get(), MEAN, colToFocus, combination, float(self.coverageCombobox.get()),
            df, hueOrder, hueColors, styleOrder, size=self.scaleSize.get(), yLabels = self.getYLabels(), axisInvert=self.axisInvert.get(), invertY=self.yInvert.get())

    def actualizeMultipleBars(self):
        """
        Replace the plot canvas with a multiple bars plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        if PARTICIPANT_ID in colsToUnite :
            colsToUnite.remove(PARTICIPANT_ID)
            combination, df = getCombination(colsToUnite, df, self.chariot.get())
        hueOrder, hueColors, df = self.processor.orderData(combination, colsToUnite, df)

        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawMultipleBars(title, colToFocus, MEAN, combination, df, hueOrder, hueColors, yLabels = self.getYLabels(), axisInvert=self.axisInvert.get(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeMultipleLines(self):
        """
        Replace the plot canvas with a multiple lines plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        hueOrder, hueColors, df = self.processor.orderData(combination, colsToUnite, df)

        title="{0} for {1}".format(colToFocus, combination)
        self.drawMultipleLines(title, colToFocus, MEAN, combination, df, hueOrder, hueColors, yLabels = self.getYLabels(), axisInvert=self.axisInvert.get(), invertX=self.xInvert.get(), invertY=self.yInvert.get())

    def actualizeMultipleBoxes(self):
        """
        Replace the plot canvas with a multiple boxes plot
        """
        StartingColToFocus, combination, colsToUnite, df = self.processor.getDataState()
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[StartingColToFocus], df, self.chariot.get())
        if PARTICIPANT_ID in colsToUnite :
            colsToUnite.remove(PARTICIPANT_ID)
            colsToUnite.append(StartingColToFocus)
        hueOrder, hueColors, df = self.processor.orderData(colToFocus, colsToUnite, df)

        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawBox(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels = self.getYLabels(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get(), showPoints=self.showPoints.get())

    def actualizeMultipleViolin(self) :
        """
        Replace the plot canvas with a multiple violins plot
        """
        StartingColToFocus, combination, colsToUnite, df = self.processor.getDataState()
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[StartingColToFocus], df, self.chariot.get())
        if PARTICIPANT_ID in colsToUnite :
            colsToUnite.remove(PARTICIPANT_ID)
            colsToUnite.append(StartingColToFocus)
        hueOrder, hueColors, df = self.processor.orderData(colToFocus, colsToUnite, df)

        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawViolin(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels = self.getYLabels(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeEffectSize(self) :
        """
        Replace the plot canvas with a plot of effect sizes with mustaches
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[colToFocus], df, self.chariot.get())
        
        focus = self.effectFocus.get()
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawEffectSize(title, colToFocus, MEAN, df, focus, isImage, orient=self.imageOrient.get())

    def actualizeOrdering(self) :
        """
        Prints in the integrated terminal the top families orderings
        """
        df = self.processor.getDataOrderings()
        self.GUIprint(df.to_string())
    
    def actualizeRegplot(self) :
        """
        Replace the plot canvas with a linear regression plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        
        if self.xLimInf.get() : xLimInf=0.045 
        else : xLimInf=False
        if self.xLimSup.get() : xLimSup=1.125 
        else : xLimSup=False
        
        self.drawRegplot(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, axisInvert=self.axisInvert.get(), invertY=self.yInvert.get(), xLimInf=xLimInf, xLimSup=xLimSup)

    def cleanDataFrameUI(self) :
        """
        Clear the DataFrameUI
        """
        for widget in self.DataFrameUI.winfo_children():
            widget.destroy()


###########################################################################
#######                          PRESETS                          #########
###########################################################################

    def actualizePreset(self) :
        """
        Apply the current preset to both the UI and the plot canvas
        """
        self.cleanDataFrameUI()
        presetName = self.presetsCombobox.get()
        if presetName!='':
            preset = self.getNamedPreset(presetName)
            preset.reverse()
            self.setPreset(preset)
            graphType = self.graphTypeCombobox.get()
            self.drawImageWrap()

            if graphType==PIE :
                self.drawPieUI()
            elif graphType==STRIP :
                self.drawStripUI()
            elif graphType==BOX :
                self.drawBoxUI()
            elif graphType==VIOLIN :
                self.drawBarUI()
            elif graphType==BAR :
                self.drawBarUI()
            elif graphType==CLUSTERED_STACKED_BAR :
                self.drawClusteredStackedBarUI()
            elif graphType==SCATTER :
                self.drawScatterUI()
            elif graphType==ELLIPSES:
                self.drawEllipsesUI()
            elif graphType==MULTIPLE_BARS :
                self.drawMultipleUI()
            elif graphType==MULTIPLE_LINES :
                self.drawMultipleUI()
            elif graphType==MULTIPLE_BOXES :
                self.drawMultipleUI()
            elif graphType==MULTIPLE_VIOLIN :
                self.drawMultipleUI()
            elif graphType==EFFECT :
                self.drawEffectSizeUI()
            elif graphType==ORDERING :
                self.drawOrderingUI()
            elif graphType==REGPLOT :
                self.drawRegplotUI()
        
            self.actualizePlot()

    def createPreset(self, name) :
        """
        Create a preset
        
        :param name: preset name
        :return: list of the preset values 
        with a preceeding preset name
        """
        return [name] + self.getPreset()

    def preset(self) :
        """
        Returns the widgets variable to create a preset
        
        :return: list of variables
        """
        return [self.graphTypeCombobox, 
                self.orderCombobox,
                self.dataCombobox,
                self.dataValueCombobox, 
                self.phaseCombobox,
                self.seeParameters, 
                self.seeFamilies,
                self.dataFocus,
                self.effectFocus,
                self.infoFocus,
                self.previous,
                self.notPrevious,
                self.sd, 
                self.ds, 
                self.static, 
                self.dynamic, 
                self.tap, 
                self.swipe, 
                self.flex, 
                self.hold,
                self.checkAllMicrogestures, 
                self.checkAllFamilies, 
                self.allOrders, 
                self.allPrevious,
                self.allStates, 
                self.allMicrogestures, 
                self.allFamilies, 
                self.axisInvert, 
                self.xInvert, 
                self.yInvert, 
                self.styleInvert, 
                self.scaleBefore, 
                self.scaleAfter, 
                self.scaleSize, 
                self.coverageCombobox, 
                self.aandb, 
                self.dpa, 
                self.dea, 
                self.ba, 
                self.at, 
                self.chv, 
                self.atc, 
                self.chp, 
                self.hiz, 
                self.hib, 
                self.dsl, 
                self.dsb, 
                self.cws, 
                self.cns, 
                self.mst, 
                self.eml, 
                self.ri, 
                self.mas, 
                self.elc, 
                self.gra, 
                self.tar,
                self.trajectoryFocus, 
                self.directionFocus, 
                self.fingerFocus, 
                self.familyNbr,
                self.seeMovements,
                self.checkAllMovements,
                self.wrapCanvas,
                self.xWrap,
                self.yWrap,
                self.widthWrap,
                self.heightWrap,
                self.imageOrient,
                self.returnFocus,
                self.touchPointFocus,
                self.aisFocus,
                self.ddFocus,
                self.movingPartFocus,
                self.pauseFocus,
                self.natureFocus,
                self.userAggregate,
                self.showPoints,
                self.chariot,
                self.scaleTop,
                self.explicitnessStringFocus,
                self.explicitnessValueFocus,
                self.explicitnessFocus,
                self.absissesCombobox,
                self.xLimInf,
                self.xLimSup,
                self.arUnknown,
                self.arHeardAbout,
                self.arNeverUsed,
                self.arWithoutHeadset,
                self.arWithHeadset,
                self.arWellKnown,
                self.mgUnknown,
                self.mgVagueIdeas,
                self.mgSomeExamples,
                self.mgDiverseKnowledge,
                self.mgWellKnown,
                self.stackFocus,
                self.hashFocus,
                self.clusterFocus,
                self.perfStates,
                self.perfPhases,
                self.perfMicrogestures,
                self.perfFamilies,
                self.hashInvert,
                self.clusterInvert]

    def getPreset(self) :
        """
        Returns the values associated to the current preset variables
        
        :return: list of values
        """
        return [x.get() for x in self.preset()]

    def setPreset(self, preset) :
        """
        Associates the values to the current preset variables
        
        :param preset: values to set on the variables
        """
        for x in self.preset() :
            x.set(preset.pop())

###########################################################################
#######                          DRAWINGS                         #########
###########################################################################
    
    def drawImageWrap(self) :
        """
        Adds the image wrapping buttons to the DataFrameUI
        
        Allows to move and flex the plot canvas
        """
        drawCheckButton(self.DataFrameUI, self.wrapCanvas, "Wrap canvas", function=self.actualizeDataFrameUI, checked=self.wrapCanvas.get(), side=LEFT)

        if self.wrapCanvas.get()!=0 :
            drawLabel(self.DataFrameUI, "xWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.xWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.xWrap.get())
            drawLabel(self.DataFrameUI, "yWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.yWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.yWrap.get())
            drawLabel(self.DataFrameUI, "heightWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.heightWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.heightWrap.get())
            drawLabel(self.DataFrameUI, "widthWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.widthWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.widthWrap.get())

    def drawCombineUI(self, cond=True) :
        """
        Adds the aggregation buttons to the DataFrameUI
        
        Allows to aggregate the values in the dataframe
        """
        drawLabel(self.DataFrameUI, "Combine all ...", side=SPAN)
        if self.graphTypeCombobox.get()!=CLUSTERED_STACKED_BAR :
            drawCheckButton(self.DataFrameUI, self.userAggregate, "Users", function=self.actualizeDataFrameUI, checked=self.userAggregate.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.allPrevious, "Previous", function=self.actualizeDataFrameUI, checked=self.allPrevious.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.allStates, "States", function=self.actualizeDataFrameUI, checked=self.allStates.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.allOrders, "Orders", function=self.actualizeDataFrameUI, checked=self.allOrders.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.allMicrogestures, "Microgestures", function=self.actualizeDataFrameUI, checked=self.allMicrogestures.get(), side=LEFT)
            if self.phaseCombobox.get()==PERFORMANCES :
                drawCheckButton(self.DataFrameUI, self.allPhases, "Phases", function=self.actualizeDataFrameUI, checked=self.allPhases.get(), side=RIGHT)
        else : 
            self.userAggregate.set(1)
            self.allPrevious.set(0)
            self.allStates.set(0)
            self.allOrders.set(1)
            self.allMicrogestures.set(0)
            self.allPhases.set(0)
        if cond :
            drawCheckButton(self.DataFrameUI, self.allFamilies, "Families", function=self.actualizeDataFrameUI, checked=self.allFamilies.get(), side=RIGHT)

    def drawPieUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the pie chart
        """
        drawLabel(self.DataFrameUI, "Combine all ...", side=LEFT)
        drawCheckButton(self.DataFrameUI, self.allOrders, "Orders", function=self.actualizeDataFrameUI, checked=False, side=RIGHT)

        if self.allOrders.get()==0 :
            drawLabel(self.DataFrameUI, "Order", side=LEFT)
            drawCombobox(self.DataFrameUI, self.orderCombobox, SD_ORDER, side=RIGHT, current=self.orderCombobox.get())
        
        drawCombobox(self.DataFrameUI, self.dataCombobox, [GENDER], current=self.dataCombobox.get())
        drawCombobox(self.DataFrameUI, self.dataValueCombobox, [QUANTITY, PERCENTAGE], current=self.dataValueCombobox.get())
    
    def getDataToFocus(self) :
        """
        Returns the column to focus on in the dataframe
        
        :return: The column to focus on
        """
        dataToFocus = DATA_TO_FOCUS.copy()
        if self.userAggregate.get()!=0 : dataToFocus.remove(PARTICIPANT_ID)
        if self.allOrders.get()!=0 : dataToFocus.remove(ORDER)
        if self.allPrevious.get()!=0 : dataToFocus.remove(PREVIOUS_STUDY)
        if self.allStates.get()!=0 : dataToFocus.remove(STATE)
        if self.allMicrogestures.get()!=0 : dataToFocus.remove(MICROGESTURE)
        if self.allFamilies.get()!=0 : dataToFocus.remove(FAMILY)
        return dataToFocus

    def getStackFocus(self) :
        """
        Returns the column to use as stack in the dataframe
        
        :return: The column to use as stack
        """
        return [x for x in PERFORMANCE_COLUMNS]      

    def getHashFocus(self) :
        """
        Returns the column to use as hash in the dataframe
        
        :return: The column to use as hash
        """
        return [x for x in PERFORMANCE_COLUMNS if x!=self.stackFocus.get() and x!=self.clusterFocus.get()]    

    def getClusterFocus(self) :
        """
        Returns the column to use as cluster in the dataframe
        
        :return: The column to use as cluster
        """
        return [x for x in PERFORMANCE_COLUMNS if x!=self.stackFocus.get() and x!=self.hashFocus.get()]      

    def getCurrentFocusInList(self, data, datalist):
        """
        Returns the data corresponding to the
        focused column in the dataframe
        
        :return: the focused data
        """
        if data not in datalist :
            data = datalist[0]
        return data
    
    def getEffectFocus(self) :
        """
        Returns the combinations of columns on which the
        effect size can be focused on
        
        :return: a list of combinations on which we can focus
        our effect size analysis
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState()
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[colToFocus], df, self.chariot.get())
        return df[colToFocus].unique().tolist()  
    
    def drawDataFocus(self) :
        """
        Adds to the DataFrameUI the data focus combobox
        """
        if self.graphTypeCombobox.get()!=CLUSTERED_STACKED_BAR :
            drawLabel(self.DataFrameUI, "Data focus", side=LEFT)
            drawCombobox(self.DataFrameUI, self.dataFocus, self.getDataToFocus(), function=self.actualizeDataFrameUI, current=self.getCurrentFocusInList(self.dataFocus.get(), self.getDataToFocus()), side=RIGHT)
        else : 
            drawLabel(self.DataFrameUI, "Data focus", side=SPAN)
            drawLabel(self.DataFrameUI, "Stack", side=LEFT)
            drawCombobox(self.DataFrameUI, self.stackFocus, self.getStackFocus(), function=self.actualizeDataFrameUI, current=self.getCurrentFocusInList(self.stackFocus.get(), self.getStackFocus()), side=RIGHT)
            drawLabel(self.DataFrameUI, "Hash", side=LEFT)
            drawCombobox(self.DataFrameUI, self.hashFocus, self.getHashFocus(), function=self.actualizeDataFrameUI, current=self.getCurrentFocusInList(self.hashFocus.get(), self.getHashFocus()), side=RIGHT)
            drawLabel(self.DataFrameUI, "Cluster", side=LEFT)
            drawCombobox(self.DataFrameUI, self.clusterFocus, self.getClusterFocus(), function=self.actualizeDataFrameUI, current=self.getCurrentFocusInList(self.clusterFocus.get(), self.getClusterFocus()), side=RIGHT)
        
    def drawStripUI(self):
        """
        Adds to the DataFrameUI the buttons relative to the strip plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)
        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=LEFT)
            drawCombobox(self.DataFrameUI, self.estimator, list(ESTIMATORS.keys()), current=self.estimator.get(), side=RIGHT)
            drawCombobox(self.DataFrameUI, self.searchedValue, SEARCHED_VALUES, current=self.searchedValue.get(), side=SPAN)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
    
    def drawBoxUI(self):
        """
        Adds to the DataFrameUI the buttons relative to the box plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)
        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showPoints, "Show points", checked=self.showPoints.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=LEFT)
            drawCombobox(self.DataFrameUI, self.estimator, list(ESTIMATORS.keys()), current=self.estimator.get(), side=RIGHT)
            drawCombobox(self.DataFrameUI, self.searchedValue, SEARCHED_VALUES, current=self.searchedValue.get(), side=SPAN)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
    
    def drawBarUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the bar plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=RIGHT)
            drawCombobox(self.DataFrameUI, self.estimator, list(ESTIMATORS.keys()), current=self.estimator.get(), side=LEFT)
            drawCombobox(self.DataFrameUI, self.searchedValue, SEARCHED_VALUES, current=self.searchedValue.get(), side=RIGHT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
    
    def drawScatterUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the scatter plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", side=LEFT, checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Size")
            drawScale(self.DataFrameUI, self.scaleSize, from_=0.0, to=1000.0, resolution=10.0, side=RIGHT, set=self.scaleSize.get())   

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())

    def drawEllipsesUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the scatter and ellipses plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", side=LEFT, checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Size", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleSize, from_=0.0, to=1000.0, resolution=10.0, side=RIGHT, set=self.scaleSize.get())   
            
            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
            
            drawLabel(self.DataFrameUI, "Coverage", side=LEFT)
            
            drawCombobox(self.DataFrameUI, self.coverageCombobox, list(COVERAGE_PERCENTS.keys()), side=RIGHT, current=self.coverageCombobox.get())

    def drawMultipleUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the multipe bar/box/line plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        self.drawPhases()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.chariot, "Chariot", checked=self.chariot.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
    
    def drawClusteredStackedBarUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the clustered stacked bar plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        self.drawPhases()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.chariot, "Chariot", checked=self.chariot.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.hashInvert, "Hash Invert", side=LEFT, checked=self.hashInvert.get())
            drawCheckButton(self.DataFrameUI, self.clusterInvert, "Cluster Invert", side=RIGHT, checked=self.clusterInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
    
    
    def drawEffectSizeUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the effect sizes
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)
        
        drawLabel(self.DataFrameUI, "Effect focus", side=LEFT)
        drawCombobox(self.DataFrameUI, self.effectFocus, self.getEffectFocus(), current=self.getCurrentFocusInList(self.effectFocus.get(), self.getEffectFocus()), side=RIGHT)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.chariot, "Chariot", checked=self.chariot.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
        
    def drawOrderingUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the ordering prints
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)
        
        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawLabel(self.DataFrameUI, "Top showed", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleTop, from_=1.0, to=5.0, function=self.actualizeScaleTop, resolution=1.0, side=RIGHT, set=self.scaleTop.get())
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", side=RIGHT, checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())
    
    def drawRegplotUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the regplot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        drawLabel(self.DataFrameUI, "Data values", side=SPAN)

        self.drawOrders()
        self.drawPrevious()
        self.drawStates()
        self.drawPhases()
        self.drawMicrogestures()
        self.drawFamilies()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=RIGHT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())
            
            drawCheckButton(self.DataFrameUI, self.xLimInf, "xLimInf", side=LEFT, checked=self.xLimInf.get())
            drawCheckButton(self.DataFrameUI, self.xLimSup, "xLimSup", side=RIGHT, checked=self.xLimSup.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
    
    def moreThanOneMgChecked(self) :
        """
        Determine if more than one microgesture is checked or not
        
        :return: number of microgestures considered
        """
        tap = self.tap.get()
        swipe = self.swipe.get()
        flex = self.flex.get()
        hold = self.hold.get()
        
        mgs=[tap, swipe, flex, hold]
        
        return len([x for x in mgs if x==True])>1
    
    def drawFamilies(self):
        """
        Adds to the DataFrameUI the buttons relative to the families to consider
        """
        if self.allFamilies.get()==0 :
            if self.graphTypeCombobox.get()!=CLUSTERED_STACKED_BAR :
                drawCheckButton(self.DataFrameUI, self.seeFamilies, "Families", function=self.actualizeDataFrameUI, checked=self.seeFamilies.get(), side=LEFT)
                if self.seeFamilies.get()!=0:
                    drawCheckButton(self.DataFrameUI, self.checkAllFamilies, "Check All", function=self.actualizeFamilies, checked=False, side=LEFT)
                    self.scrollableTextFamilies = drawScrollableText(self.DataFrameUI, side=SPAN)
                
                    drawScrollableCheckButton(self.scrollableTextFamilies, self.aandb, AANDB_NAME, function=self.actualizeScales, checked=self.aandb.get())
                    drawScrollableCheckButton(self.scrollableTextFamilies, self.chv, CHV_NAME, function=self.actualizeScales, checked=self.chv.get())
                    drawScrollableCheckButton(self.scrollableTextFamilies, self.chp, CHP_NAME, function=self.actualizeScales, checked=self.chp.get())
                    drawScrollableCheckButton(self.scrollableTextFamilies, self.mas, MAS_NAME, function=self.actualizeScales, checked=self.mas.get())
            else :
                stack = self.stackFocus.get()
                hash = self.hashFocus.get()
                cluster = self.clusterFocus.get()
                dropped = getSubstracted(PERFORMANCE_COLUMNS, stack, hash, cluster)
                if dropped == FAMILY :
                    drawLabel(self.DataFrameUI, "Families", side=LEFT)
                    drawCombobox(self.DataFrameUI, self.perfFamilies, list(SYMBOLS.keys()), current=self.perfFamilies.get(), side=RIGHT)
                
    def drawPrevious(self) :
        """
        Adds to the DataFrameUI the buttons relative to the state orders considered
        """
        if self.allPrevious.get()==0 :
            drawLabel(self.DataFrameUI, "Previous", side=SPAN)
            drawCheckButton(self.DataFrameUI, self.previous, PREVIOUS_DONE, checked=self.previous.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.notPrevious, PREVIOUS_NOT_DONE, checked=self.notPrevious.get(), side=SPAN)
            
    def drawOrders(self) :
        """
        Adds to the DataFrameUI the buttons relative to the state orders considered
        """
        if self.allOrders.get()==0 :
            drawLabel(self.DataFrameUI, "Order", side=SPAN)
            drawCheckButton(self.DataFrameUI, self.sd, SD, checked=self.sd.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.ds, DS, checked=self.ds.get(), side=SPAN)
    
    def drawStates(self) :
        """
        Adds to the DataFrameUI the buttons relative to the states considered
        """
        if self.allStates.get()==0 :
            if self.graphTypeCombobox.get()!=CLUSTERED_STACKED_BAR :
                drawLabel(self.DataFrameUI, "State", side=SPAN)
                drawCheckButton(self.DataFrameUI, self.static, STATIC, checked=self.static.get(), side=LEFT)
                drawCheckButton(self.DataFrameUI, self.dynamic, DYNAMIC, checked=self.dynamic.get(), side=RIGHT)
            else :
                stack = self.stackFocus.get()
                hash = self.hashFocus.get()
                cluster = self.clusterFocus.get()
                dropped = getSubstracted(PERFORMANCE_COLUMNS, stack, hash, cluster)
                if dropped == STATE :
                    drawLabel(self.DataFrameUI, "State", side=LEFT)
                    drawCombobox(self.DataFrameUI, self.perfStates, STATES, current=self.perfStates.get(), side=RIGHT)
                
    
    def drawPhases(self) :
        """
        Adds to the DataFrameUI the buttons relative to the states considered
        """
        if self.allPhases.get()==0 :
            if self.graphTypeCombobox.get()!=CLUSTERED_STACKED_BAR and self.phaseCombobox.get()==PERFORMANCES :
                drawLabel(self.DataFrameUI, "Phase", side=SPAN)
                drawCheckButton(self.DataFrameUI, self.firstPhase, FIRST_PHASE, checked=self.firstPhase.get(), side=LEFT)
                drawCheckButton(self.DataFrameUI, self.thirdPhase, THIRD_PHASE, checked=self.thirdPhase.get(), side=RIGHT)
            else :
                stack = self.stackFocus.get()
                hash = self.hashFocus.get()
                cluster = self.clusterFocus.get()
                dropped = getSubstracted(PERFORMANCE_COLUMNS, stack, hash, cluster)
                if dropped == PHASE :
                    drawLabel(self.DataFrameUI, "Phase", side=LEFT)
                    drawCombobox(self.DataFrameUI, self.perfPhases, ORDER_STRING_PHASE, current=self.perfPhases.get(), side=RIGHT)

    def drawMicrogestures(self) :
        """
        Adds to the DataFrameUI the buttons relative to the microgestures considered
        """
        if self.allMicrogestures.get()==0 :
            if self.graphTypeCombobox.get()!=CLUSTERED_STACKED_BAR :
                drawLabel(self.DataFrameUI, "Microgestures", side=SPAN)
                drawCheckButton(self.DataFrameUI, self.tap, TAP, function=self.actualizeDataFrameUI, checked=self.tap.get(), side=LEFT)
                drawCheckButton(self.DataFrameUI, self.swipe, SWIPE, function=self.actualizeDataFrameUI, checked=self.swipe.get(), side=RIGHT)
                drawCheckButton(self.DataFrameUI, self.flex, FLEX, function=self.actualizeDataFrameUI, checked=self.flex.get(), side=LEFT)
                drawCheckButton(self.DataFrameUI, self.hold, HOLD, function=self.actualizeDataFrameUI, checked=self.hold.get(), side=RIGHT)
                drawCheckButton(self.DataFrameUI, self.checkAllMicrogestures, "Check All", function=self.actualizeMicrogestures, checked=self.checkAllMicrogestures.get())
            else :
                stack = self.stackFocus.get()
                hash = self.hashFocus.get()
                cluster = self.clusterFocus.get()
                dropped = getSubstracted(PERFORMANCE_COLUMNS, stack, hash, cluster)
                if dropped == MICROGESTURE :
                    drawLabel(self.DataFrameUI, "Microgestures", side=LEFT)
                    drawCombobox(self.DataFrameUI, self.perfMicrogestures, MICROGESTURES, current=self.perfMicrogestures.get(), side=RIGHT)

    def actualizeScales(self) :
        """
        Actualize the scale values according to the last changes made on the considered variables
        """
        if (self.scaleAfter.get()+self.scaleBefore.get()>self.familyNbr.get()):
            self.scaleAfter.set(self.familyNbr.get()-self.scaleBefore.get())
    
    def actualizeScaleTop(self) :
        """
        Actualize the scale values according to the last changes made on the scaleTop
        """
        self.scaleBefore.set(self.familyNbr.get()-(self.scaleTop.get()-1))  
    
    def actualizeScaleBefore(self) :
        """
        Actualize the scale values according to the last changes made on the scaleBefore
        """
        if (self.scaleAfter.get()+self.scaleBefore.get()>self.familyNbr.get()):
            self.scaleAfter.set(self.familyNbr.get()-self.scaleBefore.get())

    def actualizeScaleAfter(self) :
        """
        Actualize the scale values according to the last changes made on the scaleAfter
        """
        if (self.scaleAfter.get()+self.scaleBefore.get()>self.familyNbr.get()):
            self.scaleBefore.set(self.familyNbr.get()-self.scaleAfter.get())

    def actualizeMicrogestures(self) :
        """
        Check or uncheck all the microgestures
        """
        if self.checkAllMicrogestures.get() :
            self.tap.set(1)
            self.swipe.set(1)
            self.flex.set(1)
            self.hold.set(1)
        else :
            self.tap.set(0)
            self.swipe.set(0)
            self.flex.set(0)
            self.hold.set(0)

    def actualizeFamilies(self) :
        """
        Check or uncheck all the families
        """
        if self.checkAllFamilies.get() :
            self.aandb.set(1)
            self.dpa.set(1)
            self.dea.set(1)
            self.ba.set(1)
            self.at.set(1)
            self.chv.set(1)
            self.atc.set(1)
            self.chp.set(1)
            self.hiz.set(1)
            self.hib.set(1)
            self.dsl.set(1)
            self.dsb.set(1)
            self.cws.set(1)
            self.cns.set(1)
            self.mst.set(1)
            self.eml.set(1)
            self.ri.set(1)
            self.mas.set(1)
            self.elc.set(1)
            self.gra.set(1)
            self.tar.set(1)
        else :
            self.aandb.set(0)
            self.dpa.set(0)
            self.dea.set(0)
            self.ba.set(0)
            self.at.set(0)
            self.chv.set(0)
            self.atc.set(0)
            self.chp.set(0)
            self.hiz.set(0)
            self.hib.set(0)
            self.dsl.set(0)
            self.dsb.set(0)
            self.cws.set(0)
            self.cns.set(0)
            self.mst.set(0)
            self.eml.set(0)
            self.ri.set(0)
            self.mas.set(0)
            self.elc.set(0)
            self.gra.set(0)
            self.tar.set(0)

###########################################################################
#######                            BAZAR                          #########
###########################################################################

    def prepareVisualize(self) :
        """
        Prepare the visualization according to the wrapping changes
        """
        self.figure = Figure()
        self.axis = self.figure.add_axes([self.xWrap.get(),self.yWrap.get(),self.widthWrap.get(),self.heightWrap.get()])
        