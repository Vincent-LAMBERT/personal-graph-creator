#!/usr/bin/env python3

import re
from os.path import exists

import numpy as np
import pandas as pd

from App.DataProcessor import DataProcessor
from App.Utils import *
from Experiences.Headset3DHandUI.Utils.GeneralUtils import *


class Headset3DHandUIProcessor (DataProcessor) :
    def __init__ (self) :
        """
        Instantiatation function

        :param file: the name of the parsed file
        """
        super().__init__('Headset3DHandUI')
        
        if not (exists(self.pandaInfoFile) and exists(self.pandaDataFile)) :
            self.ids = {}
            self.infoframe = pd.DataFrame({})
            self.dataframe = pd.DataFrame({})

            self.computeGender()
            self.computeAge()

            for phase in PHASES :
                for state in STATES :
                    for mg in MICROGESTURES :
                        self.computePhaseStateMg(phase, state, mg)
                    
            self.infoframe.to_csv(self.pandaInfoFile)
            self.dataframe.to_csv(self.pandaDataFile)
    
    def setVisualizer(self, visu):
        """
        Associate a visualizer to the computation class
        in order to filter the beforehand computed data

        :param visu: corresponding visualizer
        """
        self.visualizer = visu

    ## COMPUTE CSV ##

    def computeInfo(self, key, subsetFunction, analysisFunction) :
        """
        Compute the information relative to the
        given key
        
        :param key: the information to compute
        :param subsetFunction: function to apply
        on the raw data
        :param analysisFunction: function to apply
        during the analysis
        """
        for order in SD_ORDER:
            dataSubset = self.getDataSubset([self.getIndexFromId, subsetFunction], [ID, key], SD_CONDITIONS[order])
            dataMaths = analysisFunction(dataSubset)               
            d = pd.DataFrame({key : dataMaths})
            d[ORDER] = order
            self.infoframe=pd.concat([d, self.infoframe], ignore_index=False)

    def getIndexFromId(self, id) :
        """
        Compute the index corresponding to the given ID
        
        :param id: the id of the parsed data
        :return: the corresponding index
        """
        if id in self.ids :
            return self.ids[id]
        new_value = str(max([-1]+[int(x) for x in self.ids.values()])+1)
        self.ids[id] = new_value
        return new_value
        
        
    def computeGender(self) :
        """
        Compute the information relative to gender
        """
        self.computeInfo(GENDER, getGender, analyseGender)

    def computeAge(self) :
        """
        Compute the information relative to age
        """
        self.computeInfo(AGE, IDENTITY, analyseAge)

    def computePhaseStateMg(self, phase, state, mg) :
        """
        Compute the information relative to the a given
        phase, state and microgesture
        
        :param phase: 1, 2 or 3
        :param state: static or dynamic
        :param mg: tap, swipe, flex or hold
        """
        for order in SD_ORDER:
            stateMg = self.getDataSubset([self.getIndexFromId, getStudyDone]+[getIndexFromMgMention]*FAMILY_NBR, [ID, PREVIOUS_STUDY]+getKeysFor(phase, state, mg), SD_CONDITIONS[order])
            stateMg = modifyKeys(stateMg)
            for family in FAMILIES :
                fam_name = getFamilyNameFromId(family)
                for participant in stateMg :
                    d = getDataFrameForMg(participant[ID], participant[PREVIOUS_STUDY], order, phase, state, mg, fam_name, participant[family])
                    self.dataframe=pd.concat([d, self.dataframe], ignore_index=True)
        
    ## COMPUTE VISUALIZATIONS ##
    
    def getInfoState(self, searchedValue) :
        """
        Returns the information for a
        given value as two lists
        
        :param searchedValue: the value to
        obtain information for
        :return: a list of labels and a
        list of values
        """
        allOrder = self.visualizer.allOrders.get()
        order = self.visualizer.orderCombobox.get()
        df = self.infoframe.copy()
        
        df = df[~df[searchedValue].isna()]
        if allOrder!=0 :
            df[ORDER] = ALL
        else :
            if order==SD :
                df = df[df[ORDER] == SD]
            elif order==DS :
                df = df[df[ORDER] == DS]

        df = df.groupby(level=0).sum(numeric_only=True)
        labels = df.index.values.tolist()
        values = df[searchedValue].tolist()
        return labels, values

    def categoricalFocus(self, df, catFocus) :
        """
        Transform a column into a Categorical one 
        in the given dataframe
        
        :param df: the dataframe to transform
        :param catFocus: column name
        :return: the transformed dataframe
        """
        groupby_order = df.groupby([PARTICIPANT_ID])[SCORE].mean().sort_values().index
        df[PARTICIPANT_ID] = pd.Categorical(df[PARTICIPANT_ID], categories=[x for x in groupby_order], ordered=True) 
        return df

    def applyModifiers(self, dataframe, phase, graphType) :
        """
        Apply the combining modifiers
        for the given dataframe. It filters it by
        replacing the unfocused values by the
        "All" token
        
        :param dataframe: the dataframe to filter
        :param dataframe: the current experience phase
        :return: the filtered dataframe
        """
        userAggregate = self.visualizer.userAggregate.get()
        allOrder = self.visualizer.allOrders.get()
        allState = self.visualizer.allStates.get()
        allMicrogesture = self.visualizer.allMicrogestures.get()
        allPhases = self.visualizer.allPhases.get()
        allFamily = self.visualizer.allFamilies.get()
        allPrevious = self.visualizer.allPrevious.get()
        
        sd = self.visualizer.sd.get()
        ds = self.visualizer.ds.get()
        static = self.visualizer.static.get()
        dynamic = self.visualizer.dynamic.get()
        
        df = dataframe.copy()
        if phase!=PERFORMANCES :
            df = df[df[PHASE] == 2]
            df = df.drop(PHASE, axis=1)
            df[SCORE] = df[SCORE].astype(np.int64)
            for catFocus in CATEGORICAL_FOCUSES : df = self.categoricalFocus(df, catFocus)
            if self.visualizer.allMicrogestures.get()!=0 :
                df = df.drop(MICROGESTURE, axis=1)
            else :
                droppedMgs = self.visualizer.getDroppedMgs()
                df = df[df[MICROGESTURE].map(lambda x: x not in droppedMgs)]
        else :
            df = df[df[PHASE] != 2]
            if self.visualizer.firstPhase.get()==0 :
                df = df[df[PHASE] != 1]
            if self.visualizer.thirdPhase.get()==0 :
                df = df[df[PHASE] != 3]
        
        if phase==SATISFACTION or graphType!=CLUSTERED_STACKED_BAR: 
            if userAggregate!=0 :
                df = df.drop(PARTICIPANT_ID, axis=1)
                
            if allPrevious!=0 :
                df = df.drop(PREVIOUS_STUDY, axis=1)
            
            if allOrder!=0 :
                df = df.drop(ORDER, axis=1)
            else :
                droppedOrders = self.visualizer.getDroppedOrders()
                df = df[df[ORDER].map(lambda x: x not in droppedOrders)]

            if allState!=0 :
                df = df.drop(STATE, axis=1)
            else :
                droppedStates = self.visualizer.getDroppedStates()
                df = df[df[STATE].map(lambda x: x not in droppedStates)]
            
            if graphType==CLUSTERED_STACKED_BAR :
                if allMicrogesture!=0 :
                    df = df.drop(MICROGESTURE, axis=1)
                else :
                    droppedMgs = self.visualizer.getDroppedMgs()
                    df = df[df[MICROGESTURE].map(lambda x: x not in droppedMgs)]
            
            if allFamily!=0 :
                df = df.drop(FAMILY, axis=1)
            else :
                droppedFamilies = self.visualizer.getDroppedFamilies()
                df = df[df[FAMILY].map(lambda x: x not in droppedFamilies)]
        
        return df
    
    def getMultiIndex(self, df) :
        userAggregate = self.visualizer.userAggregate.get()
        allState = self.visualizer.allStates.get()
        allOrder = self.visualizer.allOrders.get()
        allMicrogesture = self.visualizer.allMicrogestures.get()
        allPhases = self.visualizer.allPhases.get()
        allFamily = self.visualizer.allFamilies.get()
        
        
        multiIndexUniqueList=[]
        multiIndexList=[]
        
        if userAggregate!=0 :
            df = df.drop(PARTICIPANT_ID, axis=1)
        else : 
            multiIndexUniqueList.append(df[PARTICIPANT_ID].unique())
            multiIndexList.append(PARTICIPANT_ID)
        
        if allFamily!=0 :
            df = df.drop(FAMILY, axis=1)
        else :
            droppedFamilies = self.visualizer.getDroppedFamilies()
            df = df[df[FAMILY].map(lambda x: x not in droppedFamilies)]
            multiIndexUniqueList.append(df[FAMILY].unique())
            multiIndexList.append(FAMILY)

        if allOrder!=0 :
            df = df.drop(ORDER, axis=1)
        else :
            droppedOrders = self.visualizer.getDroppedOrders()
            df = df[df[ORDER].map(lambda x: x not in droppedOrders)]
            multiIndexUniqueList.append(df[ORDER].unique())
            multiIndexList.append(ORDER)

        if allPhases!=0 :
            df = df.drop(PHASE, axis=1)
        else :
            droppedPhases = self.visualizer.getDroppedPhases()
            df = df[df[PHASE].map(lambda x: x not in droppedPhases)]
            multiIndexUniqueList.append(df[PHASE].unique())
            multiIndexList.append(PHASE)

        if allState!=0 :
            df = df.drop(STATE, axis=1)
        else :
            droppedStates = self.visualizer.getDroppedStates()
            df = df[df[STATE].map(lambda x: x not in droppedStates)]
            multiIndexUniqueList.append(df[STATE].unique())
            multiIndexList.append(STATE)
        
        if allMicrogesture!=0 :
            df = df.drop(MICROGESTURE, axis=1)
        else :
            droppedMgs = self.visualizer.getDroppedMgs()
            df = df[df[MICROGESTURE].map(lambda x: x not in droppedMgs)]
            multiIndexUniqueList.append(df[MICROGESTURE].unique())
            multiIndexList.append(MICROGESTURE)
            
        multiIndexUniqueList.append(RECOGNIZED)
        return multiIndexUniqueList, multiIndexList, df
    
    def combineColumns(self, df, colToFocus) :
        """
        Combine all the columns which are not 
        dropped nor focused
        
        :param df: the dataframe to work on
        :param colToFocus: the column not combined
        :return: the dataframe, the colToFocus, the
        combined column name and the list of the 
        original combined columns
        """
        colsToUnite = [x for x in DATA_TO_FOCUS_SPLIT_FAM if x in df.columns.to_list()]
        colsToUnite.remove(colToFocus)
        
        combination, df = getCombination(colsToUnite, df, self.visualizer.chariot.get())
        df[MEAN] =  df.groupby([combination, colToFocus])[SCORE].transform(np.mean)
        df[STDEV] =  df.groupby([combination, colToFocus])[SCORE].transform(np.std)
        
        return df, colToFocus, combination, colsToUnite

    def getDataOrderings(self) :
        """
        Returns the computed dataframe with
        the associated orderings for each
        combination of states and microgestures
        selected in the visualizer
        
        :return: the data as a panda dataframe
        """
        self.visualizer.userAggregate.set(1)
        self.visualizer.allOrders.set(1)
        
        combinedStates = self.visualizer.getCombinedStates()
        combinedMgs = self.visualizer.getCombinedMgs()
        
        roundedMean = lambda x: np.round(np.mean(x),2)
        
        globalDf = pd.DataFrame({})
        for states in combinedStates :
            self.visualizer.updateStates(states)
            for mgs in combinedMgs :
                self.visualizer.updateMgs(mgs)
                colToFocus, combination, colsToUnite, df = self.getDataState()
                df[MEAN] =  df.groupby([FAMILY])[MEAN].transform(roundedMean)
                df = df.drop(columns=colsToUnite+[combination, STDEV], axis=1)
                df = df.drop_duplicates()
                df = df.reset_index(drop=True)
                if self.visualizer.phaseCombobox.get()==SATISFACTION : df = self.reorderDf(df, MEAN, FAMILY, ascending=False)
                
                newDf = pd.DataFrame({STATE : [' & '.join(states)]})
                newDfMgs = pd.DataFrame({mg:['x'] for mg in mgs})
                if self.visualizer.imageOrient.get() == ORIENT_SIDE :
                    orient = ""
                elif self.visualizer.imageOrient.get() == ORIENT_ABOVE :
                    orient = "Above"
                else :
                    orient = "Sym"
                if self.visualizer.showAnnotations.get()!=0 :
                    newDfTops = pd.DataFrame({'Top {0}'.format(index+1):["\\"+str(row[FAMILY])+orient+" \mean"+"{"+str(row[MEAN])+"}"] for index, row in df.iterrows()})
                else :
                    newDfTops = pd.DataFrame({'Top {0}'.format(index+1):["\\"+str(row[FAMILY])+orient] for index, row in df.iterrows()})
                newDf = pd.concat([newDf, newDfMgs, newDfTops], axis=1, ignore_index=False)
                globalDf = pd.concat([newDf, globalDf], ignore_index=True)
        
        latex = ""
        alreadySeen = []
        for index, row in globalDf.iterrows() :
            index = 0
            while index < len(row) :
                value = row[index]
                if isinstance(value, str) :
                    value = value.replace("& dynamic", "\\"+"& \\dynamic")
                    value = value.replace("A&B", "AandB")
                elif isinstance(value, int): break
                else : value = ""
                if index != 0 : latex += " & " 
                else : 
                    latex += ""
                    if value in alreadySeen :
                        value = ""
                    else : 
                        alreadySeen.append(value)
                        value = "\\hline \n \\multirow"+"{"+"15"+"}"+"{"+"*"+"}"+"{"+"\\ro"+"{"+"\\"+value+"}"+"}"
                latex += str(value)
                index += 1
            latex += " \\\\ \n"
        print(latex)
            
        return globalDf

    def getDataState(self) :
        """
        Returns the computed dataframe with a focus
        on the searchedValue
        
        :param searchedValue: the value to
        focus on
        :return: the data as a panda dataframe
        """
        colToFocus = self.visualizer.dataFocus.get()
        phase = self.visualizer.phaseCombobox.get()
        graphType = self.visualizer.graphTypeCombobox.get()

        df = self.dataframe.copy()
        df = self.applyModifiers(df, phase, graphType)
        
        if colToFocus==FAMILY and phase==SATISFACTION: 
            df = self.removeUpperLowerFamilies(df)
        
        if phase==SATISFACTION :
            print(f"df before {df}")
            df, colToFocus, combination, colsToUnite = self.combineColumns(df, colToFocus)
            df = df.drop(columns=SCORE, axis=1)
            df = df.drop_duplicates()
            print(f"df after {df}")
            return colToFocus, combination, colsToUnite, df
        else :
            df[PHASE] = df[PHASE].replace({1: FIRST_PHASE, 3: THIRD_PHASE})
            if not graphType == CLUSTERED_STACKED_BAR :
                df = self.computePerformancesForBars(df)

                if self.visualizer.allMicrogestures.get()!=0 :
                    df = df.drop(MICROGESTURE, axis=1)
                else :
                    droppedMgs = self.visualizer.getDroppedMgs()
                    df = df[df[MICROGESTURE].map(lambda x: x not in droppedMgs)]
                    
                if self.visualizer.allPhases.get()!=0 :
                    df = df.drop(PHASE, axis=1)
                else :
                    droppedPhases = self.visualizer.getDroppedPhases()
                    df = df[df[PHASE].map(lambda x: x not in droppedPhases)]
                    
                df, colToFocus, combination, colsToUnite = self.combineColumns(df, colToFocus)
                df = df.drop(columns=SCORE, axis=1)
                df = df.drop_duplicates()
            else : 
                df = df.drop(PREVIOUS_STUDY, axis=1)
                df = self.computePerformancesForCluster(df)
                colToFocus = ""
                combination = ""
                colsToUnite = []
            return colToFocus, combination, colsToUnite, df
    
    def computePerformancesForBars(self, df) :
        """
        Return the table of performances by
        replacing the SCORE values by 1 if 
        they are equal to the MICROGESTURE value or
        by 0 otherwise
        """
        # minimize the first letter of each string in
        # the SCORE column of the dataframe
        df[SCORE] = df[SCORE].str.lower()
        df[SCORE] = np.where(df[SCORE]==df[MICROGESTURE], 1, 0)
        return df
    
    def computePerformancesForCluster(self, df) :
        """
        Return the cross table of performances
        corresponding to the given family
        
        :param df: the dataframe to work on
        :return: the corresponding dataframe of performances
        """
        
        
        # multiIndexUniqueList=[df[FAMILY].unique(), df[PHASE].unique(), df[STATE].unique(), df[MICROGESTURE].unique(),
        #                          RECOGNIZED]
        # multiIndexList=[FAMILY, PHASE, STATE, MICROGESTURE]
        
        multiIndexUniqueList, multiIndexList, df = self.getMultiIndex(df)
        
        
        mi = pd.MultiIndex.from_product(multiIndexUniqueList, names=multiIndexList+[SCORE])
        
        
        # df = df.drop(columns=[PARTICIPANT_ID], axis=1)
        # df = df.drop(columns=[ORDER], axis=1)
        
        # mi = pd.MultiIndex.from_product([df[FAMILY].unique(), df[PHASE].unique(), df[STATE].unique(), df[MICROGESTURE].unique(),
        #                          RECOGNIZED],
        #                         names=[FAMILY, PHASE, STATE, MICROGESTURE, SCORE])

        # out = (df.groupby([FAMILY, PHASE, STATE, MICROGESTURE, SCORE]).value_counts()/df.groupby([FAMILY, PHASE, STATE, MICROGESTURE])[SCORE].count())*100
        # out = out.reindex(mi, fill_value=0).rename("Count").reset_index()

        # df = out.merge(df, on=[FAMILY, PHASE, STATE, MICROGESTURE, SCORE], how="outer")
        # df = df.drop_duplicates()
        # df = df.set_index([FAMILY, PHASE, STATE, MICROGESTURE, SCORE]).unstack()
        # df = df.reset_index()
        # lis = [FAMILY, PHASE, STATE, MICROGESTURE] + list(df.columns.droplevel(0)[4:])
        
        # df.columns = lis



        out = (df.groupby(multiIndexList+[SCORE]).value_counts()/df.groupby(multiIndexList)[SCORE].count())*100
        out = out.reindex(mi, fill_value=0).rename("Count").reset_index()

        df = out.merge(df, on=multiIndexList+[SCORE], how="outer")
        df = df.drop_duplicates()
        df = df.set_index(multiIndexList+[SCORE]).unstack()
        df = df.reset_index()
        lis = multiIndexList + list(df.columns.droplevel(0)[len(multiIndexList):])
        
        df.columns = lis
        
        print(df)
        
        return df
    
    def removeUpperLowerFamilies(self, df) :
        """
        Filter out the unwanted families from
        the given dataframe
        
        :param df: the dataframe to work on
        :return: the filtered dataframe
        """
        allOrder = self.visualizer.allOrders.get()
        allState = self.visualizer.allStates.get()
        allMicrogesture = self.visualizer.allMicrogestures.get()
        df = self.reorderDf(df, SCORE, FAMILY)
        
        scaleStep=1
        if allOrder==False:
            scaleStep*=2
        if allState==False:
            scaleStep*=2
        if allMicrogesture==False:
            scaleStep*=4

        df = self.removeUpperFamilies(df)
        df = self.removeLowerFamilies(df)
        return df
    
    def orderDataFocused(self, colToFocus, df) :
        """
        Sorts the values according to a given
        column and return them ordered
        with their associated colors as two lists
        
        :param colToFocus: the column name to use as
        sorting base 
        :param df: the dataframe to sort
        :return: a list of the ordered values in the
        focused column of df and the list of their
        corresponding colors 
        """
        df = df.sort_values(colToFocus, ascending=True, ignore_index=True)
        order, colors = self.getColorsFor(colToFocus, df)
        return order, colors, df

    def orderData(self, combination, colsToUnite, df) :
        """
        Sorts the values according to multiple columns
        and return them ordered with their associated
        colors as two lists. The values of the multiple
        columns are thus joined and those multiple
        columns dropped because of redundance issues
        
        :param combination: the column name to use for
        the multiple combined columns
        :param colsToUnite: the column names to use as
        sorting base 
        :return: a list of the ordered values in the
        focused columns of df and the list of their
        corresponding colors 
        """
        df = df.sort_values(colsToUnite, ascending=False, ignore_index=True)
        
        ordered_combinations = df[combination].unique()
        splited_ordered_combinations = df.drop_duplicates(subset=colsToUnite)[colsToUnite].to_numpy()
        
        colors=[]
        for comb in splited_ordered_combinations :
            colors.append(mixColorsCombination(comb, colors))

        if combination not in colsToUnite :
            df = df.drop(columns=colsToUnite)

        return ordered_combinations, colors, df
    
    def getColorsFor(self, colToFocus, df) :
        """
        Return the colors for the given column
        
        :param colToFocus: column to focus
        :param df: dataframe to work on
        :return: list of ordered values and their
        corresponding colors as another list
        """
        order = df[colToFocus].unique()
        if colToFocus not in [PARTICIPANT_ID] and (colToFocus in DATA_TO_FOCUS) :
            colors = ["#{0}".format(identifyColor(x)) for x in order]
        else :
            colors = MORE_COLORS.values()
        return order, colors
    
    def getMarkersFor(self, combination, df) :
        """
        Return a list of markers for the given
        combination
        
        :param combination: column to focus
        :param df: dataframe to work on
        :return: list of markers
        """
        markers =[]
        count = 0
        for x in df[combination].unique() :
            markers.append(identifyMark(x, count))
            count += 1
        return markers

    
    def drop(self, dropped, popped, name) :
        """
        Move the name value from the popped list
        to the dropped list
        
        :param dropped: list of values to drop after
        :param popped: list of values to pop from
        :param name: value to pop and drop
        :return: dropped and popped list after treatment
        """
        dropped.append(name)
        popped.pop(name, None)
        return dropped, popped

    def removeUpperFamilies(self, df) :
        """
        Remove the best families from the dataframe
        according to the number given by scaleAfter
        
        :param df: the dataframe to work on
        :return: the dataframe after removal 
        """
        nbr = self.visualizer.scaleAfter.get()
        families = df[FAMILY].unique().tolist()
        familiesToRemove = families[21-nbr:]
        df = df[df[FAMILY].isin(familiesToRemove) == False]
        return df

    def removeLowerFamilies(self, df) :
        """
        Remove the worst families from the dataframe
        according to the number given by scaleBefore
        
        :param df: the dataframe to work on
        :return: the dataframe after removal 
        """
        nbr = self.visualizer.scaleBefore.get()
        families = df[FAMILY].unique().tolist()
        familiesToRemove = families[:nbr]
        df = df[df[FAMILY].isin(familiesToRemove) == False]
        return df
    
    def reorderDf(self, df, value, focus, ascending=True) :
        """
        Reorder the dataframe on the given
        value according to the ascending focus
        
        :param value: the column to sort on
        :param focus: the column to groupy
        :param ascending: the sorting order
        :return: the ordered data
        """
        # Print focus column order
        if (self.visualizer.searchedValue.get()==MEAN) :
            groupby_order = df.groupby([focus])[value].mean().sort_values().index
        else :
            # Sort by median value and mean value
            sorting_median_mean = lambda x: x.median()*1000 + x.mean()
            groupby_order = df.groupby([focus])[value]
            groupby_order = groupby_order.apply(sorting_median_mean).sort_values().index
        if focus in CATEGORICAL_FOCUSES :
            df[focus] = pd.Categorical(df[focus], categories=[x for x in groupby_order], ordered=True) 
        df = df.sort_values(focus, ascending=ascending, ignore_index=True)
        return df
## UTILS ##

def countValuesForKey(dictList, key) :
    """
    Count the occurences of the values for the key in the dictionary list
    
    :param dictList: list of dictionnaries
    :param key: key to search in the dictionnaries
    :return: dictionnary of occurrences
    """
    occurences = dict()
    for dic in dictList :
        value = dic[key]
        if value in occurences :
            occurences[value] += 1
        else :
            occurences[value] = 1
    return occurences

def analyseGender(data) :
    """
    Determine the gender repartition in the data
    
    :param data: the data to examine
    :return: dictionnary of gender repartition
    """
    dataGender = getValuesForKey(data, GENDER, str) 
    return {gender : dataGender.count(gender) for gender in GENDERS}  

def analyseAge(dataAge) :
    """
    Determine the age repartition in the data
    
    :param data: the data to examine
    :return: dictionnary of age statistics (sd, mean and median)
    """
    data = getValuesForKey(dataAge, AGE, int)
    return {'Sd' : np.round(np.std(data),2), 'Mean': np.round(np.mean(data),2), 'Median' : np.median(data)}

def getStudyDone(bool) :
    """
    Extract the exact gender string from the raw gender data
    
    :param genderString: the data given by the participant
    :return: participant gender
    """
    if bool=="y":
        return PREVIOUS_DONE
    else :
        return PREVIOUS_NOT_DONE
    
def getGender(genderString) :
    """
    Extract the exact gender string from the raw gender data
    
    :param genderString: the data given by the participant
    :return: participant gender
    """
    genderString = re.sub(r'[^\w\s]','', genderString).lower().replace("é", "e")

    if genderString in ["masculin", "homme", "male", "monsieur", "m", "Français", "français", "Francais", "francais", "h"] :
        return "Male"
    elif genderString in ["feminin", "femme", "femelle", "madame", "mademoiselle", "f", "Française", "française", "Francaise", "francaise"] :
        return "Female"
    else :
        return "Other"

def getKeysFor(phase, state, mg) :
    """
    Return the corresponding column name in the data for the given
    microgesture and state
    
    :param mg: considered microgesture
    :param state: considered state
    :return: data column name
    """
    return ["P{0}_{1}_{2}{3}".format(phase, mg.capitalize(), family, state[0].upper()) for family in FAMILIES]

def getIndexFromMgMention(s) :
    """
    Associate a numeric value to each score
    given by the participant
    
    :param s: string score
    :return: int score
    """
    mentionDict = {"Very bad": 1,
                   "Bad": 2,
                   "Quite bad": 3,
                   "Quite good": 4,
                   "Good": 5,
                   "Very good": 6}
    if s in mentionDict :
        return mentionDict[s]
    return s

def getDataFrameForMg(participantId, previous, order, phase, state, mg, fam_name, score) :
    """
    Return a dictionnary with the data associated with the given participant's score
    
    :param participantId: id of the participant
    :param order: state order (static or dynamic first)
    :param mgOrder: microgestures order
    :param state: static or dynamic
    :param mg: tap, swipe, flex or hold
    :param fam_name: considered family
    :param score: score given by the participant
    :return: dictionnary with the score and its context
    """
    
    if mg == TAP :
        d = {PARTICIPANT_ID : [participantId],
             PREVIOUS_STUDY : previous,
             ORDER : order, 
             PHASE : phase, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             SCORE: score}
    elif mg == SWIPE :
        d = {PARTICIPANT_ID : [participantId],
             PREVIOUS_STUDY : previous,
             ORDER : order, 
             PHASE : phase, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             SCORE: score}
    elif mg == FLEX :
        d = {PARTICIPANT_ID : [participantId],
             PREVIOUS_STUDY : previous,
             ORDER : order, 
             PHASE : phase, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             SCORE: score}
    elif mg == HOLD :
        d = {PARTICIPANT_ID : [participantId],
             PREVIOUS_STUDY : previous,
             ORDER : order, 
             PHASE : phase, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             SCORE: score}
    return pd.DataFrame(data=d)

def modifyKey(key) :
    """
    Returns the state order from a key with the shape P2_{}_{state}{}
    
    :param key: the key to modify
    :return: the state from the given key
    """
    if key==ID or key==PREVIOUS_STUDY : return key
    return key[-2:-1]

def modifyKeys(participantList) :
    """
    Allows to handle a list of participants data
    with a special treatment applied on the keys
    
    :param participantList: list of participants data
    :return: modified list
    """
    modifiedList = []
    for p in participantList :
        d = {modifyKey(k):p[k] for k in p}
        modifiedList.append(d)
    return modifiedList