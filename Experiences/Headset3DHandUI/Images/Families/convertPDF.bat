@echo off
for %%i in ("%~dp0SVG\*.svg") do (
    echo %%i to %%~ni.pdf
    "C:\Program Files\Inkscape\bin\inkscape.exe" --export-type="pdf" "%%i"
)


for %%i in ("%~dp0SVG\*.pdf") do (
    move "%%i" PDF\    
)