#!/usr/bin/env python3

import itertools
import tkinter as tk

import pandas as pd
from matplotlib.figure import Figure

from App.DataVisualizer import COVERAGE_PERCENTS, DataVisualizer
from App.DrawUI import *
from App.Utils import *
from Experiences.Online2DHand.Utils.GeneralUtils import *


class Online2DHandVisualizer (DataVisualizer) :
    def __init__ (self, GUI, processor) :
        super().__init__(GUI, processor)
        self.imageOrient = tk.StringVar()
        self.graphTypeCombobox = tk.StringVar()
        self.orderCombobox = tk.StringVar()
        self.dataCombobox = tk.StringVar()
        self.dataValueCombobox = tk.StringVar()
        self.stateCombobox = tk.StringVar()
        self.mgCombobox = tk.StringVar()
        self.seeParameters = tk.IntVar()
        self.seeFamilies = tk.IntVar()
        self.seeMovements = tk.IntVar()
        self.checkAllMovements = tk.IntVar()
        self.dataFocus = tk.StringVar()
        self.effectFocus = tk.StringVar()
        self.sd = tk.IntVar()
        self.ds = tk.IntVar()
        self.static = tk.IntVar()
        self.dynamic = tk.IntVar()
        self.tap = tk.IntVar()
        self.hold = tk.IntVar()
        self.swipe = tk.IntVar()
        self.flex = tk.IntVar()
        self.beforeMg = tk.StringVar()
        self.afterMg = tk.StringVar()
        self.checkAllMicrogestures = tk.IntVar()
        self.checkAllFamilies = tk.IntVar()
        self.userFamiliarities = tk.IntVar()
        self.allOrders = tk.IntVar()
        self.allMgOrders = tk.IntVar()
        self.allStates = tk.IntVar()
        self.allMicrogestures = tk.IntVar()
        self.allFamilies = tk.IntVar()
        self.axisInvert = tk.IntVar()
        self.specialFocus = tk.StringVar()
        self.xInvert = tk.IntVar()
        self.yInvert = tk.IntVar()
        self.styleInvert = tk.IntVar()
        self.scaleBefore = tk.IntVar()
        self.scaleAfter = tk.IntVar()
        self.scaleSize = tk.IntVar()
        self.coverageCombobox = tk.IntVar()
        self.absissesCombobox = tk.StringVar()

        self.aandb = tk.IntVar()
        self.dpa = tk.IntVar()
        self.dea = tk.IntVar()
        self.ba = tk.IntVar()
        self.at = tk.IntVar()
        self.chv = tk.IntVar()
        self.atc = tk.IntVar()
        self.chp = tk.IntVar()
        self.hiz = tk.IntVar()
        self.hib = tk.IntVar()
        self.dsl = tk.IntVar()
        self.dsb = tk.IntVar()
        self.cws = tk.IntVar()
        self.cns = tk.IntVar()
        self.mst = tk.IntVar()
        self.eml = tk.IntVar()
        self.ri = tk.IntVar()
        self.mas = tk.IntVar()
        self.elc = tk.IntVar()
        self.gra = tk.IntVar()
        self.tar = tk.IntVar()

        self.explicitnessFocus = tk.StringVar()
        self.explicitnessStringFocus = tk.IntVar()
        self.explicitnessValueFocus = tk.IntVar()
        self.trajectoryFocus = tk.IntVar()
        self.directionFocus = tk.IntVar()
        self.fingerFocus = tk.IntVar()
        self.returnFocus = tk.IntVar()
        self.touchPointFocus = tk.IntVar()
        self.aisFocus = tk.IntVar()
        self.ddFocus = tk.IntVar()
        self.movingPartFocus = tk.IntVar()
        self.pauseFocus = tk.IntVar()
        self.natureFocus = tk.IntVar()

        self.familyNbr = tk.IntVar()

        self.wrapCanvas = tk.IntVar()
        self.userAggregate = tk.IntVar()
        self.showPoints = tk.IntVar()
        self.chariot = tk.IntVar()
        self.scaleTop = tk.IntVar()
        
        self.xLimInf = tk.IntVar()
        self.xLimSup = tk.IntVar()
        self.familiarities = tk.StringVar()
        
        self.arUnknown = tk.IntVar()
        self.arHeardAbout = tk.IntVar()
        self.arNeverUsed = tk.IntVar()
        self.arWithoutHeadset = tk.IntVar()
        self.arWithHeadset = tk.IntVar()
        self.arWellKnown = tk.IntVar()
        self.mgUnknown = tk.IntVar()
        self.mgVagueIdeas = tk.IntVar()
        self.mgSomeExamples = tk.IntVar()
        self.mgDiverseKnowledge = tk.IntVar()
        self.mgWellKnown = tk.IntVar()

        self.imageOrient.set(ORIENTS[0])
        self.graphTypeCombobox.set(GRAPH_TYPES[0])
        self.orderCombobox.set(SD_ORDER[0])
        self.dataCombobox.set(GENDER)
        self.dataValueCombobox.set(PERCENTAGE)
        self.stateCombobox.set(STATES[0])
        self.mgCombobox.set(MICROGESTURES[0])
        self.seeParameters.set(0)
        self.seeFamilies.set(0)
        self.seeMovements.set(0)
        self.checkAllMovements.set(1)
        self.dataFocus.set(DATA_TO_FOCUS[-1])
        self.effectFocus.set(next(iter(EFFECT_TO_FOCUS[FAMILY])))
        self.sd.set(1)
        self.ds.set(1)
        self.static.set(1)
        self.dynamic.set(1)
        self.tap.set(1)
        self.hold.set(1)
        self.swipe.set(1)
        self.flex.set(1)
        self.beforeMg.set(MICROGESTURES[0])
        self.afterMg.set(MICROGESTURES[1])
        self.checkAllMicrogestures.set(1)
        self.checkAllFamilies.set(1)
        self.userFamiliarities.set(1)
        self.allMgOrders.set(1)
        self.allOrders.set(1)
        self.allStates.set(1)
        self.allMicrogestures.set(1)
        self.allFamilies.set(0)
        self.axisInvert.set(0)
        self.specialFocus.set(FAMILY)
        self.xInvert.set(0)
        self.yInvert.set(0)
        self.styleInvert.set(0)
        self.scaleBefore.set(13)
        self.scaleAfter.set(0)
        self.scaleSize.set(150)
        self.coverageCombobox.set(50)
        self.absissesCombobox.set(STDEV)

        self.aandb.set(1)
        self.dpa.set(1)
        self.dea.set(1)
        self.ba.set(1)
        self.at.set(1)
        self.chv.set(1)
        self.atc.set(1)
        self.chp.set(1)
        self.hiz.set(1)
        self.hib.set(1)
        self.dsl.set(1)
        self.dsb.set(1)
        self.cws.set(1)
        self.cns.set(1)
        self.mst.set(1)
        self.eml.set(1)
        self.ri.set(1)
        self.mas.set(1)
        self.elc.set(1)
        self.gra.set(1)
        self.tar.set(1)

        self.explicitnessFocus.set("String")
        self.explicitnessStringFocus.set(1)
        self.explicitnessValueFocus.set(1)
        self.trajectoryFocus.set(1)
        self.directionFocus.set(0)
        self.fingerFocus.set(0)
        self.returnFocus.set(0)
        self.touchPointFocus.set(0)
        self.aisFocus.set(0)
        self.ddFocus.set(0)
        self.movingPartFocus.set(0)
        self.pauseFocus.set(0)
        self.natureFocus.set(0)

        self.familyNbr.set(0)
        self.wrapCanvas.set(0)
        self.userAggregate.set(0)
        self.showPoints.set(1)
        self.chariot.set(0)
        self.scaleTop.set(3)
        self.graphType = None
        self.xLimInf.set(0)
        self.xLimSup.set(0)
        
        self.arUnknown.set(1)
        self.arHeardAbout.set(1)
        self.arNeverUsed.set(1)
        self.arWithoutHeadset.set(1)
        self.arWithHeadset.set(1)
        self.arWellKnown.set(1)
        self.mgUnknown.set(1)
        self.mgVagueIdeas.set(1)
        self.mgSomeExamples.set(1)
        self.mgDiverseKnowledge.set(1)
        self.mgWellKnown.set(1)
        
        self.familiarities.set(AR_FAMILIARITY)
        
        
        # pd.set_option('display.max_rows', None)

        self.actualizeVisualizerGUI()

        self.actualizePlot()
    
    
    def getSelectedStates(self) :
        """
        Returns the checked states
        
        :return: list of states
        """
        states = []
        if self.static.get()!=0 : states.append(STATIC)
        if self.dynamic.get()!=0 : states.append(DYNAMIC)
        return states
    
    def getSelectedMgs(self) :
        """
        Returns the checked microgestures
        
        :return: list of microgestures
        """
        mgs = []
        if self.tap.get()!=0 : mgs.append(TAP)
        if self.hold.get()!=0 : mgs.append(HOLD)
        if self.swipe.get()!=0 : mgs.append(SWIPE)
        if self.flex.get()!=0 : mgs.append(FLEX)
        return mgs
    
    def getCombinedStates(self) :
        """
        Returns the combined states
        
        :return: list of lists of states
        """
        selectedStates = self.getSelectedStates()
        
        combinedStates = []
        for i in range(1, len(selectedStates)+1) :
            for x in itertools.combinations(selectedStates, i) :
                combinedStates.append(x)
        return [[y for y in x] for x in combinedStates]
    
    def getCombinedMgs(self) :
        """
        Returns the combined
        microgestures
        
        :return: list of lists of
        microgestures
        """
        selectedMgs = self.getSelectedMgs()
        combinedMgs = []
        for i in range(1, len(selectedMgs)+1) :
            for x in itertools.combinations(selectedMgs, i) :
                combinedMgs.append(x)
        return [[y for y in x] for x in combinedMgs]
    
    def updateStates(self, states) :
        """
        Activate the checkboxes
        of the states in the parameter 
        "states". Desactivate the others
        
        :param states: list of states
        to consider
        """
        setIfElseUnset(self.static, states, STATIC)
        setIfElseUnset(self.dynamic, states, DYNAMIC)
    
    def updateMgs(self, mgs) :
        """
        Activate the checkboxes
        of the microgestures in the parameter 
        "mgs". Desactivate the others
        
        :param states: list of microgestures
        to consider
        """
        setIfElseUnset(self.tap, mgs, TAP)
        setIfElseUnset(self.hold, mgs, HOLD)
        setIfElseUnset(self.swipe, mgs, SWIPE)
        setIfElseUnset(self.flex, mgs, FLEX)
        
    def getDroppedOrders(self) :
        """
        Returns the orders to drop
        
        :return: a list of orders
        """
        orders={SD : self.sd.get(), 
                DS : self.ds.get()}
        return getDropped(orders)
    
    def getDroppedStates(self) :
        """
        Returns the states to drop
        
        :return: a list of states
        """
        states={STATIC : self.static.get(), 
                DYNAMIC : self.dynamic.get()}
        return getDropped(states)

    def getDroppedMgs(self) :
        """
        Returns the microgestures to drop
        
        :return: a list of microgestures
        """
        mgs={TAP : self.tap.get(), 
             HOLD : self.hold.get(), 
             SWIPE : self.swipe.get(), 
             FLEX : self.flex.get()}
        return getDropped(mgs)

    def getDroppedFamilies(self) :
        """
        Returns the families to drop
        
        :return: a list of families
        """
        fams={AANDB_NAME : self.aandb.get(), 
              DPA_NAME : self.dpa.get(), 
              DEA_NAME : self.dea.get(), 
              BA_NAME : self.ba.get(), 
              AT_NAME : self.at.get(), 
              CHV_NAME : self.chv.get(), 
              ATC_NAME : self.atc.get(),
              CHP_NAME : self.chp.get(),
              HIZ_NAME : self.hiz.get(), 
              HIB_NAME : self.hib.get(), 
              DSL_NAME : self.dsl.get(), 
              DSB_NAME : self.dsb.get(), 
              CWS_NAME : self.cws.get(), 
              CNS_NAME : self.cns.get(), 
              MST_NAME : self.mst.get(), 
              EML_NAME : self.eml.get(), 
              RI_NAME : self.ri.get(), 
              MAS_NAME : self.mas.get(), 
              ELC_NAME : self.elc.get(), 
              GRA_NAME : self.gra.get(), 
              TAR_NAME : self.tar.get()}
        return getDropped(fams)

    def getDroppedARFamiliarities(self) :
        """
        Returns the familiarities to drop
        
        :return: a list of families
        """
        fams={1 : self.arUnknown.get(), 
              2 : self.arHeardAbout.get(), 
              3 : self.arNeverUsed.get(), 
              4 : self.arWithoutHeadset.get(), 
              5 : self.arWithHeadset.get(), 
              6 : self.arWellKnown.get()}
        return getDropped(fams)
        
    def getDroppedMGFamiliarities(self) :
        """
        Returns the familiarities to drop
        
        :return: a list of families
        """
        fams={1 : self.mgUnknown.get(),
              2 : self.mgVagueIdeas.get(),
              3 : self.mgSomeExamples.get(),
              4 : self.mgDiverseKnowledge.get(),
              5 : self.mgWellKnown.get()}
        return getDropped(fams)

###########################################################################
#######                      ACTUALIZATIONS                       #########
###########################################################################

    def actualizeVisualizerGUI(self) :
        """
        Actualize the MenuFrame
        """
        for widget in self.GUI.MenuFrame.winfo_children():
            widget.destroy()

        self.familyNbr.set(FAMILY_NBR-1)
        drawLabel(self.GUI.MenuFrame, "Preset", side=LEFT)
        self.presetsNames = self.getPresetsNames()
        combobox = drawCombobox(self.GUI.MenuFrame, self.presetsCombobox, self.presetsNames, function=self.actualizePreset, side=RIGHT)
        drawButton(self.GUI.MenuFrame, "Save", function=self.savePreset, size=11, side=LEFT)
        drawButton(self.GUI.MenuFrame, "Delete", function=self.removePreset, size=11, side=RIGHT)
        drawLabel(self.GUI.MenuFrame, "Data", side=LEFT)
        drawCombobox(self.GUI.MenuFrame, self.graphTypeCombobox, GRAPH_TYPES, function=self.actualizeDataFrameUI, side=RIGHT)
        
        self.DataFrameUI = drawFrame(self.GUI.MenuFrame)
        self.DataFrameUI.grid_columnconfigure(0, weight=1, uniform="fred")
        self.DataFrameUI.grid_columnconfigure(1, weight=1, uniform="fred")
        
        self.actualizeDataFrameUI()

    def actualizeDataFrameUI(self) :
        """
        Actualize the DataFrameUI
        """
        graphType = self.graphTypeCombobox.get()

        self.cleanDataFrameUI()
        self.drawImageWrap()

        if graphType==PIE :
            self.drawPieUI()
        elif graphType==STRIP :
            self.drawStripUI()
        elif graphType==BOX :
            self.drawBoxUI()
        elif graphType==VIOLIN :
            self.drawBarUI()
        elif graphType==BAR :
            self.drawBarUI()
        elif graphType==SCATTER :
            self.drawScatterUI()
        elif graphType==ELLIPSES:
            self.drawEllipsesUI()
        elif graphType==MULTIPLE_BARS :
            self.drawMultipleUI()
        elif graphType==MULTIPLE_LINES :
            self.drawMultipleUI()
        elif graphType==MULTIPLE_BOXES :
            self.drawMultipleUI()
        elif graphType==MULTIPLE_VIOLIN :
            self.drawMultipleUI()
        elif graphType==EFFECT :
            self.drawEffectSizeUI()
        elif graphType==ORDERING :
            self.drawOrderingUI()
        elif graphType==REGPLOT :
            self.drawRegplotUI()
        else :
            pass
        self.graphType = graphType
        
    def actualizePlot(self):
        """
        Actualize the GraphFrame
        """
        self.prepareVisualize()
        graphType = self.graphTypeCombobox.get()

        if graphType==PIE :
            self.actualizePie()
        elif graphType==STRIP:
            self.actualizeStrip()
        elif graphType==BOX :
            self.actualizeBox()
        elif graphType==VIOLIN :
            self.actualizeViolin()
        elif graphType==BAR :
            self.actualizeBar()
        elif graphType==SCATTER :
            self.actualizeScatter()
        elif graphType==ELLIPSES:
            self.actualizeEllipses()
        elif graphType==MULTIPLE_BARS :
            self.actualizeMultipleBars()
        elif graphType==MULTIPLE_LINES :
            self.actualizeMultipleLines()
        elif graphType==MULTIPLE_BOXES :
            self.actualizeMultipleBoxes()
        elif graphType==MULTIPLE_VIOLIN :
            self.actualizeMultipleViolin()
        elif graphType==EFFECT :
            self.actualizeEffectSize()
        elif graphType==ORDERING :
            self.actualizeOrdering()
        elif graphType==REGPLOT :
            self.actualizeRegplot()
        else :
            pass
        
        self.applyLegendModifier()
        self.actualizeCanvas()
    
    def applyLegendModifier(self) :
        """
        Remove (or not) the graph legend
        """
        if self.showLegend.get() == False :
            self.axis.get_legend().remove()

    def actualizePie(self) :
        """
        Replace the plot canvas with a pie chart
        """
        data = self.dataCombobox.get()
        dataValue = self.dataValueCombobox.get()

        labels, values = self.processor.getInfoState(data)
        self.drawPie(data, values, labels, dataValue)
    
    def actualizeBar(self) :
        """
        Replace the plot canvas with a bar plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawBars(title, colToFocus, MEAN, df, order=hueOrder, palette=hueColors, yLabels=SCORES, invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeStrip(self) :
        """
        Replace the plot canvas with a box plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawStrip(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels=SCORES, invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeBox(self) :
        """
        Replace the plot canvas with a box plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawBox(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels=SCORES, invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get(), showPoints=self.showPoints.get())

    def actualizeViolin(self):
        """
        Replace the plot canvas with a violin plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawViolin(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels=SCORES, invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeScatter(self):
        """
        Replace the plot canvas with a scatter plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        styleOrder, styleColors, df = self.processor.orderData(combination, colsToUnite, df)
        hueOrder, hueColors = self.processor.getColorsFor(colToFocus, df)
        markers = self.processor.getMarkersFor(combination, df)

        title="{0} for {1}".format(colToFocus, combination)
        self.drawScatter(title, self.absissesCombobox.get(), MEAN, colToFocus, combination, markers,
                df, hueOrder, hueColors, styleOrder, size=self.scaleSize.get(), yLabels=SCORES, axisInvert=self.axisInvert.get(), invertY=self.yInvert.get())

    def actualizeEllipses(self):
        """
        Replace the plot canvas with a scatter plot and its bivariate ellipses
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        styleOrder, styleColors, df = self.processor.orderData(combination, colsToUnite, df)
        hueOrder, hueColors = self.processor.getColorsFor(colToFocus, df)

        title="{0} for {1}".format(colToFocus, combination)
        self.drawBivariateEllipses(title, self.absissesCombobox.get(), MEAN, colToFocus, combination, float(self.coverageCombobox.get()),
            df, hueOrder, hueColors, styleOrder, size=self.scaleSize.get(), yLabels=SCORES, axisInvert=self.axisInvert.get(), invertY=self.yInvert.get())

    def actualizeMultipleBars(self):
        """
        Replace the plot canvas with a multiple bars plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        if PARTICIPANT_ID in colsToUnite :
            colsToUnite.remove(PARTICIPANT_ID)
            combination, df = getCombination(colsToUnite, df, self.chariot.get())
        hueOrder, hueColors, df = self.processor.orderData(combination, colsToUnite, df)

        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawMultipleBars(title, colToFocus, MEAN, combination, df, hueOrder, hueColors, yLabels=SCORES, axisInvert=self.axisInvert.get(), invertX=self.xInvert.get(), invertY=self.yInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeMultipleLines(self):
        """
        Replace the plot canvas with a multiple lines plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        hueOrder, hueColors, df = self.processor.orderData(combination, colsToUnite, df)

        title="{0} for {1}".format(colToFocus, combination)
        self.drawMultipleLines(title, colToFocus, MEAN, combination, df, hueOrder, hueColors, yLabels=SCORES, axisInvert=self.axisInvert.get(), invertX=self.xInvert.get(), invertY=self.yInvert.get())

    def actualizeMultipleBoxes(self):
        """
        Replace the plot canvas with a multiple boxes plot
        """
        StartingColToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, StartingColToFocus)
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[StartingColToFocus], df, self.chariot.get())
        if PARTICIPANT_ID in colsToUnite :
            colsToUnite.remove(PARTICIPANT_ID)
            colsToUnite.append(StartingColToFocus)
        hueOrder, hueColors, df = self.processor.orderData(colToFocus, colsToUnite, df)

        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawBox(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels=SCORES, invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get(), showPoints=self.showPoints.get())

    def actualizeMultipleViolin(self) :
        """
        Replace the plot canvas with a multiple violins plot
        """
        StartingColToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[StartingColToFocus], df, self.chariot.get())
        if PARTICIPANT_ID in colsToUnite :
            colsToUnite.remove(PARTICIPANT_ID)
            colsToUnite.append(StartingColToFocus)
        hueOrder, hueColors, df = self.processor.orderData(colToFocus, colsToUnite, df)

        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawViolin(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels=SCORES, invertX=self.xInvert.get(), invertY=self.yInvert.get(), axisInvert=self.axisInvert.get(), image=isImage, orient=self.imageOrient.get())

    def actualizeEffectSize(self) :
        """
        Replace the plot canvas with a plot of effect sizes with mustaches
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[colToFocus], df, self.chariot.get())
        
        focus = self.effectFocus.get()
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        self.drawEffectSize(title, colToFocus, MEAN, df, focus, isImage, orient=self.imageOrient.get())

    def actualizeOrdering(self) :
        """
        Prints in the integrated terminal the top families orderings
        """
        df = self.processor.getDataOrderings()
        self.GUIprint(df.to_string())
    
    def actualizeRegplot(self) :
        """
        Replace the plot canvas with a linear regression plot
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        df = tweakValuesBeforeVisualizing(df, colToFocus)
        hueOrder, hueColors, df = self.processor.orderDataFocused(colToFocus, df)
        isImage=(colToFocus==FAMILY)
        title="{0} for {1}".format(colToFocus, combination)
        
        if self.xLimInf.get() : xLimInf=0.045 
        else : xLimInf=False
        if self.xLimSup.get() : xLimSup=1.125 
        else : xLimSup=False
        
        self.drawRegplot(title, colToFocus, MEAN, df, hueOrder, palette=hueColors, yLabels=SCORES, axisInvert=self.axisInvert.get(), invertY=self.yInvert.get(), xLimInf=xLimInf, xLimSup=xLimSup)

    def cleanDataFrameUI(self) :
        """
        Clear the DataFrameUI
        """
        for widget in self.DataFrameUI.winfo_children():
            widget.destroy()


###########################################################################
#######                          PRESETS                          #########
###########################################################################

    def actualizePreset(self) :
        """
        Apply the current preset to both the UI and the plot canvas
        """
        self.cleanDataFrameUI()
        presetName = self.presetsCombobox.get()
        if presetName!='':
            preset = self.getNamedPreset(presetName)
            preset.reverse()
            self.setPreset(preset)
            graphType = self.graphTypeCombobox.get()
            self.drawImageWrap()

            if graphType==PIE :
                self.drawPieUI()
            elif graphType==BOX :
                self.drawBoxUI()
            elif graphType==VIOLIN :
                self.drawBarUI()
            elif graphType==BAR :
                self.drawBarUI()
            elif graphType==SCATTER :
                self.drawScatterUI()
            elif graphType==ELLIPSES:
                self.drawEllipsesUI()
            elif graphType==MULTIPLE_BARS :
                self.drawMultipleUI()
            elif graphType==MULTIPLE_LINES :
                self.drawMultipleUI()
            elif graphType==MULTIPLE_BOXES :
                self.drawMultipleUI()
            elif graphType==MULTIPLE_VIOLIN :
                self.drawMultipleUI()
            elif graphType==EFFECT :
                self.drawEffectSizeUI()
            elif graphType==ORDERING :
                self.drawOrderingUI()
            elif graphType==REGPLOT :
                self.drawRegplotUI()
        
            self.actualizePlot()

    def createPreset(self, name) :
        """
        Create a preset
        
        :param name: preset name
        :return: list of the preset values 
        with a preceeding preset name
        """
        return [name] + self.getPreset()

    def preset(self) :
        """
        Returns the widgets variable to create a preset
        
        :return: list of variables
        """
        return [self.graphTypeCombobox, 
                self.orderCombobox,
                self.dataCombobox,
                self.dataValueCombobox, 
                self.stateCombobox, 
                self.mgCombobox, 
                self.seeParameters, 
                self.seeFamilies,
                self.dataFocus,
                self.effectFocus,
                self.sd, 
                self.ds, 
                self.static, 
                self.dynamic, 
                self.tap, 
                self.hold, 
                self.swipe, 
                self.flex, 
                self.beforeMg, 
                self.afterMg, 
                self.checkAllMicrogestures,
                self.checkAllFamilies, 
                self.allMgOrders,
                self.allOrders, 
                self.allStates, 
                self.allMicrogestures, 
                self.allFamilies, 
                self.axisInvert, 
                self.specialFocus, 
                self.xInvert, 
                self.yInvert, 
                self.styleInvert, 
                self.scaleBefore, 
                self.scaleAfter, 
                self.scaleSize, 
                self.coverageCombobox, 
                self.aandb, 
                self.dpa, 
                self.dea, 
                self.ba, 
                self.at, 
                self.chv, 
                self.atc, 
                self.chp, 
                self.hiz, 
                self.hib, 
                self.dsl, 
                self.dsb, 
                self.cws, 
                self.cns, 
                self.mst, 
                self.eml, 
                self.ri, 
                self.mas, 
                self.elc, 
                self.gra, 
                self.tar,
                self.trajectoryFocus, 
                self.directionFocus, 
                self.fingerFocus, 
                self.familyNbr,
                self.seeMovements,
                self.checkAllMovements,
                self.wrapCanvas,
                self.xWrap,
                self.yWrap,
                self.widthWrap,
                self.heightWrap,
                self.imageOrient,
                self.returnFocus,
                self.touchPointFocus,
                self.aisFocus,
                self.ddFocus,
                self.movingPartFocus,
                self.pauseFocus,
                self.natureFocus,
                self.userAggregate,
                self.showPoints,
                self.chariot,
                self.scaleTop,
                self.explicitnessStringFocus,
                self.explicitnessValueFocus,
                self.explicitnessFocus,
                self.absissesCombobox,
                self.xLimInf,
                self.xLimSup,
                self.userFamiliarities,
                self.familiarities,
                self.arUnknown,
                self.arHeardAbout,
                self.arNeverUsed,
                self.arWithoutHeadset,
                self.arWithHeadset,
                self.arWellKnown,
                self.mgUnknown,
                self.mgVagueIdeas,
                self.mgSomeExamples,
                self.mgDiverseKnowledge,
                self.mgWellKnown]

    def getPreset(self) :
        """
        Returns the values associated to the current preset variables
        
        :return: list of values
        """
        return [x.get() for x in self.preset()]

    def setPreset(self, preset) :
        """
        Associates the values to the current preset variables
        
        :param preset: values to set on the variables
        """
        for x in self.preset() :
            x.set(preset.pop())

###########################################################################
#######                          DRAWINGS                         #########
###########################################################################
    
    def drawImageWrap(self) :
        """
        Adds the image wrapping buttons to the DataFrameUI
        
        Allows to move and flex the plot canvas
        """
        drawCheckButton(self.DataFrameUI, self.wrapCanvas, "Wrap canvas", function=self.actualizeDataFrameUI, checked=self.wrapCanvas.get(), side=LEFT)

        if self.wrapCanvas.get()!=0 :
            drawLabel(self.DataFrameUI, "xWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.xWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.xWrap.get())
            drawLabel(self.DataFrameUI, "yWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.yWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.yWrap.get())
            drawLabel(self.DataFrameUI, "heightWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.heightWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.heightWrap.get())
            drawLabel(self.DataFrameUI, "widthWrap", side=LEFT)
            drawScale(self.DataFrameUI, self.widthWrap, function=self.actualizeScaleBefore, from_=0.05, to=0.95, resolution=0.05, side=RIGHT, set=self.widthWrap.get())

    def drawCombineUI(self, cond=True) :
        """
        Adds the aggregation buttons to the DataFrameUI
        
        Allows to aggregate the values in the dataframe
        """
        drawLabel(self.DataFrameUI, "Combine all ...", side=SPAN)
        drawCheckButton(self.DataFrameUI, self.userAggregate, "Users", function=self.actualizeDataFrameUI, checked=self.userAggregate.get(), side=LEFT)
        drawCheckButton(self.DataFrameUI, self.userFamiliarities, "Familiarities", function=self.actualizeDataFrameUI, checked=self.userFamiliarities.get(), side=RIGHT)
        drawCheckButton(self.DataFrameUI, self.allStates, "States", function=self.actualizeDataFrameUI, checked=self.allStates.get(), side=LEFT)
        drawCheckButton(self.DataFrameUI, self.allOrders, "Orders", function=self.actualizeDataFrameUI, checked=self.allOrders.get(), side=RIGHT)
        drawCheckButton(self.DataFrameUI, self.allMgOrders, "Mgs order", function=self.actualizeDataFrameUI, checked=self.allMgOrders.get(), side=LEFT)
        drawCheckButton(self.DataFrameUI, self.allMicrogestures, "Microgestures", function=self.actualizeDataFrameUI, checked=self.allMicrogestures.get(), side=RIGHT)
        if cond :
            drawCheckButton(self.DataFrameUI, self.allFamilies, "Families", function=self.actualizeDataFrameUI, checked=self.allFamilies.get(), side=SPAN)

    def drawPieUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the pie chart
        """
        drawLabel(self.DataFrameUI, "Combine all ...", side=LEFT)
        drawCheckButton(self.DataFrameUI, self.allOrders, "Orders", function=self.actualizeDataFrameUI, checked=False, side=RIGHT)

        if self.allOrders.get()==0 :
            drawLabel(self.DataFrameUI, "Order", side=LEFT)
            drawCombobox(self.DataFrameUI, self.orderCombobox, SD_ORDER, side=RIGHT, current=self.orderCombobox.get())
        
        self.drawMgOrdersParam()
        
        drawCombobox(self.DataFrameUI, self.dataCombobox, [GENDER, DALTONISM], current=self.dataCombobox.get())
        drawCombobox(self.DataFrameUI, self.dataValueCombobox, [QUANTITY, PERCENTAGE], current=self.dataValueCombobox.get())
    
    def getDataToFocus(self) :
        """
        Returns the column to focus on in the dataframe
        
        :return: The column to focus on
        """
        dataToFocus = DATA_TO_FOCUS.copy()
        if self.userAggregate.get()!=0 : dataToFocus.remove(PARTICIPANT_ID)
        if self.allOrders.get()!=0 : dataToFocus.remove(ORDER)
        if self.allMgOrders.get()!=0 : dataToFocus.remove(MG_ORDER)
        if self.allStates.get()!=0 : dataToFocus.remove(STATE)
        if self.allMicrogestures.get()!=0 : dataToFocus.remove(MICROGESTURE)
        if self.allFamilies.get()!=0 : dataToFocus.remove(FAMILY)
        if self.userFamiliarities.get()!=0 : dataToFocus.remove(FAM_FAMILIARITIES)
        return dataToFocus

    def getCurrentDataFocus(self, data, datalist):
        """
        Returns the data corresponding to the
        focused column in the dataframe
        
        :return: the focused data
        """
        if data not in datalist :
            data = datalist[0]
        return data        
    
    def getEffectFocus(self) :
        """
        Returns the combinations of columns on which the
        effect size can be focused on
        
        :return: a list of combinations on which we can focus
        our effect size analysis
        """
        colToFocus, combination, colsToUnite, df = self.processor.getDataState(MEAN)
        colToFocus, df = getCombination([x for x in colsToUnite if x!=PARTICIPANT_ID]+[colToFocus], df, self.chariot.get())
        return df[colToFocus].unique().tolist()  
    
    def drawDataFocus(self) :
        """
        Adds to the DataFrameUI the data focus combobox
        """
        drawLabel(self.DataFrameUI, "Data focus", side=LEFT)
        drawCombobox(self.DataFrameUI, self.dataFocus, self.getDataToFocus(), function=self.actualizeDataFrameUI, current=self.getCurrentDataFocus(self.dataFocus.get(), self.getDataToFocus()), side=RIGHT)
        self.drawSpecialFocus()
        
    def drawStripUI(self):
        """
        Adds to the DataFrameUI the buttons relative to the strip plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=LEFT)
            drawCombobox(self.DataFrameUI, self.estimator, list(ESTIMATORS.keys()), current=self.estimator.get(), side=RIGHT)
            drawCombobox(self.DataFrameUI, self.searchedValue, SEARCHED_VALUES, current=self.searchedValue.get(), side=SPAN)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
        self.drawFamilies()
            
    def drawBoxUI(self):
        """
        Adds to the DataFrameUI the buttons relative to the box plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showPoints, "Show points", checked=self.showPoints.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=LEFT)
            drawCombobox(self.DataFrameUI, self.estimator, list(ESTIMATORS.keys()), current=self.estimator.get(), side=RIGHT)
            drawCombobox(self.DataFrameUI, self.searchedValue, SEARCHED_VALUES, current=self.searchedValue.get(), side=SPAN)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
        self.drawFamilies()
    
    def drawBarUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the bar plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=RIGHT)
            drawCombobox(self.DataFrameUI, self.estimator, list(ESTIMATORS.keys()), current=self.estimator.get(), side=LEFT)
            drawCombobox(self.DataFrameUI, self.searchedValue, SEARCHED_VALUES, current=self.searchedValue.get(), side=RIGHT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
        self.drawFamilies()
    
    def drawScatterUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the scatter plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", side=LEFT, checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Size")
            drawScale(self.DataFrameUI, self.scaleSize, from_=0.0, to=1000.0, resolution=10.0, side=RIGHT, set=self.scaleSize.get())   

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())

        self.drawFamilies()

    def drawEllipsesUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the scatter and ellipses plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", side=LEFT, checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Size", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleSize, from_=0.0, to=1000.0, resolution=10.0, side=RIGHT, set=self.scaleSize.get())   
            
            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
            
            drawLabel(self.DataFrameUI, "Coverage", side=LEFT)
            
            drawCombobox(self.DataFrameUI, self.coverageCombobox, list(COVERAGE_PERCENTS.keys()), side=RIGHT, current=self.coverageCombobox.get())

        self.drawFamilies()

    def drawMultipleUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the multipe bar/box/line plot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()

        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.chariot, "Chariot", checked=self.chariot.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())

        self.drawFamilies()
    
    def drawEffectSizeUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the effect sizes
        """
        self.drawCombineUI()
        self.drawDataFocus()
        
        drawLabel(self.DataFrameUI, "Effect focus", side=LEFT)
        drawCombobox(self.DataFrameUI, self.effectFocus, self.getEffectFocus(), current=self.getCurrentDataFocus(self.effectFocus.get(), self.getEffectFocus()), side=RIGHT)

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.chariot, "Chariot", checked=self.chariot.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
        self.drawFamilies()
        
    def drawOrderingUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the ordering prints
        """
        self.drawCombineUI()
        self.drawDataFocus()
        
        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawLabel(self.DataFrameUI, "Top showed", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleTop, from_=1.0, to=5.0, function=self.actualizeScaleTop, resolution=1.0, side=RIGHT, set=self.scaleTop.get())
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", side=RIGHT, checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())
        
        self.drawFamilies()
    
    def drawRegplotUI(self) :
        """
        Adds to the DataFrameUI the buttons relative to the regplot
        """
        self.drawCombineUI()
        self.drawDataFocus()

        self.drawFamiliarities()
        self.drawOrders()
        self.drawStates()
        self.drawMgOrdersParam()
        self.drawMicrogestures()
        
        drawCheckButton(self.DataFrameUI, self.seeParameters, "Parameters", function=self.actualizeDataFrameUI, checked=self.seeParameters.get(), side=LEFT)
        if self.seeParameters.get()!=0 :
            drawCheckButton(self.DataFrameUI, self.showLegend, "Show legend", checked=self.showLegend.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.showAnnotations, "Annotations", checked=self.showAnnotations.get(), side=RIGHT)
            drawLabel(self.DataFrameUI, "Image orient", side=LEFT)
            drawCombobox(self.DataFrameUI, self.imageOrient, ORIENTS, current=self.imageOrient.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.axisInvert, "Axis Invert", checked=self.axisInvert.get())
            drawCheckButton(self.DataFrameUI, self.xInvert, "X Invert", side=LEFT, checked=self.xInvert.get())
            drawCheckButton(self.DataFrameUI, self.yInvert, "Y Invert", side=RIGHT, checked=self.yInvert.get())
            
            drawCheckButton(self.DataFrameUI, self.xLimInf, "xLimInf", side=LEFT, checked=self.xLimInf.get())
            drawCheckButton(self.DataFrameUI, self.xLimSup, "xLimSup", side=RIGHT, checked=self.xLimSup.get())

            drawLabel(self.DataFrameUI, "Removed Lower", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleBefore, function=self.actualizeScaleBefore, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleBefore.get())
            drawLabel(self.DataFrameUI, "Removed Upper", side=LEFT)
            drawScale(self.DataFrameUI, self.scaleAfter, function=self.actualizeScaleAfter, from_=0.0, to=self.familyNbr.get(), resolution=1.0, side=RIGHT, set=self.scaleAfter.get())
        self.drawFamilies()
    
    def drawSpecialFocus(self) :
        """
        Adds to the DataFrameUI the buttons relative to the focus to do on the families
        """
        if self.dataFocus.get()==SPECIAL :
            drawLabel(self.DataFrameUI, "Special focus", side=LEFT)
            drawCombobox(self.DataFrameUI, self.specialFocus, SPECIAL_FOCUS, function=self.actualizeDataFrameUI, current=self.getCurrentDataFocus(self.specialFocus.get(), SPECIAL_FOCUS), side=RIGHT)
    
    def moreThanOneMgChecked(self) :
        """
        Determine if more than one microgesture is checked or not
        
        :return: number of microgestures considered
        """
        tap = self.tap.get()
        hold = self.hold.get()
        swipe = self.swipe.get()
        flex = self.flex.get()
        
        mgs=[tap, hold, swipe, flex]
        
        return len([x for x in mgs if x==True])>1
    
    def drawFamilies(self):
        """
        Adds to the DataFrameUI the buttons relative to the families to consider
        """
        drawCheckButton(self.DataFrameUI, self.seeFamilies, "Families", function=self.actualizeDataFrameUI, checked=self.seeFamilies.get(), side=LEFT)
        if self.allFamilies.get()==0 and self.seeFamilies.get()!=0:
            drawCheckButton(self.DataFrameUI, self.checkAllFamilies, "Check All", function=self.actualizeFamilies, checked=False, side=LEFT)
            self.scrollableTextFamilies = drawScrollableText(self.DataFrameUI, side=SPAN)

            drawScrollableCheckButton(self.scrollableTextFamilies, self.aandb, AANDB_NAME, function=self.actualizeScales, checked=self.aandb.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.dpa, DPA_NAME, function=self.actualizeScales, checked=self.dpa.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.dea, DEA_NAME, function=self.actualizeScales, checked=self.dea.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.ba, BA_NAME, function=self.actualizeScales, checked=self.ba.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.at, AT_NAME, function=self.actualizeScales, checked=self.at.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.chv, CHV_NAME, function=self.actualizeScales, checked=self.chv.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.atc, ATC_NAME, function=self.actualizeScales, checked=self.atc.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.chp, CHP_NAME, function=self.actualizeScales, checked=self.chp.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.hiz, HIZ_NAME, function=self.actualizeScales, checked=self.hiz.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.hib, HIB_NAME, function=self.actualizeScales, checked=self.hib.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.dsl, DSL_NAME, function=self.actualizeScales, checked=self.dsl.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.dsb, DSB_NAME, function=self.actualizeScales, checked=self.dsb.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.cws, CWS_NAME, function=self.actualizeScales, checked=self.cws.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.cns, CNS_NAME, function=self.actualizeScales, checked=self.cns.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.mst, MST_NAME, function=self.actualizeScales, checked=self.mst.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.eml, EML_NAME, function=self.actualizeScales, checked=self.eml.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.ri, RI_NAME, function=self.actualizeScales, checked=self.ri.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.mas, MAS_NAME, function=self.actualizeScales, checked=self.mas.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.elc, ELC_NAME, function=self.actualizeScales, checked=self.elc.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.gra, GRA_NAME, function=self.actualizeScales, checked=self.gra.get())
            drawScrollableCheckButton(self.scrollableTextFamilies, self.tar, TAR_NAME, function=self.actualizeScales, checked=self.tar.get())

    def drawMgOrdersParam(self) :
        """
        Adds to the DataFrameUI the buttons relative to the microgestures apparition order
        """
        if self.allMgOrders.get()==0 :
            drawLabel(self.DataFrameUI, "Microgestures order", side=SPAN)
            drawLabel(self.DataFrameUI, "Before", side=LEFT)
            self.firstMgCombobox = drawCombobox(self.DataFrameUI, self.beforeMg, MICROGESTURES, side=RIGHT, current=self.beforeMg.get())
            drawLabel(self.DataFrameUI, "After", side=LEFT)
            self.secondMgCombobox = drawCombobox(self.DataFrameUI, self.afterMg, MICROGESTURES, side=RIGHT, current=self.afterMg.get())

    def drawFamiliarities(self) :
        """
        Adds to the DataFrameUI the buttons relative to the state orders considered
        """
        if self.userFamiliarities.get()==0 :
            drawLabel(self.DataFrameUI, "Familiarities", side=LEFT)
            
            drawCombobox(self.DataFrameUI, self.familiarities, [AR_FAMILIARITY, MG_FAMILIARITY]+[x for x in list(FAMILIARITIES.values())], current=self.familiarities.get(), function=self.actualizeDataFrameUI, side=RIGHT)
            
            if self.familiarities.get()==AR_FAMILIARITY:
                drawCheckButton(self.DataFrameUI, self.arUnknown, UNKNOWN, checked=self.arUnknown.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.arHeardAbout, HEARD_ABOUT, checked=self.arHeardAbout.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.arNeverUsed, NEVER_USED, checked=self.arNeverUsed.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.arWithoutHeadset, WITHOUT_HEADSET, checked=self.arWithoutHeadset.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.arWithHeadset, WITH_HEADSET, checked=self.arWithHeadset.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.arWellKnown, WELL_KNOWN, checked=self.arWellKnown.get(), side=SPAN)
            elif self.familiarities.get()==MG_FAMILIARITY:
                drawCheckButton(self.DataFrameUI, self.mgUnknown, UNKNOWN, checked=self.mgUnknown.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.mgVagueIdeas, VAGUE_IDEAS, checked=self.mgVagueIdeas.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.mgSomeExamples, SOME_EXAMPLES, checked=self.mgSomeExamples.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.mgDiverseKnowledge, DIVERSE_KNOWLEDGE, checked=self.mgDiverseKnowledge.get(), side=SPAN)
                drawCheckButton(self.DataFrameUI, self.mgWellKnown, WELL_KNOWN, checked=self.mgWellKnown.get(), side=SPAN)

    def drawOrders(self) :
        """
        Adds to the DataFrameUI the buttons relative to the state orders considered
        """
        if self.allOrders.get()==0 :
            drawLabel(self.DataFrameUI, "Order", side=SPAN)
            drawCheckButton(self.DataFrameUI, self.sd, SD, checked=self.sd.get(), side=SPAN)
            drawCheckButton(self.DataFrameUI, self.ds, DS, checked=self.ds.get(), side=SPAN)
    
    def drawStates(self) :
        """
        Adds to the DataFrameUI the buttons relative to the states considered
        """
        if self.allStates.get()==0 :
            drawLabel(self.DataFrameUI, "State", side=SPAN)
            drawCheckButton(self.DataFrameUI, self.static, STATIC, checked=self.static.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.dynamic, DYNAMIC, checked=self.dynamic.get(), side=RIGHT)

    def drawMicrogestures(self) :
        """
        Adds to the DataFrameUI the buttons relative to the microgestures considered
        """
        if self.allMicrogestures.get()==0 :
            drawLabel(self.DataFrameUI, "Microgestures", side=SPAN)
            drawCheckButton(self.DataFrameUI, self.tap, TAP, function=self.actualizeDataFrameUI, checked=self.tap.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.hold, HOLD, function=self.actualizeDataFrameUI, checked=self.hold.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.swipe, SWIPE, function=self.actualizeDataFrameUI, checked=self.swipe.get(), side=LEFT)
            drawCheckButton(self.DataFrameUI, self.flex, FLEX, function=self.actualizeDataFrameUI, checked=self.flex.get(), side=RIGHT)
            drawCheckButton(self.DataFrameUI, self.checkAllMicrogestures, "Check All", function=self.actualizeMicrogestures, checked=self.checkAllMicrogestures.get(), side=LEFT)

    def actualizeScales(self) :
        """
        Actualize the scale values according to the last changes made on the considered variables
        """
        if (self.scaleAfter.get()+self.scaleBefore.get()>self.familyNbr.get()):
            self.scaleAfter.set(self.familyNbr.get()-self.scaleBefore.get())
    
    def actualizeScaleTop(self) :
        """
        Actualize the scale values according to the last changes made on the scaleTop
        """
        self.scaleBefore.set(self.familyNbr.get()-(self.scaleTop.get()-1))  
    
    def actualizeScaleBefore(self) :
        """
        Actualize the scale values according to the last changes made on the scaleBefore
        """
        if (self.scaleAfter.get()+self.scaleBefore.get()>self.familyNbr.get()):
            self.scaleAfter.set(self.familyNbr.get()-self.scaleBefore.get())

    def actualizeScaleAfter(self) :
        """
        Actualize the scale values according to the last changes made on the scaleAfter
        """
        if (self.scaleAfter.get()+self.scaleBefore.get()>self.familyNbr.get()):
            self.scaleBefore.set(self.familyNbr.get()-self.scaleAfter.get())

    def actualizeMicrogestures(self) :
        """
        Check or uncheck all the microgestures
        """
        if self.checkAllMicrogestures.get() :
            self.tap.set(1)
            self.hold.set(1)
            self.swipe.set(1)
            self.flex.set(1)
        else :
            self.tap.set(0)
            self.hold.set(0)
            self.swipe.set(0)
            self.flex.set(0)

    def actualizeFamilies(self) :
        """
        Check or uncheck all the families
        """
        if self.checkAllFamilies.get() :
            self.aandb.set(1)
            self.dpa.set(1)
            self.dea.set(1)
            self.ba.set(1)
            self.at.set(1)
            self.chv.set(1)
            self.atc.set(1)
            self.chp.set(1)
            self.hiz.set(1)
            self.hib.set(1)
            self.dsl.set(1)
            self.dsb.set(1)
            self.cws.set(1)
            self.cns.set(1)
            self.mst.set(1)
            self.eml.set(1)
            self.ri.set(1)
            self.mas.set(1)
            self.elc.set(1)
            self.gra.set(1)
            self.tar.set(1)
        else :
            self.aandb.set(0)
            self.dpa.set(0)
            self.dea.set(0)
            self.ba.set(0)
            self.at.set(0)
            self.chv.set(0)
            self.atc.set(0)
            self.chp.set(0)
            self.hiz.set(0)
            self.hib.set(0)
            self.dsl.set(0)
            self.dsb.set(0)
            self.cws.set(0)
            self.cns.set(0)
            self.mst.set(0)
            self.eml.set(0)
            self.ri.set(0)
            self.mas.set(0)
            self.elc.set(0)
            self.gra.set(0)
            self.tar.set(0)

###########################################################################
#######                            BAZAR                          #########
###########################################################################

    def prepareVisualize(self) :
        """
        Prepare the visualization according to the wrapping changes
        """
        self.figure = Figure()
        self.axis = self.figure.add_axes([self.xWrap.get(),self.yWrap.get(),self.widthWrap.get(),self.heightWrap.get()])
    
def tweakValuesBeforeVisualizing(df, colToFocus) :
        """
        Replace the dataframe values with the chosen clusters
        
        :param dataframe: data to work on
        :param colToFocus: column to modify
        :return: the modified dataframe
        """
        values = df[colToFocus].unique().tolist()
        if colToFocus==EXPLICITNESS_VALUE:
            df[colToFocus] = df[colToFocus].replace(values, [getRealExplicitness(x) for x in values])
        elif colToFocus==AR_FAMILIARITY:
            df[colToFocus] = df[colToFocus].replace(values, [getMiniFamArFromInt(x) for x in values])
        elif colToFocus==MG_FAMILIARITY:
            df[colToFocus] = df[colToFocus].replace(values, [getMiniFamMgFromInt(x) for x in values])
        return df
        
    