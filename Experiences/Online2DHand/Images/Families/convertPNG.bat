@echo off
for %%i in ("%~dp0SVG\*.svg") do (
    echo %%i to %%~ni.png
    "C:\Program Files\Inkscape\bin\inkscape.exe" --export-type="png" "%%i"
)


for %%i in ("%~dp0SVG\*.png") do (
    move "%%i" PNG\    
)