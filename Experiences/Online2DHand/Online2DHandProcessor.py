#!/usr/bin/env python3

import re
from os.path import exists

import numpy as np
import pandas as pd

from App.DataProcessor import DataProcessor
from App.Utils import *
from Experiences.Online2DHand.Utils.GeneralUtils import *


class Online2DHandProcessor (DataProcessor) :
    def __init__ (self) :
        """
        Instantiatation function

        :param file: the name of the parsed file
        """
        super().__init__('Online2DHand')
        
        if not (exists(self.pandaInfoFile) and exists(self.pandaDataFile)) :
            self.computeExistingMgOrders()
            
            self.infoframe = pd.DataFrame({})
            self.dataframe = pd.DataFrame({})

            self.computeGender()
            self.computeDaltonism()
            self.computeAge()
            self.computeFamiliarities()

            for state in STATES :
                for mg in MICROGESTURES :
                    self.computeStateMg(state, mg)
                    
            self.infoframe.to_csv(self.pandaInfoFile)
            self.dataframe.to_csv(self.pandaDataFile)
    
    def setVisualizer(self, visu):
        """
        Associate a visualizer to the computation class
        in order to filter the beforehand computed data

        :param visu: corresponding visualizer
        """
        self.visualizer = visu

    ## COMPUTE CSV ##

    def computeExistingMgOrders(self) :
        """
        Compute all the combinations of microgestures
        that have appear in the study and store them 
        according to their state order 
        (static or dynamic first)
        """
        self.existingMgOrders={SD:[], DS:[]}
        for participant in self.data :
            order = SD_EQUIVALENCES[participant[SD_ORDER_KEY]]
            mgOrder = mg_treatment_participant(participant[MG_ORDER_KEY])
            mgOrder = mgOrder.replace('_', '-')
            if mgOrder not in self.existingMgOrders[order] :
                self.existingMgOrders[order].append(mgOrder)

    def computeInfo(self, key, subsetFunction, analysisFunction) :
        """
        Compute the information relative to the
        given key
        
        :param key: the information to compute
        :param subsetFunction: function to apply
        on the raw data
        :param analysisFunction: function to apply
        during the analysis
        """
        for order in SD_ORDER:
            for mgOrder in self.existingMgOrders[order] :
                dataSubset = self.getDataSubset([IDENTITY, subsetFunction], [ID, key], SD_CONDITIONS[order], MG_ORDER_CONDITIONS[mgOrder])
                dataMaths = analysisFunction(dataSubset)               
                d = pd.DataFrame({key : dataMaths})
                d[ORDER] = order
                d[MG_ORDER] = mgOrder
                self.infoframe=pd.concat([d, self.infoframe], ignore_index=False)

    def computeGender(self) :
        """
        Compute the information relative to gender
        """
        self.computeInfo(GENDER, getGender, analyseGender)

    def computeDaltonism(self) :
        """
        Compute the information relative to daltonism
        """
        self.computeInfo(DALTONISM, getDaltonism, analyseDaltonism)

    def computeAge(self) :
        """
        Compute the information relative to age
        """
        self.computeInfo(AGE, IDENTITY, analyseAge)

    def computeFamiliarities(self) :
        """
        Compute the information relative to the
        familiarities
        """
        self.participantFamiliarities = self.getDataSubset([IDENTITY]+[getFamAr, getFamMg]+[getFamiliarity]*9, [ID, FAM_AR, FAM_MG, FAM_ORIGAMI, FAM_VIDEO_GAMES, FAM_MECANICS, FAM_ELECTROMAGNETISM, FAM_SPORT, FAM_ROAD_SIGNS, FAM_ART, FAM_SMARTPHONE, FAM_COMPUTER])

    def computeStateMg(self, state, mg) :
        """
        Compute the information relative to the a given
        state and microgesture
        
        :param state: static or dynamic
        :param mg: tap, swipe, flex or hold
        """
        for order in SD_ORDER:
            for mgOrder in self.existingMgOrders[order] :
                stateMg = self.getDataSubset([IDENTITY]+[getIndexFromMgMention]*21, [ID]+getKeysFor(state, mg), SD_CONDITIONS[order], MG_ORDER_CONDITIONS[mgOrder])
                stateMg = modifyKeys(stateMg)
                for family in FAMILIES :
                    fam_name = getFamilyNameFromId(family)
                    for participant in stateMg :               
                        d = getDataFrameForMg(participant[ID], order, mgOrder, state, mg, fam_name, participant[family], self.findFamiliarities(participant[ID]))
                        self.dataframe=pd.concat([d, self.dataframe], ignore_index=True)
    
    def findFamiliarities(self, participantID) :
        """
        Return the corresponding familiarities in the data for the given
        microgesture and family combination
        
        :param mg: considered microgesture
        :param family: considered family
        :return: list of corresponding familiarities
        """
        f = self.participantFamiliarities[int(participantID)]
        return mergeDict({AR_FAMILIARITY : f[FAM_AR], MG_FAMILIARITY : f[FAM_MG]},
                     {FAMILIARITIES[x] : f[x] for x in FAMILIARITIES})
        
    ## COMPUTE VISUALIZATIONS ##
    
    def getInfoState(self, searchedValue) :
        """
        Returns the information for a
        given value as two lists
        
        :param searchedValue: the value to
        obtain information for
        :return: a list of labels and a
        list of values
        """
        allOrder = self.visualizer.allOrders.get()
        order = self.visualizer.orderCombobox.get()
        df = self.infoframe.copy()
        
        df = df[~df[searchedValue].isna()]
        if allOrder!=0 :
            df[ORDER] = ALL
        else :
            if order==SD :
                df = df[df[ORDER] == SD]
            elif order==DS :
                df = df[df[ORDER] == DS]

        df = df.groupby(level=0).sum(numeric_only=True)
        labels = df.index.values.tolist()
        values = df[searchedValue].tolist()
        return labels, values

    def categoricalFocus(self, df, catFocus) :
        """
        Transform a column into a Categorical one 
        in the given dataframe
        
        :param df: the dataframe to transform
        :param catFocus: column name
        :return: the transformed dataframe
        """
        groupby_order = df.groupby([catFocus])[SCORE].mean().sort_values().index
        df[catFocus] = pd.Categorical(df[catFocus], categories=[x for x in groupby_order], ordered=True) 
        return df

    def applyModifiers(self, dataframe) :
        """
        Apply the combining modifiers
        for the given dataframe. It filters it by
        replacing the unfocused values by the
        "All" token
        
        :param dataframe: the dataframe to filter
        :return: the filtered dataframe
        """
        userAggregate = self.visualizer.userAggregate.get()
        userFamiliarities = self.visualizer.userFamiliarities.get()
        allOrder = self.visualizer.allOrders.get()
        allMgOrders = self.visualizer.allMgOrders.get()
        allState = self.visualizer.allStates.get()
        allMicrogesture = self.visualizer.allMicrogestures.get()
        allFamily = self.visualizer.allFamilies.get()
        dataFocus = self.visualizer.dataFocus.get()
        specialFocus = self.visualizer.specialFocus.get()
        
        sd = self.visualizer.sd.get()
        ds = self.visualizer.ds.get()
        static = self.visualizer.static.get()
        dynamic = self.visualizer.dynamic.get()
        before = self.visualizer.beforeMg.get()
        after = self.visualizer.afterMg.get()
        
        df = dataframe.copy()
        for catFocus in CATEGORICAL_FOCUSES : df = self.categoricalFocus(df, catFocus)

        if userAggregate!=0 :
            df = df.drop(PARTICIPANT_ID, axis=1)
            
        if userFamiliarities!=0 :
            df = df.drop(AR_FAMILIARITY, axis=1)
            df = df.drop(MG_FAMILIARITY, axis=1)
            for fam in (FAMILIARITIES.values()) :
                df = df.drop(fam, axis=1)
        else : 
            familiaritiesFocus = self.visualizer.familiarities.get()
            df = df.drop([x for x in [AR_FAMILIARITY, MG_FAMILIARITY]+[x for x in list(FAMILIARITIES.values())] if x!=familiaritiesFocus], axis=1)
            if familiaritiesFocus==AR_FAMILIARITY : 
                droppedFamiliarities = self.visualizer.getDroppedARFamiliarities()
                df = df[df[familiaritiesFocus].map(lambda x: x not in droppedFamiliarities)]
            elif familiaritiesFocus==MG_FAMILIARITY : 
                droppedFamiliarities = self.visualizer.getDroppedMGFamiliarities()
                df = df[df[familiaritiesFocus].map(lambda x: x not in droppedFamiliarities)]
                
            
        if allOrder!=0 :
            df = df.drop(ORDER, axis=1)
        else :
            droppedOrders = self.visualizer.getDroppedOrders()
            df = df[df[ORDER].map(lambda x: x not in droppedOrders)]
        
        if allMgOrders!=0 :
            df = df.drop(MG_ORDER, axis=1)
        else :
            dfBefore = df[df[MG_ORDER].isin(orderedOrders(before, after))].copy()
            dfAfter = df[df[MG_ORDER].isin(orderedOrders(after, before))].copy()
            dfBefore.loc[:, MG_ORDER] = before+"-"+after
            dfAfter.loc[:, MG_ORDER] = after+"-"+before
            df = pd.concat([dfBefore, dfAfter], ignore_index=True)
            groupby_order = df.groupby([MG_ORDER])[SCORE].mean().sort_values().index
            df[MG_ORDER] = pd.Categorical(df[MG_ORDER], categories=[x for x in groupby_order], ordered=False) 

        if allState!=0 :
            df = df.drop(STATE, axis=1)
        else :
            droppedStates = self.visualizer.getDroppedStates()
            df = df[df[STATE].map(lambda x: x not in droppedStates)]
        
        if allMicrogesture!=0 :
            df = df.drop(MICROGESTURE, axis=1)
        else :
            droppedMgs = self.visualizer.getDroppedMgs()
            df = df[df[MICROGESTURE].map(lambda x: x not in droppedMgs)]
        
        if allFamily!=0 :
            df = df.drop(FAMILY, axis=1)
        else :
            droppedFamilies = self.visualizer.getDroppedFamilies()
            df = df[df[FAMILY].map(lambda x: x not in droppedFamilies)]
        
        if dataFocus==SPECIAL : 
            if specialFocus!=EXPLICITNESS_VALUE :
                df = df.drop(EXPLICITNESS_VALUE, axis=1)
            if specialFocus!=TRAJ:
                df = df.drop(TRAJ, axis=1)
            if specialFocus!=DIR:
                df = df.drop(DIR, axis=1)
            if specialFocus!=SEPARATE:
                df = df.drop(SEPARATE, axis=1)
        else :
            df = df.drop(TRAJ, axis=1)
            df = df.drop(EXPLICITNESS_VALUE, axis=1)
            df = df.drop(DIR, axis=1)
            df = df.drop(SEPARATE, axis=1)
        
        return df
    
    def combineColumns(self, df, colToFocus, specialFocus) :
        """
        Combine all the columns which are not 
        dropped nor focused
        
        :param df: the dataframe to work on
        :param colToFocus: the column not combined
        :param specialFocus: the column not combined
        which will be the next colToFocus if the 
        latter has a certain value
        :return: the dataframe, the colToFocus, the
        combined column name and the list of the 
        original combined columns
        """
        colsToUnite = [x for x in DATA_TO_FOCUS_SPLIT_FAM if x in df.columns.to_list()]
        if colToFocus!=SPECIAL : 
            colsToUnite.remove(colToFocus)
        # if PARTICIPANT_ID in colsToUnite : colsToUnite.remove(PARTICIPANT_ID)
        # colsToUnite = [w.replace(SPECIAL_FOCUS, specialFocus) for w in colsToUnite]
        else :
             colToFocus=specialFocus
        # Combine remaining columns
        combination, df = getCombination(colsToUnite, df, self.visualizer.chariot.get())
        
        df[MEAN] =  df.groupby([combination, colToFocus])[SCORE].transform(np.mean)
        if colToFocus==EXPLICITNESS_VALUE :
            df[EXPLICITNESS_VALUE] =  df.groupby([combination, colToFocus])[EXPLICITNESS_VALUE].transform(np.mean)
            df[EXPLICITNESS_VALUE] = df[EXPLICITNESS_VALUE].astype(np.int64)
        df[STDEV] =  df.groupby([combination, colToFocus])[SCORE].transform(np.std)
        
        return df, colToFocus, combination, colsToUnite

    def getDataOrderings(self) :
        """
        Returns the computed dataframe with
        the associated orderings for each
        combination of states and microgestures
        selected in the visualizer
        
        :return: the data as a panda dataframe
        """
        self.visualizer.userAggregate.set(1)
        self.visualizer.allOrders.set(1)
        self.visualizer.allMgOrders.set(1)
        self.visualizer.dataFocus.set(FAMILY)
        self.visualizer.actualizeScaleTop()
        
        combinedStates = self.visualizer.getCombinedStates()
        combinedMgs = self.visualizer.getCombinedMgs()
        combinedMgs = sortElementsbyList(combinedMgs, MICROGESTURES)
        
        roundedMean = lambda x: np.round(np.mean(x),2)
        
        globalDf = pd.DataFrame({})
        for states in combinedStates :
            self.visualizer.updateStates(states)
            for mgs in combinedMgs :
                self.visualizer.updateMgs(mgs)
                colToFocus, combination, colsToUnite, df = self.getDataState(MEAN)
                df[MEAN] =  df.groupby([FAMILY])[MEAN].transform(roundedMean)
                df = df.drop(columns=colsToUnite+[combination, STDEV], axis=1)
                df = df.drop_duplicates()
                df = df.reset_index(drop=True)
                df = self.reorderDf(df, MEAN, FAMILY, ascending=False)
                
                newDf = pd.DataFrame({STATE : [' & '.join(states)]})
                newDfMgs = pd.DataFrame({mg:['x'] for mg in mgs})
                if self.visualizer.imageOrient.get() == ORIENT_SIDE :
                    orient = ""
                elif self.visualizer.imageOrient.get() == ORIENT_ABOVE :
                    orient = "Above"
                else :
                    orient = "Sym"
                if self.visualizer.showAnnotations.get()!=0 :
                    newDfTops = pd.DataFrame({'Top {0}'.format(index+1):["\\"+str(row[FAMILY])+orient+" \mean"+"{"+str(row[MEAN])+"}"] for index, row in df.iterrows()})
                else :
                    newDfTops = pd.DataFrame({'Top {0}'.format(index+1):["\\"+str(row[FAMILY])+orient] for index, row in df.iterrows()})
                newDf = pd.concat([newDf, newDfMgs, newDfTops], axis=1, ignore_index=False)
                globalDf = pd.concat([globalDf, newDf], ignore_index=True)
        
        latex = ""
        alreadySeen = []
        for index, row in globalDf.iterrows() :
            index = 0
            while index < len(row) :
                value = row[index]
                if isinstance(value, str) :
                    value = value.replace("& dynamic", "\\"+"& \\dynamic")
                    value = value.replace("A&B", "AandB")
                elif isinstance(value, int): break
                else : value = ""
                if index != 0 : latex += " & " 
                else : 
                    latex += ""
                    if value in alreadySeen :
                        value = ""
                    else : 
                        alreadySeen.append(value)
                        value = "\\hline\n\\multirow"+"{"+"15"+"}"+"{"+"*"+"}"+"{"+"\\ro"+"{"+"\\"+value+"}"+"}"
                latex += str(value)
                index += 1
            latex += "\\\\\n"
        print(latex)
            
        return globalDf

    def getDataState(self, searchedValue) :
        """
        Returns the computed dataframe with a focus
        on the searchedValue
        
        :param searchedValue: the value to
        focus on
        :return: the data as a panda dataframe
        """
        specialFocus = self.visualizer.specialFocus.get()
        colToFocus = self.visualizer.dataFocus.get()

        df = self.dataframe.copy()
        df = self.applyModifiers(df)
        if colToFocus==FAMILY : 
            df = self.removeUpperLowerFamilies(df)
        elif colToFocus==FAM_FAMILIARITIES :
            colToFocus = self.visualizer.familiarities.get()
        if colToFocus!=SPECIAL:
            specialFocus = colToFocus
        df, colToFocus, combination, colsToUnite = self.combineColumns(df, colToFocus, specialFocus)
        df = df.drop(columns=SCORE, axis=1)
        df = df.drop_duplicates()
        
        if colToFocus==SPECIAL:
            df = self.reorderDf(df, searchedValue, specialFocus)
        if colToFocus in CATEGORICAL_FOCUSES :
            df[colToFocus] = df[colToFocus].cat.remove_unused_categories()
        return colToFocus, combination, colsToUnite, df
    
    def removeUpperLowerFamilies(self, df) :
        """
        Filter out the unwanted families from
        the given dataframe
        
        :param df: the dataframe to work on
        :return: the filtered dataframe
        """
        allOrder = self.visualizer.allOrders.get()
        allState = self.visualizer.allStates.get()
        allMicrogesture = self.visualizer.allMicrogestures.get()
        df = self.reorderDf(df, SCORE, FAMILY)
        
        scaleStep=1
        if allOrder==False:
            scaleStep*=2
        if allState==False:
            scaleStep*=2
        if allMicrogesture==False:
            scaleStep*=4

        df = self.removeUpperFamilies(df)
        df = self.removeLowerFamilies(df)
        return df         

    def orderDataFocused(self, colToFocus, df) :
        """
        Sorts the values according to a given
        column and return them ordered
        with their associated colors as two lists
        
        :param colToFocus: the column name to use as
        sorting base 
        :param df: the dataframe to sort
        :return: a list of the ordered values in the
        focused column of df and the list of their
        corresponding colors 
        """
        df = self.reorderDf(df, MEAN, colToFocus)
        order, colors = self.getColorsFor(colToFocus, df)
        return order, colors, df

    def orderData(self, combination, colsToUnite, df) :
        """
        Sorts the values according to multiple columns
        and return them ordered with their associated
        colors as two lists. The values of the multiple
        columns are thus joined and those multiple
        columns dropped because of redundance issues
        
        :param combination: the column name to use for
        the multiple combined columns
        :param colsToUnite: the column names to use as
        sorting base 
        :return: a list of the ordered values in the
        focused columns of df and the list of their
        corresponding colors 
        """
        df = df.sort_values(colsToUnite, ascending=False, ignore_index=True)
        
        ordered_combinations = df[combination].unique()
        splited_ordered_combinations = df.drop_duplicates(subset=colsToUnite)[colsToUnite].to_numpy()
        
        colors=[]
        for comb in splited_ordered_combinations :
            colors.append(mixColorsCombination(comb, colors))

        if combination not in colsToUnite :
            df = df.drop(columns=colsToUnite)

        return ordered_combinations, colors, df
    
    def getColorsFor(self, colToFocus, df) :
        """
        Return the colors for the given column
        
        :param colToFocus: column to focus
        :param df: dataframe to work on
        :return: list of ordered values and their
        corresponding colors as another list
        """
        order = df[colToFocus].unique()
        if colToFocus not in [PARTICIPANT_ID, MG_ORDER, FAM_FAMILIARITIES] and colToFocus in DATA_TO_FOCUS :
            colors = ["#{0}".format(identifyColor(x)) for x in order]
        else :
            colors = MORE_COLORS.values()
        return order, colors
    
    def getMarkersFor(self, combination, df) :
        """
        Return a list of markers for the given
        combination
        
        :param combination: column to focus
        :param df: dataframe to work on
        :return: list of markers
        """
        markers =[]
        count = 0
        for x in df[combination].unique() :
            markers.append(identifyMark(x, count))
            count += 1
        return markers

    
    def drop(self, dropped, popped, name) :
        """
        Move the name value from the popped list
        to the dropped list
        
        :param dropped: list of values to drop after
        :param popped: list of values to pop from
        :param name: value to pop and drop
        :return: dropped and popped list after treatment
        """
        dropped.append(name)
        popped.pop(name, None)
        return dropped, popped

    def removeUpperFamilies(self, df) :
        """
        Remove the best families from the dataframe
        according to the number given by scaleAfter
        
        :param df: the dataframe to work on
        :return: the dataframe after removal 
        """
        nbr = self.visualizer.scaleAfter.get()
        families = df[FAMILY].unique().tolist()
        familiesToRemove = families[21-nbr:]
        df = df[df[FAMILY].isin(familiesToRemove) == False]
        return df

    def removeLowerFamilies(self, df) :
        """
        Remove the worst families from the dataframe
        according to the number given by scaleBefore
        
        :param df: the dataframe to work on
        :return: the dataframe after removal 
        """
        nbr = self.visualizer.scaleBefore.get()
        families = df[FAMILY].unique().tolist()
        familiesToRemove = families[:nbr]
        df = df[df[FAMILY].isin(familiesToRemove) == False]
        return df
    
    def reorderDf(self, df, value, focus, ascending=True) :
        """
        Reorder the dataframe on the given
        value according to the ascending focus
        
        :param value: the column to sort on
        :param focus: the column to groupy
        :param ascending: the sorting order
        :return: the ordered data
        """
        # Print focus column order
        if (self.visualizer.searchedValue.get()==MEAN) :
            groupby_order = df.groupby([focus])[value].mean().sort_values().index
        else :
            # Sort by median value and mean value
            sorting_median_mean = lambda x: x.median()*1000 + x.mean()
            groupby_order = df.groupby([focus])[value]
            groupby_order = groupby_order.apply(sorting_median_mean).sort_values().index
        if focus in CATEGORICAL_FOCUSES :
            df[focus] = pd.Categorical(df[focus], categories=[x for x in groupby_order], ordered=True) 
        df = df.sort_values(focus, ascending=ascending, ignore_index=True)
        return df

## UTILS ##

def countValuesForKey(dictList, key) :
    """
    Count the occurences of the values for the key in the dictionary list
    
    :param dictList: list of dictionnaries
    :param key: key to search in the dictionnaries
    :return: dictionnary of occurrences
    """
    occurences = dict()
    for dic in dictList :
        value = dic[key]
        if value in occurences :
            occurences[value] += 1
        else :
            occurences[value] = 1
    return occurences

def analyseGender(data) :
    """
    Determine the gender repartition in the data
    
    :param data: the data to examine
    :return: dictionnary of gender repartition
    """
    dataGender = getValuesForKey(data, GENDER, str) 
    return {gender : dataGender.count(gender) for gender in GENDERS}  

def analyseDaltonism(data) :
    """
    Determine the daltonism repartition in the data
    
    :param data: the data to examine
    :return: dictionnary of daltonism repartition
    """
    dataDaltonism = getValuesForKey(data, DALTONISM, str) 
    nbDaltonian = dataDaltonism.count('Daltonian')
    return {'Daltonian' : nbDaltonian, 'Not daltonian': len(dataDaltonism)-nbDaltonian}

def analyseAge(dataAge) :
    """
    Determine the age repartition in the data
    
    :param data: the data to examine
    :return: dictionnary of age statistics (sd, mean and median)
    """
    data = getValuesForKey(dataAge, AGE, int)
    return {'Sd' : np.round(np.std(data),2), 'Mean': np.round(np.mean(data),2), 'Median' : np.median(data)}

def analyseFamAr(data) :
    """
    Determine the familiarity with augmented reality in the data
    
    :param data: the data to examine
    :return: dictionnary of AR familiarity
    """
    return countValuesForKey(data, FAM_AR)

def analyseFamMg(data) :
    """
    Determine the familiarity with microgestures in the data
    
    :param data: the data to examine
    :return: dictionnary of microgestures familiarity
    """
    return countValuesForKey(data, FAM_MG)

def getGender(genderString) :
    """
    Extract the exact gender string from the raw gender data
    
    :param genderString: the data given by the participant
    :return: participant gender
    """
    genderString = re.sub(r'[^\w\s]','', genderString).lower().replace("é", "e")

    if genderString in ["masculin", "homme", "male", "monsieur", "m", "Français", "français", "Francais", "francais"] :
        return "Male"
    elif genderString in ["feminin", "femme", "femelle", "madame", "mademoiselle", "f", "Française", "française", "Francaise", "francaise"] :
        return "Female"
    else :
        return "Other"

def getDaltonism(s) :
    """
    Determine if the participant is daltonian or not
    
    :param s: the data given by the participant
    :return: daltonism of the participant
    """
    if s.lower() == "oui" : return "Daltonian"
    else : return ""

def getKeysFor(state, mg) :
    """
    Return the corresponding column name in the data for the given
    microgesture and state
    
    :param mg: considered microgesture
    :param state: considered state
    :return: data column name
    """
    return ["mg_{0}_{1}{2}".format(mg, family, state[0].upper()) for family in FAMILIES]

def getIndexFromMgMention(s) :
    """
    Associate a numeric value to each score
    given by the participant
    
    :param s: string score
    :return: int score
    """
    return {
        "Très mauvais": 1,
        "Mauvais": 2,
        "Assez mauvais": 3,
        "Assez bon": 4,
        "Bon": 5,
        "Très bon": 6,
    }[s]

def getFamiliarity(s) :
    """
    Associate a 1 if the participant has indicated
    being familiar with the field and a 0 otherwise
    
    :param s: string "on" or nothing
    :return: binary value
    """
    if s=="on": return 1
    return 0

def getCoupleFromFamAr(s) :
    """
    Associate a tuple (numeric value, label) to each 
    familiarity with AR expressed by the participant
    
    :param s: string score
    :return: (int, label) score
    """
    return {
        "Je ne connais pas cette technologie": (1, "Unknown"),
        "J'en ai entendu parler mais sans plus": (2, "Heard about"),
        "Je comprends ce que c'est mais je ne l'ai jamais utilisée": (3, "Never used"),
        "J'ai déjà utilisé la réalité augmentée mais sans casque": (4, "Without headset"),
        "J'ai déjà utilisé un casque de réalité augmentée": (5, "With headset"),
        "Je connais très bien cette technologie": (6, "Well known"),
    }[s]

def getCoupleFromFamMg(s) :
    """
    Associate a tuple (numeric value, label) to each 
    familiarity with microgestures expressed by the participant
    
    :param s: string score
    :return: (int, label) score
    """
    return {
        "Je n'ai jamais porté attention à comment étaient représentés les gestes": (1, "Unknown"),
        "J'ai quelques exemples de représentations en tête": (2, "Vague ideas"),
        "Je pourrais donner une certaine diversité d'exemples": (3, "Some examples"),
        "Je vois bien comment représenter la gestuelle et ce dans différents domaines": (4, "Diverse knowledge"),
        "Je suis très familier avec ces notions": (5, "Well known"),
    }[s]

def getDataFrameForMg(participantId, order, mgOrder, state, mg, fam_name, score, familiarities) :
    """
    Return a dictionnary with the data associated with the given participant's score
    
    :param participantId: id of the participant
    :param order: state order (static or dynamic first)
    :param mgOrder: microgestures order
    :param state: static or dynamic
    :param mg: tap, swipe, flex or hold
    :param fam_name: considered family
    :param score: score given by the participant
    :return: dictionnary with the score and its context
    """
    
    if mg == TAP :
        d = mergeDict({PARTICIPANT_ID : participantId,
             ORDER : order, 
             MG_ORDER : mgOrder, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             EXPLICITNESS_VALUE: EXPLICITNESS_VALUE_DICT[mg][fam_name],
             TRAJ: getKeyByValue(TRAJECTORIES_DICT, fam_name),
             DIR: getKeyByValue(DIRECTIONS_DICT, fam_name),
             SEPARATE: getKeyByValue(SEPARATE_DICT[mg], fam_name),
             SCORE: score,
             AR_FAMILIARITY: familiarities[AR_FAMILIARITY],
             MG_FAMILIARITY: familiarities[MG_FAMILIARITY]},
             {x: [familiarities[x]] for x in FAMILIARITIES.values()})
    elif mg == SWIPE :
        d = mergeDict({PARTICIPANT_ID : participantId,
             ORDER : order, 
             MG_ORDER : mgOrder, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             EXPLICITNESS_VALUE: EXPLICITNESS_VALUE_DICT[mg][fam_name],
             TRAJ: getKeyByValue(TRAJECTORIES_DICT, fam_name),
             DIR: getKeyByValue(DIRECTIONS_DICT, fam_name),
             SCORE: score,
             AR_FAMILIARITY: familiarities[AR_FAMILIARITY],
             MG_FAMILIARITY: familiarities[MG_FAMILIARITY]},
             {x: [familiarities[x]] for x in FAMILIARITIES.values()})
    elif mg == FLEX :
        d = mergeDict({PARTICIPANT_ID : participantId,
             ORDER : order, 
             MG_ORDER : mgOrder, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             EXPLICITNESS_VALUE: EXPLICITNESS_VALUE_DICT[mg][fam_name],
             TRAJ: getKeyByValue(TRAJECTORIES_DICT, fam_name),
             DIR: getKeyByValue(DIRECTIONS_DICT, fam_name),
             SCORE: score,
             AR_FAMILIARITY: familiarities[AR_FAMILIARITY],
             MG_FAMILIARITY: familiarities[MG_FAMILIARITY]},
             {x: [familiarities[x]] for x in FAMILIARITIES.values()})
    elif mg == HOLD :
        d = mergeDict({PARTICIPANT_ID : participantId,
             ORDER : order, 
             MG_ORDER : mgOrder, 
             STATE : state, 
             MICROGESTURE : mg, 
             FAMILY : fam_name,
             EXPLICITNESS_VALUE: EXPLICITNESS_VALUE_DICT[mg][fam_name],
             TRAJ: getKeyByValue(TRAJECTORIES_DICT, fam_name),
             DIR: getKeyByValue(DIRECTIONS_DICT, fam_name),
             SEPARATE: getKeyByValue(SEPARATE_DICT[mg], fam_name),
             SCORE: score,
             AR_FAMILIARITY: familiarities[AR_FAMILIARITY],
             MG_FAMILIARITY: familiarities[MG_FAMILIARITY]},
             {x: [familiarities[x]] for x in FAMILIARITIES.values()})
    return pd.DataFrame(data=d)


def modifyKey(key) :
    """
    Returns the state order from a key with the shape mg_{}_{state}{}
    
    :param key: the key to modify
    :return: the state from the given key
    """
    if key==ID : return key
    return key[-2:-1]

def modifyKeys(participantList) :
    """
    Allows to handle a list of participants data
    with a special treatment applied on the keys
    
    :param participantList: list of participants data
    :return: modified list
    """
    modifiedList = []
    for p in participantList :
        d = {modifyKey(k):p[k] for k in p}
        modifiedList.append(d)
    return modifiedList

def sortElementsbyList(listOfLists, sortingList) :
    priorityDict = dict(zip(sortingList, range(len(sortingList))))
    listOfLists.sort(key=lambda x : priorityValue(x, priorityDict), reverse=True)
    return listOfLists

def priorityValue(list, priorityDict) :
    upper = len(priorityDict.keys())
    values = [(upper-priorityDict[x])**3+(upper-i) for i, x in enumerate(list)]
    return sum(values)