#!/usr/bin/env python3

from Experiences.Online2DHand.Utils.GeneralUtils import ID, SCORE

def mathsAnalysis(data, columnIds) :
    """
    Prepare the visualization according to the wrapping changes
    """
    array = []
    ids = []
    for participant in data :
        notes = []
        for key in participant:
            if key != ID :
                notes.append(participant[key])
            else :
                ids.append(participant[key])
        array.append(notes)

    candidates=[]
    narray = [list(i) for i in zip(*array)]
    for i in range(len(narray)) :
        new_candidate = { ID : columnIds[i], SCORE: narray[i]}
        candidates.append(new_candidate)
    candidates = idSort(analyseData(candidates))
    return ids, candidates

def idSort(array) :
    outArray = []
    sortedArray = sorted(array, key = lambda x : x[ID])
    for dict in sortedArray :
        outArray.append({ID: dict[ID], SCORE: dict[SCORE]})
    return outArray

def analyseData(candidates) :
    candidates = list(map(assignSortedVotes, candidates))
    return candidates

def assignSortedVotes(candidate) :
    candidate[SCORE] = sorted(candidate[SCORE])
    return candidate