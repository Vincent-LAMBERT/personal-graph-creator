#!/usr/bin/env python3

import os
import re

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.offsetbox import AnnotationBbox, OffsetImage
from svgpath2mpl import parse_path

from Experiences.Online2DHand.Utils.FamiliesUtils import *

FAMILY_NBR = 21
ID="id"
GENDER="genre"
GENDERS=["Male", "Female", "Other"]
AGE="age"
DALTONISM="daltonisme"
SD_ORDER_KEY="array_static_dynamic"
MG_ORDER_KEY="array_action_order"
FAM_AR="familiarite_ra"
FAM_MG="familiarite_mg"
FAM_ORIGAMI="origami"
FAM_VIDEO_GAMES="jeuxvideo"
FAM_MECANICS="mecanique"
FAM_ELECTROMAGNETISM="electromagnetisme"
FAM_SPORT="sport"
FAM_ROAD_SIGNS="codedelaroute"
FAM_ART="art"
FAM_SMARTPHONE="smartphone"
FAM_COMPUTER="ordinateur"
FAM_FAMILIARITIES=[FAM_ORIGAMI, 
                   FAM_VIDEO_GAMES, 
                   FAM_MECANICS, 
                   FAM_ELECTROMAGNETISM,
                   FAM_SPORT,
                   FAM_ROAD_SIGNS, 
                   FAM_ART, 
                   FAM_SMARTPHONE, 
                   FAM_COMPUTER]
ORIGAMI_FAMILIARITY="Origami"
VIDEO_GAMES_FAMILIARITY="Video games"
MECANICS_FAMILIARITY="Mecanics"
ELECTROMAGNETISM_FAMILIARITY="Electromagnetism"
SPORT_FAMILIARITY="Sport"
ROAD_SIGNS_FAMILIARITY="Road signs"
ART_FAMILIARITY="Art"
SMARTPHONE_FAMILIARITY="Smartphone"
COMPUTER_FAMILIARITY="Computer"
AR_FAMILIARITY="AR"
MG_FAMILIARITY="Microgestures"
FAMILIARITIES={FAM_ORIGAMI : ORIGAMI_FAMILIARITY, 
               FAM_VIDEO_GAMES : VIDEO_GAMES_FAMILIARITY, 
               FAM_MECANICS : MECANICS_FAMILIARITY, 
               FAM_ELECTROMAGNETISM : ELECTROMAGNETISM_FAMILIARITY,
               FAM_SPORT : SPORT_FAMILIARITY,
               FAM_ROAD_SIGNS : ROAD_SIGNS_FAMILIARITY, 
               FAM_ART : ART_FAMILIARITY, 
               FAM_SMARTPHONE : SMARTPHONE_FAMILIARITY, 
               FAM_COMPUTER : COMPUTER_FAMILIARITY}

ALL_MG="All"
ALL="All"
FAMILIES = [chr(a) for a in range(65, 65+21)]
TAP="tap"
SWIPE="swipe"
FLEX="flex"
HOLD="hold"
STATIC="static"
DYNAMIC="dynamic"
QUANTITY="quantity"
PERCENTAGE="percentage"
MATHS="Maths"
SPACE=" "
MICROGESTURES = [TAP, HOLD, SWIPE, FLEX]
MICROGESTURES_GUI = [ALL, TAP, HOLD, SWIPE, FLEX]
STATES = [STATIC, DYNAMIC]
STATES_GUI = [ALL]+STATES

ARROW=" => "
FIRST = "1st"
SD=STATIC+SPACE+FIRST
DS=DYNAMIC+SPACE+FIRST

STATES_COLORS = {STATIC : "#BD2222",
                 DYNAMIC : "#2F22BD"}
MG_COLORS = {TAP : "#267123",
            HOLD : "#4E9E4B",
            SWIPE : "#7CC17A",
            FLEX : "#B7EAB5"}
ORDER_COLORS = {SD: "#B445A0",
                DS: "#4BB445"}

STDEV="stdev"
RANK="rank"
SIZE="size"

def effectSize(means, stdevs) :
    effectSizes=[]
    for i in range(len(means)):
        effectSizes.append(computeEffectSize(means[i], stdevs[i]))
    return pd.Series(effectSizes)

def computeEffectSize(mean, stdev) :
    return mean/stdev

SCORES = {"Very bad" : 1, "Bad" : 2,  "Quite bad" : 3, "Quite good" : 4, "Good" : 5, "Very good" : 6}
REPLETTERTONBR = {chr(a):a-64 for a in range(65, 65+21)}
REPNBRTOLETTER = {a-64:chr(a) for a in range(65, 65+21)}
IDENTITY=lambda x:x

PARTICIPANT_ID='Participant ID'
ORDER='Order'
MG_ORDER="Mg order"
STATE='State'
MICROGESTURE='Microgesture'
FAMILY='Family'
FAM_FAMILIARITIES="Familiarities"
TRAJ="Trajectory"
DIR="Direction"
SEPARATE = "Separate"
EXPLICITNESS_STRING="Explicitness Str"
EXPLICITNESS_VALUE="Explicitness Val"
SPECIAL_FOCUS=[EXPLICITNESS_VALUE, TRAJ, DIR, SEPARATE]
SCORE="Note"
SPECIAL="Special focus"
DATA_TO_FOCUS=[PARTICIPANT_ID, ORDER, MG_ORDER, STATE, MICROGESTURE, FAMILY, FAM_FAMILIARITIES, SPECIAL]
DATA_TO_FOCUS_SPLIT_FAM=[PARTICIPANT_ID, ORDER, MG_ORDER, STATE, MICROGESTURE, FAMILY, AR_FAMILIARITY, MG_FAMILIARITY,
                         ORIGAMI_FAMILIARITY, VIDEO_GAMES_FAMILIARITY, MECANICS_FAMILIARITY, ELECTROMAGNETISM_FAMILIARITY,
                         SPORT_FAMILIARITY, ROAD_SIGNS_FAMILIARITY, ART_FAMILIARITY, SMARTPHONE_FAMILIARITY, COMPUTER_FAMILIARITY]
CATEGORICAL_FOCUSES = [PARTICIPANT_ID, STATE, ORDER, MICROGESTURE, FAMILY]

PIE="Pie"
BOX="Box"
STRIP="Strip"
VIOLIN="Violin"
BAR="Bar"
SCATTER="Scatter"
ELLIPSES="Ellipses"
MULTIPLE_LINES="Multiple lines"
MULTIPLE_BARS="Multiple bars"
MULTIPLE_BOXES="Multiple boxes"
MULTIPLE_VIOLIN="Multiple violin"
EFFECT="Effect sizes"
ORDERING="Ordering"
REGPLOT="Regplot"
GRAPH_TYPES = [PIE, STRIP, BOX, VIOLIN, BAR, SCATTER, ELLIPSES, MULTIPLE_BARS, MULTIPLE_LINES, MULTIPLE_BOXES, MULTIPLE_VIOLIN, EFFECT, ORDERING, REGPLOT]


AANDB_NAME="A&B"
DPA_NAME="DpA"
DEA_NAME="DeA"
BA_NAME="BA"
AT_NAME="At"
CHV_NAME="Chv"
ATC_NAME="AtC"
CHP_NAME="Chp"
HIZ_NAME="HiZ"
HIB_NAME="HiB"
DSL_NAME="DsL"
DSB_NAME="DsB"
CWS_NAME="CwS"
CNS_NAME="CnS"
MST_NAME="MSt"
EML_NAME="EmL"
RI_NAME="Ri"
MAS_NAME="MaS"
ELC_NAME="ElC"
GRA_NAME="Gra"
TAR_NAME="Tar"

ORIENT_SIDE="Side"
ORIENT_ABOVE="Above"
ORIENT_NONE="None"
ORIENTS=[ORIENT_SIDE, ORIENT_ABOVE, ORIENT_NONE]

SYMBOLS={AANDB_NAME : AANDB, DPA_NAME : DPA, DEA_NAME : DEA, BA_NAME : BA, AT_NAME : AT, CHV_NAME : CHV, ATC_NAME : ATC, CHP_NAME : CHP, HIZ_NAME : HIZ, HIB_NAME : HIB, DSL_NAME : DSL, DSB_NAME : DSB, CWS_NAME : CWS, CNS_NAME : CNS, MST_NAME : MST, EML_NAME : EML, RI_NAME : RI, MAS_NAME : MAS, ELC_NAME : ELC, GRA_NAME : GRA, TAR_NAME : TAR}

def computeMark(family) :
    sym = parse_path(SYMBOLS[family])
    sym.vertices -= sym.vertices.mean(axis=0)
    return sym

FAM_MARKERS={f:computeMark(f) for f in SYMBOLS}

FAM_COLORS={AANDB_NAME : "#C68D8D", DPA_NAME : "#C7A884", DEA_NAME : "#BBBB79", BA_NAME : "#EC9CB0", AT_NAME : "#FAC5A0", CHV_NAME : "#B8F2FF", ATC_NAME : "#FCEF9C", CHP_NAME : "#DEF3AB", HIZ_NAME : "#CEF8DB", HIB_NAME : "#90BA96", DSL_NAME : "#EBDCFF", DSB_NAME : "#A283AC", CWS_NAME : "#CEA0B1", CNS_NAME : "#F3D6B9", MST_NAME : "#CECACA", EML_NAME : "#6E6E93", RI_NAME : "#979E81", MAS_NAME : "#6C9A95", ELC_NAME : "#907789", GRA_NAME : "#7DA091", TAR_NAME : "#C177C9"}

def get_flag(name, orient):
    path = os.path.dirname(__file__)
    path = path+"/../Images/Families/Icons/"+orient+"/PNG/{}.png".format(name)
    im = plt.imread(path)
    return im

def offsetXImage(coord, name, ax, orient):
    img = get_flag(name.get_text(), orient)
    
    if orient == ORIENT_SIDE :
        margin = 33.
        zoom=0.70
    elif orient == ORIENT_ABOVE :
        margin = 40.
        zoom=0.70
    elif orient == ORIENT_NONE :
        margin = 35.
        zoom=0.47


    im = OffsetImage(img, zoom=zoom)
    im.image.axes = ax
    ab = AnnotationBbox(im, coord,  xybox=(0., -margin), frameon=False,
                        xycoords='data',  boxcoords="offset points", pad=0)

    ax.add_artist(ab)

def offsetYImage(coord, name, ax, orient):
    img = get_flag(name.get_text(), orient)
    im = OffsetImage(img, zoom=0.70)
    im.image.axes = ax

    ab = AnnotationBbox(im, coord,  xybox=(-33., 0.), frameon=False,
                        xycoords='data',  boxcoords="offset points", pad=0)

    ax.add_artist(ab)

## UTILS ##

def getFamilyNameFromId(id) :
    return {
        "A" : AANDB_NAME,
        "B" : DPA_NAME,
        "C" : DEA_NAME,
        "D" : BA_NAME,
        "E" : AT_NAME,
        "F" : CHV_NAME,
        "G" : ATC_NAME,
        "H" : CHP_NAME,
        "I" : HIZ_NAME,
        "J" : HIB_NAME,
        "K" : DSL_NAME,
        "L" : DSB_NAME,
        "M" : CWS_NAME,
        "N" : CNS_NAME,
        "O" : MST_NAME,
        "P" : EML_NAME,
        "Q" : RI_NAME,
        "R" : MAS_NAME,
        "S" : ELC_NAME,
        "T" : GRA_NAME,
        "U" : TAR_NAME,
    }[id]

def getValuesForKey(dictList, key, function=IDENTITY) :
    return [function(dictList[i][key]) for i in range(len(dictList))]

def getGender(genderString) :
    genderString = re.sub(r'[^\w\s]','', genderString).lower().replace("é", "e")

    if genderString in ["masculin", "homme", "male", "monsieur", "m", "Français", "français", "Francais", "francais"] :
        return "Male"
    elif genderString in ["feminin", "femme", "femelle", "madame", "mademoiselle", "f", "Française", "française", "Francaise", "francaise"] :
        return "Female"
    else :
        return "Other"

def getDaltonism(s) :
    if s.lower() == "oui" : return "Daltonian"
    else : return ""


FAMILIAR="Familiar"
NOT_FAMILIAR="Not familiar"

UNKNOWN="Unknown"
HEARD_ABOUT="Heard\nabout"
NEVER_USED="Never\nused"
WITHOUT_HEADSET="Without\nheadset"
WITH_HEADSET="With\nheadset"
WELL_KNOWN="Well\nknown"
    
def getMiniFamArFromInt(s) :
    return {
        1: NOT_FAMILIAR, # UNKNOWN,
        2: NOT_FAMILIAR, # HEARD_ABOUT,
        3: NOT_FAMILIAR, # NEVER_USED,
        4: FAMILIAR, # WITHOUT_HEADSET,
        5: FAMILIAR, # WITH_HEADSET,
        6: FAMILIAR, # WELL_KNOWN,
    }[s]
    
def getFamAr(s) :
    return {
        "Je ne connais pas cette technologie": 1,
        "J'en ai entendu parler mais sans plus": 2,
        "Je comprends ce que c'est mais je ne l'ai jamais utilisée": 3,
        "J'ai déjà utilisé la réalité augmentée mais sans casque": 4,
        "J'ai déjà utilisé un casque de réalité augmentée": 5,
        "Je connais très bien cette technologie": 6
    }[s]

VAGUE_IDEAS="Vague\nideas"
SOME_EXAMPLES="Some\nexamples"
DIVERSE_KNOWLEDGE="Diverse\nknowledge"

def getMiniFamMgFromInt(s) :
    return {
        1: NOT_FAMILIAR, # UNKOWN,
        2: NOT_FAMILIAR, # VAGUE_IDEAS,
        3: NOT_FAMILIAR, # SOME_EXAMPLES,
        4: FAMILIAR, # DIVERSE_KNOWLEDGE,
        5: FAMILIAR # WELL_KNOWN,
    }[s]

def getFamMg(s) :
    return {
        "Je n'ai jamais porté attention à comment étaient représentés les gestes": 1,
        "J'ai quelques exemples de représentations en tête": 2,
        "Je pourrais donner une certaine diversité d'exemples": 3,
        "Je vois bien comment représenter la gestuelle et ce dans différents domaines": 4,
        "Je suis très familier avec ces notions": 5
    }[s]

SD_ORDER=[SD, DS]
SD_ORDER_GUI=[ALL]+SD_ORDER
D_S = "D_S"
S_D = "S_D"
SD_EQUIVALENCES={D_S : DS, S_D : SD}
SD_CONDITIONS={ ALL: lambda x:True, 
                SD : lambda x:matchingSdOrder(x, SD), 
                DS : lambda x:matchingSdOrder(x, DS)}

def removeAssembledMgs(stateData) :
    stateData.pop(TAP, None)
    stateData.pop(SWIPE, None)
    stateData.pop(FLEX, None)
    stateData.pop(HOLD, None)
    return stateData

def matchingSdOrder(participant, order) :
    return SD_EQUIVALENCES[participant[SD_ORDER_KEY]] == order

def mg_treatment_participant(st) :
    st = st.replace("-S", "")
    st = st.replace("-D", "")
    return st

def mg_treatment_utils_side(ar) :
    st = '_'.join(ar)
    return st

def matchingMgOrder(participant, mgOrder) :
    return mg_treatment_participant(participant[MG_ORDER_KEY]) == mg_treatment_utils_side(mgOrder)


TA_SW_ST_HO = [TAP, SWIPE, FLEX, HOLD]
TA_SW_HO_ST = [TAP, SWIPE, HOLD, FLEX]
TA_ST_SW_HO = [TAP, FLEX, SWIPE, HOLD]
TA_ST_HO_SW = [TAP, FLEX, HOLD, SWIPE]
TA_HO_SW_ST = [TAP, HOLD, SWIPE, FLEX]
TA_HO_ST_SW = [TAP, HOLD, FLEX, SWIPE]
SW_TA_ST_HO = [SWIPE, TAP, FLEX, HOLD]
SW_TA_HO_ST = [SWIPE, TAP, HOLD, FLEX]
SW_ST_TA_HO = [SWIPE, FLEX, TAP, HOLD]
SW_ST_HO_TA = [SWIPE, FLEX, HOLD, TAP]
SW_HO_TA_ST = [SWIPE, HOLD, TAP, FLEX]
SW_HO_ST_TA = [SWIPE, HOLD, FLEX, TAP]
ST_SW_TA_HO = [FLEX, SWIPE, TAP, HOLD]
ST_SW_HO_TA = [FLEX, SWIPE, HOLD, TAP]
ST_TA_SW_HO = [FLEX, TAP, SWIPE, HOLD]
ST_TA_HO_SW = [FLEX, TAP, HOLD, SWIPE]
ST_HO_SW_TA = [FLEX, HOLD, SWIPE, TAP]
ST_HO_TA_SW = [FLEX, HOLD, TAP, SWIPE]
HO_SW_ST_TA = [HOLD, SWIPE, FLEX, TAP]
HO_SW_TA_ST = [HOLD, SWIPE, TAP, FLEX]
HO_ST_SW_TA = [HOLD, FLEX, SWIPE, TAP]
HO_ST_TA_SW = [HOLD, FLEX, TAP, SWIPE]
HO_TA_SW_ST = [HOLD, TAP, SWIPE, FLEX]
HO_TA_ST_SW = [HOLD, TAP, FLEX, SWIPE]

MG_ORDER_CONDITIONS={ '-'.join(TA_SW_ST_HO) : lambda x:matchingMgOrder(x, TA_SW_ST_HO), 
                      '-'.join(TA_SW_HO_ST) : lambda x:matchingMgOrder(x, TA_SW_HO_ST), 
                      '-'.join(TA_ST_SW_HO) : lambda x:matchingMgOrder(x, TA_ST_SW_HO), 
                      '-'.join(TA_ST_HO_SW) : lambda x:matchingMgOrder(x, TA_ST_HO_SW), 
                      '-'.join(TA_HO_SW_ST) : lambda x:matchingMgOrder(x, TA_HO_SW_ST), 
                      '-'.join(TA_HO_ST_SW) : lambda x:matchingMgOrder(x, TA_HO_ST_SW), 
                      '-'.join(SW_TA_ST_HO) : lambda x:matchingMgOrder(x, SW_TA_ST_HO), 
                      '-'.join(SW_TA_HO_ST) : lambda x:matchingMgOrder(x, SW_TA_HO_ST),
                      '-'.join(SW_ST_TA_HO) : lambda x:matchingMgOrder(x, SW_ST_TA_HO),
                      '-'.join(SW_ST_HO_TA) : lambda x:matchingMgOrder(x, SW_ST_HO_TA),
                      '-'.join(SW_HO_TA_ST) : lambda x:matchingMgOrder(x, SW_HO_TA_ST),
                      '-'.join(SW_HO_ST_TA) : lambda x:matchingMgOrder(x, SW_HO_ST_TA),
                      '-'.join(ST_SW_TA_HO) : lambda x:matchingMgOrder(x, ST_SW_TA_HO),
                      '-'.join(ST_SW_HO_TA) : lambda x:matchingMgOrder(x, ST_SW_HO_TA),
                      '-'.join(ST_TA_SW_HO) : lambda x:matchingMgOrder(x, ST_TA_SW_HO),
                      '-'.join(ST_TA_HO_SW) : lambda x:matchingMgOrder(x, ST_TA_HO_SW),
                      '-'.join(ST_HO_SW_TA) : lambda x:matchingMgOrder(x, ST_HO_SW_TA),
                      '-'.join(ST_HO_TA_SW) : lambda x:matchingMgOrder(x, ST_HO_TA_SW),
                      '-'.join(HO_SW_ST_TA) : lambda x:matchingMgOrder(x, HO_SW_ST_TA),
                      '-'.join(HO_SW_TA_ST) : lambda x:matchingMgOrder(x, HO_SW_TA_ST),
                      '-'.join(HO_ST_SW_TA) : lambda x:matchingMgOrder(x, HO_ST_SW_TA),
                      '-'.join(HO_ST_TA_SW) : lambda x:matchingMgOrder(x, HO_ST_TA_SW),
                      '-'.join(HO_TA_SW_ST) : lambda x:matchingMgOrder(x, HO_TA_SW_ST),
                      '-'.join(HO_TA_ST_SW) : lambda x:matchingMgOrder(x, HO_TA_ST_SW)}

MICROGESTURES_ORDERS = [TA_SW_ST_HO,
                        TA_SW_HO_ST,
                        TA_ST_SW_HO,
                        TA_ST_HO_SW,
                        TA_HO_SW_ST,
                        TA_HO_ST_SW,
                        SW_TA_ST_HO,
                        SW_TA_HO_ST,
                        SW_ST_TA_HO,
                        SW_ST_HO_TA,
                        SW_HO_TA_ST,
                        SW_HO_ST_TA,
                        ST_SW_TA_HO,
                        ST_SW_HO_TA,
                        ST_TA_SW_HO,
                        ST_TA_HO_SW,
                        ST_HO_SW_TA,
                        ST_HO_TA_SW,
                        HO_SW_ST_TA,
                        HO_SW_TA_ST,
                        HO_ST_SW_TA,
                        HO_ST_TA_SW,
                        HO_TA_SW_ST,
                        HO_TA_ST_SW]

def orderedOrders(before, after) :
    orderedOrders = []
    for order_index in range(len(MICROGESTURES_ORDERS)) :
        order = MICROGESTURES_ORDERS[order_index]
        selected = False
        for mg in order :
            if mg == before :
                selected = True
            if mg == after and selected==True:
                orderedOrders.append(MG_ORDERS[order_index])
    return orderedOrders

def matchingOrders(place, mg) :
    matching_orders = []
    for order_index in range(len(MICROGESTURES_ORDERS)) :
        if MICROGESTURES_ORDERS[order_index][place]==mg :
            matching_orders.append(MG_ORDERS[order_index])
    return matching_orders

MG_ORDERS=['-'.join(x) for x in MICROGESTURES_ORDERS]



class RGB(np.ndarray):
  @classmethod
  def from_str(cls, rgbstr):
    return np.array([
      int(rgbstr[i:i+2], 16)
      for i in range(1, len(rgbstr), 2)
    ]).view(cls)
 
  def __str__(self):
    self = self.astype(np.uint8)
    return '#' + ''.join(format(n, 'x') for n in self)

def getKeyByValue(dictOfElements, valueToFind):
    for item in dictOfElements :
        if valueToFind in dictOfElements[item] :
            return item
    return None


palette = sns.color_palette("husl", 45).as_hex()
MORE_COLORS= {x : palette[x] for x in range(45)}
    
def mixColorsCombination(combination, cols) :
    d = {identifyColor(combination[k]):1-k/len(combination) for k in range(len(combination))}
    colors = [x for x in MORE_COLORS.values() if str(x).lower() not in cols]
    d_items = list(d.items())
    for i in range(len(d_items)) :
        if d_items[i][0]==None:
            c = colors.pop()
            d_items[i]=(c[1:], d_items[i][1])
    d_items = sorted(d_items)
    tot_weight = sum(d.values())
    red = int(sum([int(k[:2], 16)*v for k, v in d_items])/tot_weight)
    green = int(sum([int(k[2:4], 16)*v for k, v in d_items])/tot_weight)
    blue = int(sum([int(k[4:6], 16)*v for k, v in d_items])/tot_weight)
    zpad = lambda x: x if len(x)==2 else '0' + x
    return "#" + zpad(hex(red)[2:]) + zpad(hex(green)[2:]) + zpad(hex(blue)[2:])

DARK="#262626"

def darkenColor(col) :
    d = {col[1:]: 0.5, DARK[1:]: 0.5}
    d_items = sorted(d.items())
    tot_weight = sum(d.values())
    red = int(sum([int(k[:2], 16)*v for k, v in d_items])/tot_weight)
    green = int(sum([int(k[2:4], 16)*v for k, v in d_items])/tot_weight)
    blue = int(sum([int(k[4:6], 16)*v for k, v in d_items])/tot_weight)
    zpad = lambda x: x if len(x)==2 else '0' + x
    color = "#" + zpad(hex(red)[2:]) + zpad(hex(green)[2:]) + zpad(hex(blue)[2:])
    return color
    

def identifyColor(stri) :
    if isinstance(stri, int) :
        return MORE_COLORS[stri][1:]
    if stri in SD_ORDER :
        return ORDER_COLORS[stri][1:]
    if stri in STATES :
        return STATES_COLORS[stri][1:]
    if stri in MICROGESTURES :
        return MG_COLORS[stri][1:]
    if stri in FAM_COLORS :
        return FAM_COLORS[stri][1:]
    if stri in EXPLICITNESS_VALUE_COLORS :
        return EXPLICITNESS_VALUE_COLORS[stri][1:]
    if stri in TRAJECTORIES_COLORS :
        return TRAJECTORIES_COLORS[stri][1:]
    
DEFAULT_MARKERS = [',', '.', 'o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X']*200

def identifyMark(str, i) :
    if str in FAM_MARKERS :
        return FAM_MARKERS[str]
    else :
        return DEFAULT_MARKERS[i]
    
def getCombination(colsToUnite, df, chariot=False) :
    combination = ' '.join(colsToUnite)
    df[combination] = df[colsToUnite].agg(lambda x : aggregate(x, chariot), axis=1)
    return combination, df

def aggregate(value, chariot) :
    if chariot : joint='\n'
    else : joint=' '
    strList = [str(x) for x in value.to_list()]
    return joint.join(strList)

TRAJ_EXPLICIT = "Traj Explicit"
TRAJ_IMPLICIT = "Traj Implicit"
TRAJ_NONE = "Traj None"


TRAJECTORIES_DICT = { TRAJ_EXPLICIT : {AANDB_NAME, DPA_NAME, BA_NAME, CHV_NAME, CHP_NAME, DSB_NAME, MST_NAME, GRA_NAME}, 
                      TRAJ_IMPLICIT : {MAS_NAME, ELC_NAME, TAR_NAME},
                      TRAJ_NONE : {DEA_NAME, AT_NAME, ATC_NAME, HIZ_NAME, HIB_NAME, DSL_NAME, CWS_NAME, CNS_NAME, EML_NAME, RI_NAME}}

TRAJECTORIES_COLORS = { TRAJ_EXPLICIT : "#9C954C", TRAJ_IMPLICIT : "#A34B4B", TRAJ_NONE : "#A350A0"}

DIR_EXPLICIT = "Dir Explicit"
DIR_NONE = "Dir None"

DIRECTIONS_DICT = { DIR_EXPLICIT : {AANDB_NAME, DPA_NAME, BA_NAME, CHV_NAME}, 
                    DIR_NONE : {DEA_NAME, AT_NAME, ATC_NAME, HIZ_NAME, HIB_NAME, DSL_NAME, CWS_NAME, CNS_NAME, EML_NAME, RI_NAME,CHP_NAME, DSB_NAME, MST_NAME, GRA_NAME,MAS_NAME, ELC_NAME, TAR_NAME}}

DIRECTIONS_COLORS = { DIR_EXPLICIT : "#9C954C", DIR_NONE : "#A350A0"}

SEPARATE_YES = "Separate"
SEPARATE_NO = "Superimposed"

SEPARATE_DICT = { TAP: {SEPARATE_YES : {DEA_NAME, DPA_NAME, BA_NAME}, 
                    SEPARATE_NO : {AANDB_NAME, AT_NAME, ATC_NAME, CHV_NAME, HIZ_NAME, HIB_NAME, DSL_NAME, CWS_NAME, CNS_NAME, EML_NAME, RI_NAME,CHP_NAME, DSB_NAME, MST_NAME, GRA_NAME,MAS_NAME, ELC_NAME, TAR_NAME}},
                 
                  HOLD : {SEPARATE_YES : {AANDB_NAME, BA_NAME, CHV_NAME, CWS_NAME, CNS_NAME, RI_NAME, GRA_NAME,MAS_NAME, ELC_NAME, TAR_NAME},
                          SEPARATE_NO : {DEA_NAME, DPA_NAME, AT_NAME, ATC_NAME, HIZ_NAME, HIB_NAME, DSL_NAME, EML_NAME, CHP_NAME, DSB_NAME, MST_NAME}}}

SEPARATE_COLORS = { DIR_EXPLICIT : "#9C954C", DIR_NONE : "#A350A0"}
                        
EXPLICITNESS_VALUE_DICT = { TAP : { AANDB_NAME : 4, DPA_NAME : 6, DEA_NAME : 3, BA_NAME : 6, AT_NAME : 2, 
                              CHV_NAME : 4,  ATC_NAME : 2, CHP_NAME : 4, HIZ_NAME : 1, HIB_NAME : 1, 
                            DSL_NAME : 1, DSB_NAME : 4, CWS_NAME : 2, CNS_NAME : 2, MST_NAME : 4, EML_NAME : 1,
                            RI_NAME :  1, MAS_NAME : 2, ELC_NAME : 3, GRA_NAME : 3, TAR_NAME : 3},
                    SWIPE : { AANDB_NAME : 6, DPA_NAME : 4, DEA_NAME : 2, BA_NAME : 4, AT_NAME : 2, 
                              CHV_NAME : 4,  ATC_NAME : 2, CHP_NAME : 2, HIZ_NAME : 2, HIB_NAME : 2, 
                            DSL_NAME : 2, DSB_NAME : 4, CWS_NAME : 6, CNS_NAME : 2, MST_NAME : 2, EML_NAME : 2,
                            RI_NAME : 2, MAS_NAME : 4, ELC_NAME : 4, GRA_NAME : 2, TAR_NAME : 6},
                    FLEX : { AANDB_NAME : 6, DPA_NAME : 4, DEA_NAME : 4, BA_NAME : 4, AT_NAME : 2, 
                              CHV_NAME : 4,  ATC_NAME : 4, CHP_NAME : 2, HIZ_NAME : 2, HIB_NAME : 2, 
                            DSL_NAME : 2, DSB_NAME : 2, CWS_NAME : 6, CNS_NAME : 4, MST_NAME : 2, EML_NAME : 4,
                            RI_NAME : 4, MAS_NAME : 4, ELC_NAME : 4, GRA_NAME : 2, TAR_NAME : 6},
                    HOLD : { AANDB_NAME : 6, DPA_NAME : 5, DEA_NAME : 2, BA_NAME : 6, AT_NAME : 2, 
                              CHV_NAME : 6,  ATC_NAME : 2, CHP_NAME : 4, HIZ_NAME : 1, HIB_NAME : 1, 
                            DSL_NAME : 2, DSB_NAME : 4, CWS_NAME : 3, CNS_NAME : 3, MST_NAME : 4, EML_NAME : 2,
                            RI_NAME : 3, MAS_NAME : 4, ELC_NAME : 4, GRA_NAME : 5, TAR_NAME : 6}}

EXPLICITNESS_VALUE_COLORS = { 1 : "#9C954C",
                        2 : "#9C734C",
                        3 : "#A34B4B",
                        4 : "#576BA8",
                        5 : "#519C5B",
                        6 : "#A350A0",}

EFFECT_TO_FOCUS = { ORDER : {x for x in SD_ORDER},
                    MG_ORDER : {x for x in MG_ORDERS},
                    STATE : {x for x in SYMBOLS},
                    MICROGESTURE : {x for x in MICROGESTURES},
                    FAMILY : {x for x in SYMBOLS},
                    EXPLICITNESS_VALUE : {x for x in EXPLICITNESS_VALUE_COLORS},
                    TRAJ : {x for x in TRAJECTORIES_DICT},
                    DIR : {x for x in DIRECTIONS_DICT},
                    SEPARATE : {x for x in SEPARATE_COLORS}}

def getRealExplicitness(value) :
    return {
        1: 0.17,
        2: 0.33,
        3: 0.5,
        4: 0.67,
        5: 0.83,
        6: 1
    }[value]

def setIfElseUnset(el, values, value) :
    """
    Sets to 1 the value of an IntVar
    if the given value is in the given
    values. Sets to 0 otherwise.
    
    :param values: list of values
    to consider
    :param value: value to consider
    """
    if value in values: 
        el.set(1)
    else : 
        el.set(0)
    

def getDropped(dic) :
    """
    Fetch the dictionnary keys
    associated with a value of zero
    
    :param dic: a dictionnary with
    binary values
    :return: a list of keys
    """
    droppedKeys = []
    for key in dic :
        if not dic[key] :
            droppedKeys.append(key)
    return droppedKeys
