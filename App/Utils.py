#!/usr/bin/env python3

import scipy as spy


def mergeDict(dict1, dict2): return {**dict1, **dict2}

ESTIMATORS = {"median": spy.median, "mean": spy.mean}
MEAN="mean"
MEDIAN="median"
SEARCHED_VALUES = [MEAN, MEDIAN]