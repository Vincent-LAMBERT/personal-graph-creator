#!/usr/bin/env python3

import csv
import os
from os.path import exists

import pandas as pd

RIGH_ARROW ='\ue0b0 '
LEFT_ARROW =' \ue0b2'
WIDTH = 75
COMPENSATE = 9

class DataProcessor :
    """
    Visualizer for the data visualized by a DataVisualizer
    """
    def __init__ (self, file) :
        """
        Instantiatation function

        :param file: the name of the parsed file
        """
        self.file = file
        self.data = []
        self.dataDict = dict()
        path = os.getcwd()+"/Experiences/"+file+"/CSV/"
        self.pandaInfoFile = path+"PandaInfo.csv"
        self.pandaDataFile = path+"PandaData.csv"
        self.dataFile = path+"Data.csv"
        self.presetFile = path+"Presets.csv"
        if exists(self.pandaInfoFile) and exists(self.pandaDataFile) :
            self.infoframe = pd.read_csv(self.pandaInfoFile, index_col=0)
            self.dataframe = pd.read_csv(self.pandaDataFile, index_col=0)
        else :
            with open(self.dataFile, 'r') as file :
                reader = csv.reader(file)
                keys = next(reader)
                for row in reader :
                    dataRow = {}
                    for i in range(len(keys)) :
                        dataRow[keys[i]] = row[i]
                    self.data.append(dataRow)

    def getPresets(self) :
        """
        Parse the presets writen in the Preset.csv file
        
        :return: a list of presets
        """
        data = []
        if exists(self.presetFile) :
            with open(self.presetFile, 'r') as file :
                reader = csv.reader(file)
                for line in reader :
                    data.append(line)
            file.close()
            return data
        return []
    
    def writePresets(self, presets) :
        """
        Write the given presets to the Preset.csv file

        :param presets: the presets to write
        """
        with open(self.presetFile, 'w') as file :
            writer = csv.writer(file)
            for preset in presets :
                writer.writerow(preset)
        file.close()
    

    def getDataSubset(self, functions, keys, *conditions) :
        """
        Fetch a subset of the computed data corresponding
        to the given keys. The associated values go through
        the according function in the functions list and is
        added to the return subset only if the conditions are
        all matched 

        :param functions: list of functions for the given keys
        :param keys: list of keys to fetch
        :param conditions: conditions to be matched for each
        fetched data in the resulting subset
        :return: a subset of self.data
        """
        dataSubset=[]
        for data in self.data :
            addData = {keys[i]: functions[i](data[keys[i]]) for i in range(len(keys))}
            if self.allConditionsMatched(data, conditions) :
                dataSubset.append(addData)
        return dataSubset
    
    def allConditionsMatched(self, data, conditions) :
        """
        Fetch a subset of the computed data corresponding
        to the given keys. The associated values go through
        the according function in the functions list and is
        added to the return subset only if the conditions are
        all matched 
        
        :param keys: list of keys to fetch
        :return: a subset of self.data
        """
        for condition in conditions :
            if not condition(data) :
                return False
        return True
