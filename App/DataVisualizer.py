#!/usr/bin/env python3

import sys
import tkinter as tk
from tkinter.filedialog import asksaveasfilename
from tkinter.simpledialog import askstring
import itertools

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pingouin
import scipy.stats as sp
import seaborn as sns
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.patches import Ellipse
from PIL import ImageColor
from App.Utils import *

from Experiences.Online2DHand.Utils.GeneralUtils import *

RIGH_ARROW ='\ue0b0 '
LEFT_ARROW =' \ue0b2'
WIDTH = 75
COMPENSATE = 9
CAPSIZE=0.1

class DataVisualizer :
    """
    Visualizer for the data handled by a DataProcessor
    """
    def __init__ (self, GUI, processor) :
        """
        Initialize the visualizer
        
        :param GUI: the application context
        :param processor: the corresponding DataProcessor
        """
        self.GUI = GUI
        self.processor = processor
        self.processor.setVisualizer(self)
        self.canvas = None
        self.axis = None
        self.generate=0
        self.showLegend = tk.IntVar()
        self.showAnnotations = tk.IntVar()
        self.estimator = tk.StringVar()
        self.searchedValue = tk.StringVar()
        self.effectSize = tk.IntVar()
        self.xWrap = tk.DoubleVar()
        self.yWrap = tk.DoubleVar()
        self.heightWrap = tk.DoubleVar()
        self.widthWrap = tk.DoubleVar()

        self.showLegend.set(1)
        self.showAnnotations.set(1)
        self.estimator.set("mean")
        self.searchedValue.set(MEAN)
        self.effectSize.set(0)
        self.xWrap.set(0.15)
        self.yWrap.set(0.15)
        self.widthWrap.set(0.70)
        self.heightWrap.set(0.70)

        self.presetsCombobox = tk.StringVar()
        plt.rcParams.update({'font.size': 15})
        plt.rcParams.update({'axes.labelsize': 18})
        plt.rcParams.update({'axes.titlesize': 22})
        plt.rcParams.update({"axes.labelpad": 30})
        plt.rcParams.update({"axes.titlepad": 30})
        plt.rcParams.update({"legend.fontsize": 11})
        plt.rcParams.update({"figure.figsize": [20,20]})
        plt.margins(0.2)
    
    def GUIprint(self, obj) :
        """
        Print in the integrated terminal
        """
        logger = PrintLogger(self.GUI.ConsoleFrame)
        logger.redirect()
        print(obj)
        logger.reset()
        self.GUI.ConsoleFrame.yview("scroll", -100, "units")

    def getPresetsNames(self) :
        """
        Returns all the presets name from the preset file
        
        :return: presets name of the preset file
        """
        presetNames = []
        for preset in self.getPresets() :
            presetNames.append(preset[0])
        return presetNames

    def getPresets(self) :
        """
        Returns all the presets from the preset file
        
        :return: presets of the preset file
        """
        self.presets = [['']]+self.processor.getPresets()
        return self.presets

    def getNamedPreset(self, name) :
        """
        Returns the preset with the given name
        
        :param name: name of the preset to get
        :return: preset with the given name
        """
        for preset in self.presets :
            if preset[0]==name :
                return preset[1:]
        return None

    def createPreset(self, name) :
        """
        Create a preset with the given name
        
        :param name: name of the preset
        :return: list of values of the preset
        """
        return [name]

    def savePreset(self) :
        """
        Save the preset into the corresponding file
        """
        name = askstring("Save preset", "Give a name to your preset")
        self.presets.append(self.createPreset(name))
        self.processor.writePresets(list(filter(lambda x:x[0]!="" , self.presets)))
        imageFile = "Images/PNG/"+name
        self.savePlot(imageFile)
        tk.messagebox.showinfo("Info", "Preset {0} saved".format(name))
        self.actualizeVisualizerGUI()

    def removePreset(self) :
        """
        Remove the preset from the corresponding file
        """
        name = self.presetsCombobox.get()
        self.presets = list(filter(lambda x:x[0]!=name and x[0]!="" , self.presets))
        self.processor.writePresets(self.presets)
        tk.messagebox.showinfo("Info", "Preset {0} deleted".format(name))
        self.actualizeVisualizerGUI()

    def savePlot(self, filename=None) :
        """
        Saves the plot with the corresponding filename
        Asks for a name with a prompt if not given
        
        :param filename: name to save the plot on
        """
        if filename==None:
            filename = asksaveasfilename()
        self.figure.savefig(filename)

    def actualizeVisualizerGUI(self) :
        """
        Actualize the MenuFrame
        """
        pass

    def actualizePlot(self) :
        """
        Actualize the GraphFrame
        """
        pass

    def resetAxis(self, title, data) :
        """
        Resets the canvas, sets the title and prints the
        data in the integrated terminal
        
        :param title: title of the plot
        :param value: specific value in all values
        """
        self.axis.set_title(title)
        self.GUIprint(str(data))

    def funcPie(self, value, values, shown) :
        """
        Formats the value according to the type of annotation shown

        :param title: title of the plot
        :param value: specific value in all values
        :param values: all values
        :param shown: type of information shown
        """
        if value>8 :
            if shown==PERCENTAGE :
                return "{0}%".format(int(value))
            else :
                total = sum(values)
                val = int(round(value*total/100.0))
                return "{0}".format(int(val))
        else : return ""

    def drawPie(self, title, data, labels, shown) :
        """
        Plots a pie for the given data

        :param title: title of the plot
        :param data: data to work on
        :param labels: annotations on the pie
        :param shown: type of information shown
        """
        self.resetAxis(title, data)
        
        colors = plt.get_cmap('Blues')(np.linspace(0.2, 0.7, len(data)))

        wedges, texts, autotexts = self.axis.pie(data, wedgeprops=dict(width=0.7), startangle=-40, autopct=lambda x: self.funcPie(x, data, shown))

        bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
        kw = dict(arrowprops=dict(arrowstyle="-"),
                bbox=bbox_props, zorder=0, va="center")

        for i, p in enumerate(wedges):
            ang = (p.theta2 - p.theta1)/2. + p.theta1
            y = np.sin(np.deg2rad(ang))
            x = np.cos(np.deg2rad(ang))
            horizontalalignment = {-1: "right", 1: "left"}[int(np.sign(x))]
            connectionstyle = "angle,angleA=0,angleB={}".format(ang)
            kw["arrowprops"].update({"connectionstyle": connectionstyle})
            self.axis.annotate(labels[i], xy=(x, y), xytext=(1.35*np.sign(x), 1.4*y),
                        horizontalalignment=horizontalalignment, **kw)

    def drawMultipleLines(self, title, xKey, yKey, hue, data, hue_order=None, palette=None, xLabels = False, yLabels = False, invertY=False, invertX=False, axisInvert=False) :
        """
        Plots multiple lines for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param hue: data change the color on
        :param data: data to work on
        :param hue_order: order to organize the hue data on
        :param palette: colors to use
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        ax = sns.lineplot(ax=self.axis, x=x, y=y, data=data, sort=False)
        ax = sns.lineplot(ax=self.axis, x=x, y=y, hue=hue, hue_order=hue_order, palette=palette, data=data, alpha=.3, style=hue, markers=True, dashes=False, sort=False)

        if invertY : ax.invert_yaxis()
        if invertX : ax.invert_xaxis()

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

    def drawMultipleBars(self, title, xKey, yKey, hue, data, hue_order=None, palette=None, xLabels = False, yLabels = False, invertX=False, invertY=False, axisInvert=False, image=False, orient=ORIENT_SIDE) :
        """
        Plots multiple bars for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param hue: data change the color on
        :param data: data to work on
        :param hue_order: order to organize the hue data on
        :param palette: colors to use
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        :param image: annotate with images if True
        :param orient: image annotation value
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        if len(data[x].unique())*len(data[hue].unique())>8 :
            ax = sns.barplot(ax=self.axis, x=x, y=y, hue=hue, hue_order=hue_order, palette=palette, data=data)
        else :
            ax = sns.barplot(ax=self.axis, x=x, y=y, hue=hue, hue_order=hue_order, palette=palette, data=data, capsize=CAPSIZE)

        if invertY : ax.invert_yaxis()
        if invertX : ax.invert_xaxis()

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

        if image :
            ax.tick_params(axis='x', which='major', pad=26)

            for i, c in enumerate(ax.get_xticklabels()):
                offsetXImage((i, 0), c, ax, orient)
        #Drawing a horizontal line at point 3
        # for i in range(2, 5):
        #     ax.axhline(y=i, linestyle='-.', color='k', alpha=.2)

    def drawScatter(self, title, xKey, yKey, hue, styles, markers, data, hue_order, palette, style_order, size=100, xLabels = False, yLabels = False, invertY=False, axisInvert=False) :
        """
        Plots a scatterplot for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param hue: data change the color on
        :param styles: list of styles to use
        :param markers: list of markers to use
        :param data: data to work on
        :param hue_order: order to organize the hue data on
        :param palette: colors to use
        :param style_order: order to organize the styles on
        :param size: scatter plot size
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        ax = sns.scatterplot(ax=self.axis, x=x, y=y, hue=hue, style=styles, markers=markers, hue_order=hue_order, palette=palette,
                             style_order=style_order, data=data, s=size, linewidth=0)

        if invertY :
            ax.invert_yaxis()
            ax.set_ylim(0,2)
        else :
            ax.set_xlim(0,2)

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

    def drawBivariateEllipses(self, title, xKey, yKey, hue, styles, coverage, data, hue_order, palette, style_order, size=10, xLabels = False, yLabels = False, invertY=False, axisInvert=False) :
        """
        Plots a scatterplot and its bivariate ellipses for the given data, keys and coverage percentage

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param hue: data change the color on
        :param styles: list of styles to use
        :param coverage: coverage percentage
        :param data: data to work on
        :param hue_order: order to organize the hue data on
        :param palette: colors to use
        :param style_order: order to organize the styles on
        :param size: scatter plot size
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        ax = sns.scatterplot(ax=self.axis, x=x, y=y, hue=hue, style=styles, hue_order=hue_order, palette=palette,
                             style_order=style_order, data=data, s=size, linewidth=0)

        if invertY :
            ax.invert_yaxis()
            ax.set_ylim(0,2)
        else :
            ax.set_xlim(0,2)

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

        hues = data[hue].unique().tolist()

        for i in range(len(hues)):
            sdata = data[data[hue]==hues[i]]
            sdataX = sdata[x].values.tolist()
            sdataY = sdata[y].values.tolist()
            sdataX_mean = np.mean(sdataX)
            sdataY_mean = np.mean(sdataY)
            cov = np.cov(sdataX, sdataY)

            if palette!=None :
                e = getCovEllipse(cov, (sdataX_mean, sdataY_mean), coverage, fc=palette[i], alpha=0.3)
            else :
                e = getCovEllipse(cov, (sdataX_mean, sdataY_mean), coverage, alpha=0.3)
            ax.add_artist(e)
    
    def drawRegplot(self, title, xKey, yKey, data, hueOrder, palette, xLabels = False, yLabels = False, invertY=False, axisInvert=False, xLimInf=False, xLimSup=False) :
        """
        Plots a regplot for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param data: data to work on
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        """
        self.resetAxis(title, data)
    
        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels
        
        bplot = data.boxplot(ax=self.axis, by=x, column=y, positions=hueOrder, return_type='dict', grid=False, patch_artist=True)

        # Style boxplot
        for patch, color in zip(bplot[y]['boxes'], palette):
            r, g, b, a = ImageColor.getcolor(color, "RGBA")
            patch.set_facecolor((r/256, g/256, b/256, 0.3))
            patch.set_edgecolor((0, 0, 0))
            patch.set_linewidth(1.5)
            patch.set_alpha(0.3)
        for whisker in bplot[y]['whiskers']:
            whisker.set_color((0, 0, 0))
            whisker.set_linewidth(1.5)
            whisker.set_alpha(0.3)
        for fliers in bplot[y]['fliers']:
            fliers.set_alpha(None)
            fliers.set_markerfacecolor((0, 0, 0, 0.05))
            fliers.set_markeredgecolor((0, 0, 0, 0.05))
            fliers.set_marker('d')
        for median in bplot[y]['medians']:
            median.set_color((0, 0, 0, 0.3))
            median.set_linewidth(1.5)
            median.set_alpha(0.3)
        for caps in bplot[y]['caps']:
            caps.set_color((0, 0, 0, 0.3))
            caps.set_linewidth(1.5)
            caps.set_alpha(0.3)        
        
        data = data.drop([x for x in data.columns if x!=x and x!=y], axis=1)
        data[y] =  data.groupby([x])[y].transform(np.mean)
        
        ax = sns.regplot(ax=self.axis, x=x, y=y, data=data, truncate=False)
        
        def annotate(ax, data, x, y):
            slope, intercept, rvalue, pvalue, stderr = sp.linregress(x=data[x], y=data[y])
            ax.text(.05, .9, f'slope={slope:.2f}', transform=ax.transAxes)
            ax.text(.05, .8, f'r2={rvalue ** 2:.2f} ', transform=ax.transAxes)

        annotate(ax, data=data, x=x, y=y)

        if xLabels :
            ax.set_xticks(ticks = [x for x in hueOrder], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)
            
        if xLimInf: ax.set_xlim(left=xLimInf)
        if xLimSup: ax.set_xlim(right=xLimSup)

    def GUIPrintDataStats(self, data, xKey, yKey):
        xKeys = data[xKey].unique().tolist()
        d = {x: data[data[xKey]==x][yKey].to_list() for x in xKeys}

        # Compute mean, standard deviation and CI of d
        mean = {x: np.round(np.mean(d[x]),2) for x in d}
        median = {x: np.round(np.median(d[x]),2) for x in d}
        std = {x: np.round(np.std(d[x]),2) for x in d}
        # ci = {x: np.round(sp.t.interval(0.95, len(d[x])-1, loc=np.mean(d[x]), scale=sp.sem(d[x])),2) for x in d}
        mean_ci = {x: np.round(self.getBootstrappedCI(d[x], np.mean), 2) for x in d}
        median_ci = {x: np.round(self.getBootstrappedCI(d[x], np.median), 2) for x in d}

        for x in d :
            self.GUIprint(x.__str__()+" - (median="+ median[x].__str__()+" +- " + median_ci[x].__str__() + ", mean="+ mean[x].__str__()+" +- " + mean_ci[x].__str__() + ", delta = " + std[x].__str__() + ", n="+ len(d[x]).__str__() +")")
            
        # Non-parametric test
        # Kruskal-Wallis H-test
        kruskal = sp.kruskal(*d.values())
        man_whitney = {(x,y): sp.mannwhitneyu(d[x], d[y], alternative='two-sided') for x in d for y in d if x!=y}
        
        for (x,y) in man_whitney.keys() :
            self.GUIprint(f"Mann-Whitney U test between {x} and {y} : {man_whitney[(x,y)]}")
        
        self.GUIprint("Kruskal-Wallis H-test : " + kruskal.__str__())
            
            
    
    def getBootstrappedCI(self, data, estimator):
        # Sample must be a sequence
        data = (data,)
        # Calculate 95% bootstrapped confidence interval for median
        bootstrap_ci = sp.bootstrap(data, estimator, confidence_level=0.95,
                                random_state=1, method='percentile')
        
        return bootstrap_ci.confidence_interval

    def drawStrip(self, title, xKey, yKey, data, order, palette=None, xLabels = False, yLabels = False, invertX=False, invertY=False, axisInvert=False, image=False, orient=ORIENT_SIDE) :
        """
        Plots a barplot for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param data: data to work on
        :param order: order to reorganize the data on
        :param palette: colors to use
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        :param image: annotate with images if True
        :param orient: image annotation value
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        if palette!=None:
            ax = sns.stripplot(ax=self.axis, x=x, y=y, hue=x, data=data, order=order, palette=palette, zorder=-1)
        else :
            ax = sns.stripplot(ax=self.axis, x=x, y=y, hue=x, data=data, order=order, zorder=-1)
        
        if self.showAnnotations.get()!=0:
            # Plot medians CI computed with bootstrapping
            boxes_data = [data[data[xKey]==x][yKey].to_list() for x in order]
            
            median_ci = [self.getBootstrappedCI(d, np.median) for d in boxes_data]
            # Plot a line for each median CI
            for i in range(len(order)):
                ax.plot([i, i], median_ci[i], color="#400E0E", linestyle='-', linewidth=3, zorder=1)
            
            # Plot median
            ax = sns.pointplot(ax=self.axis, x=x, y=y, data=data, order=order, dodge=.8 - .8 / 3, color="#400E0E", errorbar=None, estimator=np.median, join=False, markers='o', scale=1.2)
            
            # Plot means
            ax = sns.pointplot(ax=self.axis, x=x, y=y, data=data, order=order, dodge=.8 - .8 / 3, color="#BF2929", errorbar=None, estimator=np.mean, join=False, markers='d', scale=0.8)
            
        self.GUIPrintDataStats(data, xKey, yKey)

        if invertY : ax.invert_yaxis()
        if invertX : ax.invert_xaxis()

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

        labels = [text.get_text() for text in ax.get_xticklabels()]
        if palette!=None:
            markers = [plt.Line2D([0,0],[0,0], color=c, marker='o', linestyle='') for c in palette]
            ax.legend(markers, labels, numpoints=1)
        else :
            ax.legend(labels, numpoints=1)

        if image :
            # use the ytick values to locate the image
            y = ax.get_yticks()[1]

            for i, c in enumerate(ax.get_xticklabels()):
                xy = (i, y-1.05)
                offsetXImage(xy, c, ax, orient)

        # #Drawing a horizontal line at point 3
        # for i in range(2, 5):
        #     ax.axhline(y=i, linestyle='-.', color='k', alpha=.2)

    def drawBox(self, title, xKey, yKey, data, order, palette=None, xLabels = False, yLabels = False, invertX=False, invertY=False, axisInvert=False, image=False, orient=ORIENT_SIDE, showPoints=False) :
        """
        Plots a barplot for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param data: data to work on
        :param order: order to reorganize the data on
        :param palette: colors to use
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        :param image: annotate with images if True
        :param orient: image annotation value
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        if palette!=None:
            ax = sns.boxplot(ax=self.axis, x=x, y=y, data=data, order=order, palette=palette)
        else :
            ax = sns.boxplot(ax=self.axis, x=x, y=y, data=data, order=order)
        
        if showPoints: ax = sns.stripplot(ax=self.axis, x=x, y=y, hue=x, data=data, order=order, palette=[darkenColor(x) for x in palette], zorder=1)
        
        if self.showAnnotations.get()!=0:
            # Plot medians CI computed with bootstrapping
            boxes_data = [data[data[xKey]==x][yKey].to_list() for x in order]
            
            median_ci = [self.getBootstrappedCI(d, np.median) for d in boxes_data]
            # Plot a line for each median CI
            for i in range(len(order)):
                ax.plot([i, i], median_ci[i], color="#400E0E", linestyle='-', linewidth=3, zorder=2)
                # Plot a horizontal border for each CI line
                ax.plot([i-0.1, i], [median_ci[i][0]-0.1, median_ci[i][0]], color="#400E0E", linestyle='-', linewidth=3, zorder=2)
                ax.plot([i, i+0.1], [median_ci[i][0], median_ci[i][0]-0.1], color="#400E0E", linestyle='-', linewidth=3, zorder=2)
                
                ax.plot([i-0.1, i], [median_ci[i][1]+0.1, median_ci[i][1]], color="#400E0E", linestyle='-', linewidth=3, zorder=2)
                ax.plot([i, i+0.1], [median_ci[i][1], median_ci[i][1]+0.1], color="#400E0E", linestyle='-', linewidth=3, zorder=2)
                
                
            # Plot means
            ax = sns.pointplot(ax=self.axis, x=x, y=y, data=data, order=order, color="#BF2929", errorbar=None, estimator=np.mean, join=False, markers='o', scale=1.2)
            
        self.GUIPrintDataStats(data, xKey, yKey)

        if invertY : ax.invert_yaxis()
        if invertX : ax.invert_xaxis()

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

        labels = [text.get_text() for text in ax.get_xticklabels()]
        if palette!=None:
            markers = [plt.Line2D([0,0],[0,0], color=c, marker='o', linestyle='') for c in palette]
            ax.legend(markers, labels, numpoints=1)
        else :
            ax.legend(labels, numpoints=1)

        if image :
            # use the ytick values to locate the image
            y = ax.get_yticks()[1]

            for i, c in enumerate(ax.get_xticklabels()):
                xy = (i, y-1.05)
                offsetXImage(xy, c, ax, orient)

        # #Drawing a horizontal line at point 3
        # for i in range(2, 5):
        #     ax.axhline(y=i, linestyle='-.', color='k', alpha=.2)

    def drawViolin(self, title, xKey, yKey, data, order, palette=None, xLabels = False, yLabels = False, invertX=False, invertY=False, axisInvert=False, image=False, orient=ORIENT_SIDE) :
        """
        Plots a barplot for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param data: data to work on
        :param order: order to reorganize the data on
        :param palette: colors to use
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        :param image: annotate with images if True
        :param orient: image annotation value
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        if palette!=None:
            ax = sns.violinplot(ax=self.axis, x=x, y=y, data=data, order=order, palette=palette)
        else :
            ax = sns.violinplot(ax=self.axis, x=x, y=y, data=data, order=order)
        
        self.GUIPrintDataStats(data, xKey, yKey)
        
        if invertY : ax.invert_yaxis()
        if invertX : ax.invert_xaxis()

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

        labels = [text.get_text() for text in ax.get_xticklabels()]
        if palette!=None:
            markers = [plt.Line2D([0,0],[0,0], color=c, marker='o', linestyle='') for c in palette]
            ax.legend(markers, labels, numpoints=1)
        else :
            ax.legend(labels, numpoints=1)

        if image :
            ax.tick_params(axis='x', which='major', pad=26)

            for i, c in enumerate(ax.get_xticklabels()):
                offsetXImage((i, 0), c, ax, orient)

        #Drawing a horizontal line at point 3
        for i in range(2, 5):
            ax.axhline(y=i, linestyle='-.', color='k', alpha=.2)
    
    def drawBars(self, title, xKey, yKey, data, order, palette=None, xLabels = False, yLabels = False, invertX=False, invertY=False, axisInvert=False, image=False, orient=ORIENT_SIDE) :
        """
        Plots a barplot for the given data and keys

        :param title: title of the plot
        :param xKey: column label for the x axis
        :param yKey: column label for the y axis
        :param data: data to work on
        :param order: order to reorganize the data on
        :param palette: colors to use
        :param xLabels: labels to replace the absissa values with
        :param yLabels: labels to replace the ordinate values with
        :param invertX: reverse the absissa order
        :param invertY: reverse the ordinate order
        :param axisInvert: reverse absissa and ordinate
        :param image: annotate with images if True
        :param orient: image annotation value
        """
        self.resetAxis(title, data)

        if not axisInvert :
            x, y = xKey, yKey
        else :
            x, y = yKey, xKey
            xLabels, yLabels = yLabels, xLabels

        if palette!=None:
            ax = sns.barplot(ax=self.axis, x=x, y=y, data=data, order=order, palette=palette, capsize=CAPSIZE, estimator=ESTIMATORS[self.estimator.get()])
        else :
            ax = sns.barplot(ax=self.axis, x=x, y=y, data=data, order=order, capsize=CAPSIZE, estimator=ESTIMATORS[self.estimator.get()])

        if self.showAnnotations.get()!=0:
            ax.bar_label(ax.containers[-1], fmt='Mean:\n%.2f', label_type='center')
        
        self.GUIPrintDataStats(data, xKey, yKey)
        
        if invertY : ax.invert_yaxis()
        if invertX : ax.invert_xaxis()

        if xLabels :
            ax.set_xticks(ticks = [x for x in range(1, len(xLabels)+1)], labels = xLabels)
        if yLabels :
            ax.set_yticks(ticks = [y for y in range(1, len(yLabels)+1)], labels = yLabels)

        labels = [text.get_text() for text in ax.get_xticklabels()]
        if palette!=None:
            markers = [plt.Line2D([0,0],[0,0], color=c, marker='o', linestyle='') for c in palette]
            ax.legend(markers, labels, numpoints=1)
        else :
            ax.legend(labels, numpoints=1)

        if image :
            ax.tick_params(axis='x', which='major', pad=26)

            for i, c in enumerate(ax.get_xticklabels()):
                offsetXImage((i, 0), c, ax, orient)

        #Drawing a horizontal line at point 3
        for i in range(2, 5):
            ax.axhline(y=i, linestyle='-.', color='k', alpha=.2)
            
    def drawClusteredStackedBars(self, title, df, droppedColumn, keptValue, stack, hash, cluster, hashInvert=False, clusterInvert=False) :
        """
        Plots multiple lines for the given data and keys

        :param title: title of the plot
        :param df: dataframe to work on
        :param droppedColumn: column to drop
        :param keptValue: value to keep on the 
        droppedColumn to plot the whole graph
        :param stack: column to use as stack parameter
        :param hash: column to use as hash parameter
        :param cluster: column to use as cluster parameter
        """
        self.resetAxis(title, df)
        H="/"
        
        axis = self.axis
        secAxis = axis.secondary_xaxis('top')
        
        df = df[df[droppedColumn]==keptValue] 
        stacks=list(df[stack].unique())
        hashes=list(df[hash].unique())
        clusters=list(df[cluster].unique())
        # Inverts or not the data
        if hashInvert : hashes.reverse()
        if clusterInvert : clusters.reverse()
        
        nbStacks = len(stacks)
        nbHashes = len(hashes)
        nbClusters = len(clusters)
        nbStacked = False
        totalBars = nbClusters*nbStacks*nbHashes
        barWidth = 1 / float(totalBars + 1)
        
        # Prepare the data
        clustersDf = {}
        independantDf = {x: y for x, y in df.groupby(cluster)}
        # print("\n\nclusters", clusters)
        # print("independantDf", independantDf)
        for state in clusters : 
            subDf = independantDf[state].drop(columns=[droppedColumn, cluster], axis=1)
            newIndependantDf = {x: y for x, y in subDf.groupby(hash)}
            # print("hashes", hashes)
            # print("newIndependantDf", newIndependantDf)
            # print("clusters[i]", state)
            clustersDf[state] = {}
            for j in newIndependantDf :
                clustersDf[state][j] = newIndependantDf[j].drop(columns=hash, axis=1).set_index(stack)
                if not nbStacked : nbStacked = len(clustersDf[state][j].columns)
        
        # Plots the data
        for clusterId in clusters :
            hashesDf = clustersDf[clusterId]
            for hashId in hashes :
                stackDf = hashesDf[hashId]
                axis = stackDf.plot(kind="bar",
                            linewidth=0,
                            stacked=True,   
                            ax=axis,
                            legend=False,
                            grid=False)  # make bar plots
            
        # Rearrange the plotted rectangles
        stackTicks = {}
        clusterTicks = {}
        h,l = axis.get_legend_handles_labels() # get the handles we want to modify
        for clusterIndex in range(nbClusters) :
            # Compute the cluster translation
            stackAndHashWidth = 2*nbStacks*barWidth
            clusterXMove = clusterIndex*2*stackAndHashWidth
            
            # Update the ticks with the corresponding x and cluster values
            clusterTicks[clusterXMove+(stackAndHashWidth+barWidth)/2] = clusters[clusterIndex]
            
            for hashIndex in range(nbHashes) :
                # Compute the hash translation
                stackWidth = (nbStacks+1)*barWidth
                hashXMove = hashIndex*stackWidth
                
                for stackedIndex in range(nbStacked) :
                    # Get each stacked pile and treat each type in it individually
                    # (As they are considered in different Bar Containers)
                    plotIndex = stackedIndex+hashIndex*nbStacked+clusterIndex*nbHashes*nbStacked    
                    plot = h[plotIndex]
                    piledRects = [(x, y) for x,y in enumerate(plot.patches)]
                    
                    for (stackIndex, piledRect) in piledRects : # for each index
                        # Calculate the stack translation
                        barXMove = stackIndex*barWidth
                        
                        # Calculate the total translation
                        totalXMove = barXMove+hashXMove+clusterXMove
                        
                        # Apply the translation to the rect
                        piledRect.set_x(totalXMove)   
                        piledRect.set_hatch(H * hashIndex) #edited part 
                        piledRect.set_width(barWidth)
                        
                        # Update the ticks with the corresponding x and stack values
                        stackTicks[totalXMove+barWidth/2] = stacks[stackIndex]
                        
        # Set windows borders
        existingMin = h[0].patches[0].get_x()
        existingMax = h[-1].patches[-1].get_x()+barWidth
        margin = (existingMax-existingMin)*0.1
        axis.set_xlim(existingMin-margin,existingMax+margin)
        
        # Set ticks
        axis.set_xticks(list(stackTicks.keys()))
        axis.set_xticklabels(list(stackTicks.values()))
        secAxis.set_xticks(list(clusterTicks.keys()))
        secAxis.set_xticklabels(list(clusterTicks.values()))
        
        # Set legends
        l1 = axis.legend(h[:nbStacked], l[:nbStacked], loc=[1.01, 0.5])
        axis.add_artist(l1)
        
        # Add invisible data to add another legend
        n=[]        
        for i in range(nbHashes):
            n.append(axis.bar(0, 0, color="gray", hatch=H * i))
        l2 = axis.legend(n, hashes, loc=[1.01, 0.1]) 
        axis.add_artist(l2)
    
    def drawEffectSize(self, title, xKey, yKey, data, focus, image=False, orient=ORIENT_ABOVE) :
        """
        Plots effects sizes relatives to the given focus

        :param title: title of the plot
        :param xKey: column label for the various categories
        :param yKey: column label for the associated values
        :param focus: focused category
        :param image: annotate with images if True
        :param orient: image annotation value
        :param data: data to work on
        """
        self.resetAxis(title, data)

        xKeys = data[xKey].unique().tolist()
        d = {x: data[data[xKey]==x][yKey].to_list() for x in xKeys}

        # Works but insanely slow
        # df = pd.DataFrame(dict([ (k,pd.Series(v)) for k,v in d.items() ]))
        # dabestGroups = dabest.load(df, idx=xKeys, resamples=5000)
        # dabestGroups.mean_diff.plot(ax=self.axis)

        xList = d[focus]
        esciDict={}
        tstatsDict={}
        pvalueDict={}
        i=0
        minEs = 0
        for key in d :
            i+=1
            yList = d[key]
            es = pingouin.compute_effsize(xList, yList,eftype="cohen")
            tstats, pvalue = sp.ttest_ind(xList, yList)
            tstatsDict[key]=tstats
            pvalueDict[key]=pvalue
            if minEs > es : minEs = es
            nx = len(xList)
            ny = len(yList)
            esci = pingouin.compute_esci(es, nx, ny, eftype="cohen")
            esciDict[key] = esci
            self.axis = plotConfidenceInterval(i, es, esci, self.axis)
            if key==focus:
                esciFocus=esci

        for key in esciDict :
            ol=overlapPercentage(esciDict[key], esciFocus)
            self.GUIprint(str(focus)+" - " +str(key)+" : overlap "+str(ol)+"% | pvalue "+str(pvalueDict[key]))
        
        self.axis.set_xticks(ticks = [i for i in range(1, len(d)+1)], labels = [key for key in d])

        xBound = self.axis.get_xbound()
        yBound = self.axis.get_ybound()
        hstep=(xBound[1]-xBound[0])/10
        vstep=(yBound[1]-yBound[0])/10
        
        self.GUIprint("Focus on : "+str(focus))
        self.axis.axhline(y=0, linestyle='-.', color='r', alpha=.2)
        self.axis.axhline(y=-0.2, linestyle='-.', color='k', alpha=.2)
        self.axis.axhline(y=0.2, linestyle='-.', color='k', alpha=.2)

        if image :
            # use the ytick values to locate the image
            y = minEs

            for i, c in enumerate(self.axis.get_xticklabels()):
                xy = (i+1, y-0.165)
                
                offsetXImage(xy, c, self.axis, orient)

    def actualizeCanvas(self) :
        """
        Actualize the visualization canvas
        """
        if self.canvas: self.canvas.get_tk_widget().pack_forget()  # remove previous image
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.GUI.GraphFrame)
        self.canvas.get_tk_widget().pack()
        self.canvas.draw()

class PrintLogger(object):  # create file like object
    """
    A object to redirect print into a textbox widget
    """
    def __init__(self, textbox):
        """
        Pass reference to textbox widget
        """
        self.textbox = textbox  # keep ref

    def write(self, text):
        """
        Insert text to the textbox
        
        :param text: text to insert
        """
        self.textbox.configure(state="normal")  # make field editable
        self.textbox.insert('0.0', text)  # write text to textbox
        self.textbox.configure(state="disabled")  # make field readonly
    
    def reset(self):
        """
        Reset the standard output
        """
        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

    def redirect(self):
        """
        Redirect the standard output
        to self
        
        Allows to write in the textbox
        widget with print commands
        """
        sys.stdout = self
        sys.stderr = self

def getCovEllipse(cov, center, percent, **kwargs):
        """
        Return a matplotlib Ellipse patch representing the covariance matrix
        cov centered at center and scaled by the coverage percentage
        
        :param center: where to center the covariance matrix
        :param percent: coverage percentage
        :param kwargs: Ellipse kwargs
        :return: coverage Ellipse
        """
        # Find and sort eigenvalues and eigenvectors into descending order
        eigvals, eigvecs = np.linalg.eigh(cov)
        order = eigvals.argsort()[::-1]
        eigvals, eigvecs = eigvals[order], eigvecs[:, order]

        if percent not in COVERAGE_PERCENTS :
            return

        # The anti-clockwise angle to rotate our ellipse by
        vx, vy = eigvecs[:,0][0], eigvecs[:,0][1]
        theta = np.arctan2(vy, vx)

        # Width and height of ellipse to draw
        eigvals = [abs(x) for x in eigvals]
        width, height = 2 * COVERAGE_PERCENTS[percent] * np.sqrt(eigvals)

        return Ellipse(xy=center, width=width, height=height,
                    angle=np.degrees(theta), **kwargs)


COVERAGE_PERCENTS = {5 : 0.10, 10 : 0.21, 20 : 0.45, 30: 0.71, 50: 1.39, 70: 2.41, 80: 3.22, 90: 4.61, 95: 5.99, 99: 9.21, 99.9 : 13.82}

def overlapPercentage(xlist,ylist):
    """
    Returns the overlap percentage of
    the given lists based on their maximum
    and minimum values
    
    :param xlist: initial list
    :param ylist: overlapping list
    :return: overlap percentage
    """
    min1 = min(xlist)
    max1 = max(xlist)
    min2 = min(ylist)
    max2 = max(ylist)

    overlap = max(0, min(max1, max2) - max(min1, min2))
    length = min(max1-min1, max2-min2)

    return int((overlap/length)*100)

def plotConfidenceInterval(x, y, esci, axis):
    """
    Plots the confidence interval 
    on the given Axis
    
    :param x: considered absissa 
    :param y: ordinate of the
    interval center  
    :param esci: interval
    :param axis: Axis to plot on
    """
    horizontal_line_width = 0.1
    color= "#585858"

    left = x - horizontal_line_width / 2
    top = esci[0]
    right = x + horizontal_line_width / 2
    bottom = esci[1]
    axis.plot([x, x], [top, bottom], color=color)
    axis.plot([left, right], [top, top], color=color)
    axis.plot([left, right], [bottom, bottom], color=color)
    axis.plot(x, y, 'o', color='#f44336')

    return axis

