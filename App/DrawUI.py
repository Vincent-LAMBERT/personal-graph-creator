#!/usr/bin/env python3

import tkinter as tk
from tkinter import DISABLED, HORIZONTAL, ttk
from tkinter.scrolledtext import ScrolledText

rows={}
columns={}
columnspans={}
RIGHT="Right"
LEFT="LEFT"
SPAN="SPAN"

PACK="Pack"
PLACE="Place"
GRID="Grid"
METHODS=[PACK, PLACE, GRID]

# Draw functions

def drawFrame(df, method=GRID, side=SPAN) :
    """
    Create, configure and returns a Frame

    :param df: the parent container
    :param method: placement method
    :param side: side to grid in if grid
    :return: Frame instance
    """
    DataFrame = tk.Frame(df)
    placeWidget(df, DataFrame, method, side)
    DataFrame.configure(relief='groove')
    DataFrame.configure(borderwidth="2")
    DataFrame.configure(relief="groove")
    DataFrame.configure(background="#d9d9d9")
    return DataFrame

def drawCombobox(df, variable, values, function=None, current="", method=GRID, side=SPAN) :
    """
    Create, configure and returns a Combobox

    :param df: the parent container
    :param variable: variable to bind value to
    :param values: list of values in the Combobox
    :param function: function to trigger on validation
    :param current: current value
    :param method: placement method
    :param side: side to grid in if grid
    :return: Combobox instance
    """
    Combobox = ttk.Combobox(df)
    placeWidget(df, Combobox, method, side)
    Combobox.configure(state='readonly')
    Combobox.configure(textvariable=variable)
    Combobox.configure(takefocus="")
    Combobox.configure(values=values)
    if function!=None :
        registered_function = Combobox.register(function)
        Combobox.bind("<<ComboboxSelected>>", registered_function)
    if len(Combobox['values']) :
        values = {Combobox['values'][x]:x for x in range(len(Combobox['values']))}
        values[""]=0
        if isinstance(current, int) :
            Combobox.current(current)
        else :
            Combobox.current(values[current])
    return Combobox

def drawCheckButton(df, variable, text, function=None, checked=False, method=GRID, side=SPAN) :
    """
    Create, configure and returns a CheckButton

    :param df: the parent container
    :param variable: variable to bind value to
    :param text: associated text value
    :param function: function to trigger on validation
    :param checked: current value
    :param method: placement method
    :param side: side to grid in if grid
    :return: CheckButton instance
    """
    CheckButton = tk.Checkbutton(df)
    placeWidget(df, CheckButton, method, side)
    CheckButton.configure(activebackground="beige")
    CheckButton.configure(activeforeground="black")
    CheckButton.configure(anchor='w')
    CheckButton.configure(background="#d9d9d9")
    CheckButton.configure(compound='left')
    CheckButton.configure(disabledforeground="#a3a3a3")
    CheckButton.configure(foreground="#000000")
    CheckButton.configure(highlightbackground="#d9d9d9")
    CheckButton.configure(highlightcolor="black")
    CheckButton.configure(justify='left')
    CheckButton.configure(selectcolor="#d9d9d9")
    CheckButton.configure(text=text)
    CheckButton.configure(variable=variable)
    if function!=None : CheckButton.configure(command=function)
    if checked: CheckButton.select()
    return CheckButton

def drawRadioButton(df, variable, text, function=None, checked=False, method=GRID, side=SPAN) :
    """
    Create, configure and returns a RadioButton

    :param df: the parent container
    :param variable: variable to bind value to
    :param text: associated text value
    :param function: function to trigger on validation
    :param checked: current value
    :param method: placement method
    :param side: side to grid in if grid
    :return: RadioButton instance
    """
    Radiobutton = tk.Radiobutton(df)
    placeWidget(df, Radiobutton, method, side)
    Radiobutton.configure(text=text)
    Radiobutton.configure(variable=variable)
    if function!=None : Radiobutton.configure(command=function)
    if checked : Radiobutton.configure(value=text)
    return Radiobutton

def drawButton(df, text, function=None, size=12, method=GRID, side=SPAN) :
    """
    Create, configure and returns a Button

    :param df: the parent container
    :param text: associated text value
    :param function: function to trigger on validation
    :param size: button font size
    :param method: placement method
    :param side: side to grid in if grid
    :return: Button instance
    """
    Button = tk.Button(df)
    placeWidget(df, Button, method, side)
    Button.configure(activebackground="beige")
    Button.configure(activeforeground="black")
    Button.configure(background="#d9d9d9")
    Button.configure(compound='left')
    Button.configure(disabledforeground="#a3a3a3")
    Button.configure(font="-family {Segoe UI} -size "+str(size))
    Button.configure(foreground="#000000")
    Button.configure(highlightbackground="#d9d9d9")
    Button.configure(highlightcolor="black")
    Button.configure(pady="0")
    Button.configure(text=text)
    if function!=None : Button.configure(command=function)
    return Button

def drawScrollableCheckButton(scrolledText, variable, text, function=None, checked=True, method=GRID, side=SPAN) :
    """
    Create, configure and returns a ScrollableCheckButton
    which is a CheckButton associated to a ScrolledText

    :param scrolledText: the parent ScrolledText
    :param variable: variable to bind value to
    :param text: associated text value
    :param function: function to trigger on validation
    :param checked: current value
    :param method: placement method
    :param side: side to grid in if grid
    :return: ScrollableCheckButton instance
    """
    CheckButton = tk.Checkbutton(scrolledText)
    placeWidget(scrolledText, CheckButton, method, side)
    CheckButton.configure(activebackground="beige")
    CheckButton.configure(activeforeground="black")
    CheckButton.configure(anchor='w')
    CheckButton.configure(width=30)
    CheckButton.configure(background="#d9d9d9")
    CheckButton.configure(compound='left')
    CheckButton.configure(disabledforeground="#a3a3a3")
    CheckButton.configure(foreground="#000000")
    CheckButton.configure(highlightbackground="#d9d9d9")
    CheckButton.configure(highlightcolor="black")
    CheckButton.configure(justify='left')
    CheckButton.configure(selectcolor="#d9d9d9")
    CheckButton.configure(text=text)
    CheckButton.configure(variable=variable)
    if function!=None : CheckButton.configure(command=function)
    if checked: CheckButton.select()
    scrolledText.window_create('end', window=CheckButton)
    scrolledText.insert('end', '\n')
    return CheckButton


def drawLabel(df, text, method=GRID, side=SPAN) :
    """
    Create, configure and returns a Label

    :param df: the parent container
    :param text: associated text value
    :param method: placement method
    :param side: side to grid in if grid
    :return: Label instance
    """
    Label = tk.Label(df)
    placeWidget(df, Label, method, side)
    Label.configure(activebackground="#f9f9f9")
    Label.configure(background="#d9d9d9")
    Label.configure(compound='left')
    Label.configure(disabledforeground="#a3a3a3")
    Label.configure(font="-family {Segoe UI} -size 10")
    Label.configure(foreground="#000000")
    Label.configure(highlightbackground="#d9d9d9")
    Label.configure(highlightcolor="black")
    Label.configure(text=text)
    return Label

def drawScale(df, variable, function=None, from_=0.0, to=20.0, resolution=1.0, set=0, method=GRID, side=SPAN) :
    """
    Create, configure and returns a Scale

    :param df: the parent container
    :param variable: variable to bind value to
    :param function: function to trigger on validation
    :param from_: range beginning value
    :param to_: range ending value
    :param resolution: distance between two following values
    :param set: current value
    :param method: placement method
    :param side: side to grid in if grid
    :return: Scale instance
    """
    Scale = tk.Scale(df, from_=from_, to=to, resolution=resolution)
    placeWidget(df, Scale, method, side)
    Scale.configure(activebackground="beige")
    Scale.configure(background="#d9d9d9")
    Scale.configure(foreground="#000000")
    Scale.configure(highlightbackground="#d9d9d9")
    Scale.configure(highlightcolor="black")
    Scale.configure(orient="horizontal")
    Scale.configure(variable=variable)
    Scale.configure(troughcolor="#d9d9d9")
    Scale.set(set)
    if function!=None : 
        actualizeScale = Scale.register(function)
        Scale.bind("<ButtonRelease-1>", actualizeScale)
    return Scale

def drawScrollableText(df, height=10, method=GRID, side=SPAN) :
    """
    Create, configure and returns a ScrolledText

    :param df: the parent container
    :param height: height of the ScrolledText
    :param method: placement method
    :param side: side to grid in if grid
    :return: ScrolledText instance
    """
    scrollableText = ScrolledText(df, state=DISABLED, height=height, background="#d9d9d9", foreground="#000000")
    placeWidget(df, scrollableText, method, side)
    
    scrollableText.bind("<Button-4>", lambda event: mouse_wheel(event, scrollableText))
    scrollableText.bind("<Button-5>", lambda event: mouse_wheel(event, scrollableText))

    return scrollableText

# Draw functions utils

def placeWidget(df, widget, method, side) :
    """
    place a widget

    :param df: the parent container
    :param widget: widget to place
    :param method: placement method
    :param side: side to grid in if grid
    """
    global rows, columns, columnspans
    if method==GRID:
        if not df in rows :
            rows[df]=0
            columns[df]=0
            columnspans[df]=1
        # Update from preceeding drawings
        if columns[df]==1 or side==SPAN or columnspans[df]==2 or columns[df]==0 and side==LEFT:
            rows[df]+=1
            columns[df]=0
            columnspans[df]=1
        # Setup for the nex drawing
        if side==RIGHT:
            columns[df]=1
        if side==SPAN:
            columnspans[df]=2
        else :
            columnspans[df]=1
        widget.grid(row=rows[df], column=columns[df], columnspan=columnspans[df])
    else :
        widget.pack()

def mouse_wheel(event, scrollableText):
    """
    Move the canvas view according to the scroll
    
    :param event: the mouse scroll event
    """
    # respond to Linux or Windows wheel event
    if event.num == 4 or event.delta == 120:
        scrollableText.yview("scroll", -1, "units")
    if event.num == 5 or event.delta == -120:
        scrollableText.yview("scroll", 1, "units")
        
